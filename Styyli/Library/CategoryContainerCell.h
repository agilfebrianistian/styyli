//
//  CategoryContainerCell.h
//  STYYLI
//
//  Created by Agil Febrianistian on 2/10/15.
//  Copyright (c) 2015 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryContainerCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *itemtitle;

@end
