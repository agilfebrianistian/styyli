//
//  ViewDeckSearchBar.m
//  STYYLI
//
//  Created by Agil Febrianistian on 6/12/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import "ViewDeckSearchBar.h"
#define kViewDeckPadding 260

@interface ViewDeckSearchBar()
@property (readonly) UITextField *textField;
@end

@implementation ViewDeckSearchBar

static CGRect initialTextFieldFrame;

- (void) layoutSubviews {
    
    [super layoutSubviews];
    
    // Store the initial frame for the the text field
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        initialTextFieldFrame = self.textField.frame;
    });
    
    [self updateTextFieldFrame];
}

-(void)updateTextFieldFrame{
    
    int width = initialTextFieldFrame.size.width - (kViewDeckPadding + 6);
    CGRect newFrame = CGRectMake (self.textField.frame.origin.x,
                                  self.textField.frame.origin.y,
                                  width,
                                  self.textField.frame.size.height);
    
    self.textField.frame = newFrame;
}

-(UITextField *)textField{
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass: [UITextField class]]){
            
            return (UITextField *)view;
        }
    }
    return nil;
}

-(void) setShowsScopeBar:(BOOL)showsScopeBar {
    [super setShowsScopeBar:YES]; //Initially make search bar appear with scope bar
}

@end