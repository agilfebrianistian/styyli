//
//  ViewDeckSearchBar.h
//  STYYLI
//
//  Created by Agil Febrianistian on 6/12/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewDeckSearchBar : UISearchBar

@end
