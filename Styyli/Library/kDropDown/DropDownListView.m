//
//  DropDownListView.m
//  KDropDownMultipleSelection
//
//  Created by macmini17 on 03/01/14.
//  Copyright (c) 2014 macmini17. All rights reserved.
//

#import "DropDownListView.h"
#import "DropDownViewCell.h"

#define DROPDOWNVIEW_SCREENINSET 0
#define DROPDOWNVIEW_HEADER_HEIGHT 50.
#define RADIUS 0


@interface DropDownListView (private)
- (void)fadeIn;
- (void)fadeOut;
@end
@implementation DropDownListView
@synthesize _kDropDownOption;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (id)initWithTitle:(NSString *)aTitle options:(NSArray *)aOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple
{
    isMultipleSelection=isMultiple;
    CGRect rect = CGRectMake(point.x, point.y,size.width,size.height);
    if (self = [super initWithFrame:rect])
    {
        
        isexpanded = false;
        
        self.backgroundColor = [UIColor clearColor];
        _kTitleText = [aTitle copy];
        
        _kDropDownOption = [[NSMutableArray alloc]initWithArray:aOptions];
        
//        _kDropDownOption = [aOptions copy];
        self.arryData=[[NSMutableArray alloc]init];
        _kTableView = [[UITableView alloc] initWithFrame:CGRectMake(DROPDOWNVIEW_SCREENINSET,
                                                                   DROPDOWNVIEW_SCREENINSET + DROPDOWNVIEW_HEADER_HEIGHT,
                                                                   rect.size.width - 2 * DROPDOWNVIEW_SCREENINSET,
                                                                   rect.size.height - 2 * DROPDOWNVIEW_SCREENINSET - DROPDOWNVIEW_HEADER_HEIGHT - RADIUS)];
        _kTableView.separatorColor = [UIColor colorWithWhite:1 alpha:.2];
        _kTableView.backgroundColor = [UIColor clearColor];
        _kTableView.dataSource = self;
        _kTableView.delegate = self;
        [self addSubview:_kTableView];
        if (isMultipleSelection) {
            UIButton *btnDone=[UIButton  buttonWithType:UIButtonTypeCustom];
            [btnDone setFrame:CGRectMake(rect.origin.x+182,rect.origin.y-45, 82, 31)];
            [btnDone setImage:[UIImage imageNamed:@"done@2x.png"] forState:UIControlStateNormal];
            [btnDone addTarget:self action:@selector(Click_Done) forControlEvents: UIControlEventTouchUpInside];
            [self addSubview:btnDone];
        }

        isLooping = NO;
        brandCounter = 1;
        
    }
    return self;
}

-(void)Click_Done{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(DropDownListView:Datalist:)]) {
        NSMutableArray *arryResponceData=[[NSMutableArray alloc]init];
        NSLog(@"%@",self.arryData);
        for (int k=0; k<self.arryData.count; k++) {
            NSIndexPath *path=[self.arryData objectAtIndex:k];
            [arryResponceData addObject:[_kDropDownOption objectAtIndex:path.row]];
            NSLog(@"pathRow=%d",path.row);
        }
    
        [self.delegate DropDownListView:self Datalist:arryResponceData];
        
    }
    // dismiss self
    [self fadeOut];
}
#pragma mark - Private Methods
- (void)fadeIn
{
    self.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.alpha = 0;
    [UIView animateWithDuration:.35 animations:^{
        self.alpha = 1;
        self.transform = CGAffineTransformMakeScale(1, 1);
    }];
    
}
- (void)fadeOut
{
    [UIView animateWithDuration:.35 animations:^{
        self.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
        }
    }];
}

#pragma mark - Instance Methods
- (void)showInView:(UIView *)aView animated:(BOOL)animated
{
    [aView addSubview:self];
    if (animated) {
        [self fadeIn];
    }
}

#pragma mark - Tableview datasource & delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
 
    if ([[postData sharedObject].step isEqualToString:@"2"]) {
        return [_kDropDownOption count]+1;
    }
    else{
        return [_kDropDownOption count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    
    static NSString *cellIdentity = @"DropDownViewCell";
    
    DropDownViewCell *cell =  (DropDownViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentity];
    
    
    if (cell == nil) {
        
        cell = [[DropDownViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentity] ;
        
    }
    
    if(indexPath.row >= _kDropDownOption.count && isLooping == FALSE)
    {
        brandCounter++;
                [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        [self performSelector:@selector(getBrands) withObject:nil afterDelay:2.0];
    }
    else
    {
        NSMutableArray  * items = [_kDropDownOption objectAtIndex:indexPath.row];
        
        UIImageView *imgarrow=[[UIImageView alloc]init ];
        
        if([self.arryData containsObject:indexPath]){
            imgarrow.frame=CGRectMake(230,2, 27, 27);
            imgarrow.image=[UIImage imageNamed:@"check_mark@2x.png"];
        } else
            imgarrow.image=nil;
        
        [cell addSubview:imgarrow];
        
        
        UIView *bgView = [[UIView alloc] init];
        bgView.backgroundColor = [UIColor grayColor];
        cell.selectedBackgroundView = bgView;
        cell.lblTitle.text = [items valueForKey:@"name"];
        cell.lblTitle.textColor = [UIColor whiteColor];
        
        [cell setIndentationLevel:[[[_kDropDownOption objectAtIndex:indexPath.row] valueForKey:@"level"] intValue]];
        cell.indentationWidth = 25;
        
        float indentPoints = cell.indentationLevel * cell.indentationWidth;
        
        cell.contentView.frame = CGRectMake(indentPoints,cell.contentView.frame.origin.y,cell.contentView.frame.size.width - indentPoints,cell.contentView.frame.size.height);
        
        
        if([items valueForKey:@"categories"])
        {
            cell.btnExpand.alpha = 1.0;
            [cell.btnExpand addTarget:self action:@selector(showSubItems:) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            cell.btnExpand.alpha = 0.0;
        }
        
        
    }
            return cell;

}


-(void)CollapseRows:(NSArray*)ar
{
    
    for(NSDictionary *dInner in ar )
    {
		NSUInteger indexToRemove=[_kDropDownOption indexOfObjectIdenticalTo:dInner];
		NSArray *arInner=[dInner valueForKey:@"categories"];
		if(arInner && [arInner count]>0)
        {
			[self CollapseRows:arInner];
		}
		
		if([_kDropDownOption indexOfObjectIdenticalTo:dInner]!=NSNotFound)
        {
			[_kDropDownOption removeObjectIdenticalTo:dInner];
			[_kTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:
                                                        [NSIndexPath indexPathForRow:indexToRemove inSection:0]
                                                        ]
                                      withRowAnimation:UITableViewRowAnimationLeft];
        }
	}
    
}


-(void)showSubItems :(id) sender
{
    UIButton *btn = (UIButton*)sender;
    CGRect buttonFrameInTableView = [btn convertRect:btn.bounds toView:_kTableView];
    NSIndexPath *indexPath = [_kTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    if(btn.alpha==1.0)
    {
        if ([[btn imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"down-arrow.png"]])
        {
            [btn setImage:[UIImage imageNamed:@"up-arrow.png"] forState:UIControlStateNormal];
        }
        else
        {
            [btn setImage:[UIImage imageNamed:@"down-arrow.png"] forState:UIControlStateNormal];
        }
        
    }
    
    NSDictionary *d=[_kDropDownOption objectAtIndex:indexPath.row] ;
    
    NSArray *arr=[[d valueForKey:@"categories"] valueForKey:@"category"];
    
    
    if([d valueForKey:@"categories"])
    {
        BOOL isTableExpanded=NO;
        for(NSDictionary *subitems in arr )
        {
            NSInteger index=[_kDropDownOption indexOfObjectIdenticalTo:subitems];
            isTableExpanded=(index>0 && index!=NSIntegerMax);
            if(isTableExpanded) break;
        }
        
        if(isTableExpanded)
        {
            [self CollapseRows:arr];
        }
        else
        {
            NSUInteger count=indexPath.row+1;
            NSMutableArray *arrCells=[NSMutableArray array];
            for(NSDictionary *dInner in arr )
            {
                [arrCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
                [_kDropDownOption insertObject:dInner atIndex:count++];
            }
            [_kTableView insertRowsAtIndexPaths:arrCells withRowAnimation:UITableViewRowAnimationLeft];
        }
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (isMultipleSelection) {
        if([self.arryData containsObject:indexPath]){
            [self.arryData removeObject:indexPath];
        } else {
            [self.arryData addObject:indexPath];
        }
        [tableView reloadData];

    }
    else{
    
        
        NSMutableArray  * items = [_kDropDownOption objectAtIndex:indexPath.row];
        
        if([items valueForKey:@"categories"])
        {
            
            NSArray *arr=[[items valueForKey:@"categories"] valueForKey:@"category"];
            
            BOOL isTableExpanded=NO;
            
            for(NSDictionary *subitems in arr )
            {
                NSInteger index=[_kDropDownOption indexOfObjectIdenticalTo:subitems];
                isTableExpanded=(index>0 && index!=NSIntegerMax);
                if(isTableExpanded) break;
            }
            
            if(isTableExpanded)
            {
                [self CollapseRows:arr];
            }
            else
            {
                NSUInteger count=indexPath.row+1;
                NSMutableArray *arrCells=[NSMutableArray array];
                for(NSDictionary *dInner in arr )
                {
                    
                    [arrCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
                    [_kDropDownOption insertObject:dInner atIndex:count++];
                }
                [_kTableView insertRowsAtIndexPaths:arrCells withRowAnimation:UITableViewRowAnimationLeft];
            }
        }
        else
        {
            
            if ([[postData sharedObject].step isEqualToString:@"1"]) {
                
                [postData sharedObject].categoryId = [items valueForKey:@"id"];
                [[CSActivityViewController sharedObject] showActivityView];
                
                [self getBrands];
                
            }
            
            else if ([[postData sharedObject].step isEqualToString:@"2"]) {
                
                [postData sharedObject].brandId = [items valueForKey:@"id"];
                
                [[CSActivityViewController sharedObject] showActivityView];
                [self getItems];
                
            }
            
            else
            {
                [postData sharedObject].itemName = [items valueForKey:@"name"];
                [postData sharedObject].itemId = [items valueForKey:@"id"];
                if (self.delegate && [self.delegate respondsToSelector:@selector(DropDownListView:didSelectedIndex:)]) {
                    [self.delegate DropDownListView:self didSelectedIndex:[indexPath row]-1];
                }
                // dismiss self
                [self fadeOut];
            }
        }
    }
	
}

#pragma mark - TouchTouchTouch
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    // tell the delegate the cancellation
    
    
}

#pragma mark - DrawDrawDraw
- (void)drawRect:(CGRect)rect
{
    CGRect bgRect = CGRectInset(rect, DROPDOWNVIEW_SCREENINSET, DROPDOWNVIEW_SCREENINSET);
    CGRect titleRect = CGRectMake(DROPDOWNVIEW_SCREENINSET + 10, DROPDOWNVIEW_SCREENINSET + 10 + 5,
                                  rect.size.width -  2 * (DROPDOWNVIEW_SCREENINSET + 10), 30);
    CGRect separatorRect = CGRectMake(DROPDOWNVIEW_SCREENINSET, DROPDOWNVIEW_SCREENINSET + DROPDOWNVIEW_HEADER_HEIGHT - 2,
                                      rect.size.width - 2 * DROPDOWNVIEW_SCREENINSET, 2);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    // Draw the background with shadow
    // Draw the background with shadow
    CGContextSetShadowWithColor(ctx, CGSizeZero, 6., [UIColor colorWithWhite:0 alpha:1.0].CGColor);
    [[UIColor colorWithRed:R/255 green:G/255 blue:B/255 alpha:A] setFill];
    
    float x = DROPDOWNVIEW_SCREENINSET;
    float y = DROPDOWNVIEW_SCREENINSET;
    float width = bgRect.size.width;
    float height = bgRect.size.height;
    CGMutablePathRef path = CGPathCreateMutable();
	CGPathMoveToPoint(path, NULL, x, y + RADIUS);
	CGPathAddArcToPoint(path, NULL, x, y, x + RADIUS, y, RADIUS);
	CGPathAddArcToPoint(path, NULL, x + width, y, x + width, y + RADIUS, RADIUS);
	CGPathAddArcToPoint(path, NULL, x + width, y + height, x + width - RADIUS, y + height, RADIUS);
	CGPathAddArcToPoint(path, NULL, x, y + height, x, y + height - RADIUS, RADIUS);
	CGPathCloseSubpath(path);
	CGContextAddPath(ctx, path);
    CGContextFillPath(ctx);
    CGPathRelease(path);
    
    // Draw the title and the separator with shadow

    CGContextSetShadowWithColor(ctx, CGSizeMake(0, 1), 0.5f, [UIColor blackColor].CGColor);
    [[UIColor colorWithWhite:1 alpha:1.] setFill];
    
    if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)) {
        UIFont *font = [UIFont fontWithName:@"Helvetica" size:16.0];
        UIColor *cl=[UIColor whiteColor];
        
        
        NSDictionary *attributes = @{ NSFontAttributeName: font,NSForegroundColorAttributeName:cl};
        
        [_kTitleText drawInRect:titleRect withAttributes:attributes];
    }
    else
        [_kTitleText drawInRect:titleRect withFont:[UIFont systemFontOfSize:16.]];
    
    CGContextFillRect(ctx, separatorRect);
    
}
-(void)SetBackGroundDropDwon_R:(CGFloat)r G:(CGFloat)g B:(CGFloat)b alpha:(CGFloat)alph{
    R=r;
    G=g;
    B=b;
    A=alph;
}

- (void)getBrands
{
     _kDropDownOption = [[NSMutableArray alloc]init];
    [_kDropDownOption removeAllObjects];
    
    
    NSLog(@"%d",brandCounter);
    
    
    for (int i = 1; i<=brandCounter; i++) {
        
        isLooping = TRUE;
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        NSDictionary *params = @{@"api_key": API_KEY,
                                 @"auth_token": [UserInfo sharedObject].tokenUser,
                                 @"page":[NSString stringWithFormat:@"%d",i]};
        
        [manager GET:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_BRANDS] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSArray *results = [(NSDictionary *)responseObject objectForKey:@"brands"];
            
            for (NSDictionary * items in results) {
                NSDictionary * item = [items valueForKey:@"brand"];
                
                [_kDropDownOption addObject:item];
            }
            
            [postData sharedObject].step = @"2";
            

            
            if (i == brandCounter) {
                [_kTableView reloadData];
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                [[CSActivityViewController sharedObject] hideActivityView];
                
            }
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            NSLog(@"Error: %@", error);
            [postData sharedObject].step = @"0";
            [[CSActivityViewController sharedObject] hideActivityView];
            
            UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [message show];
            
        }];
    }
    
    isLooping = FALSE;
    
}

- (void)getItems
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token": [UserInfo sharedObject].tokenUser,
                             @"category_id": [postData sharedObject].categoryId,
                             @"brand_id": [postData sharedObject].brandId};
    
    [manager GET:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_ITEMS] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSLog(@"%@",responseObject);
        
        NSArray *results = [(NSDictionary *)responseObject objectForKey:@"items"];
        
        _kDropDownOption = [[NSMutableArray alloc]init];
        
        for (NSDictionary * items in results) {
            NSDictionary * item = [items valueForKey:@"item"];
            
            [_kDropDownOption addObject:item];
        }
        
        NSLog(@"%@",_kDropDownOption);
        
        [postData sharedObject].step = @"3";
        
        
        [[CSActivityViewController sharedObject] hideActivityView];
        
        [_kTableView reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [postData sharedObject].step = @"0";
        [[CSActivityViewController sharedObject] hideActivityView];
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}

@end
