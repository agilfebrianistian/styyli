//
//  CSActivityViewControllerViewController.h
//  SEAKR
//
//  Created by Agil Febrianistian on 3/11/13.
//  Copyright (c) 2013 Agil Febrianistian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSActivityViewController : UIViewController

@property (retain, nonatomic) IBOutlet UIView *activityContainer;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

+(CSActivityViewController *)sharedObject;
-(void)showActivityView;
-(void)hideActivityView;
@end

