//
//  CategoryContainerCell.m
//  STYYLI
//
//  Created by Agil Febrianistian on 2/10/15.
//  Copyright (c) 2015 Mirosea. All rights reserved.
//

#import "CategoryContainerCell.h"

@implementation CategoryContainerCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
