//
//  UserInfo.h
//  Applist
//
//  Created by Gean Ginanjar on 2/5/14.
//  Copyright (c) 2014 Agil Febrianistian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserInfo : NSObject

@property (nonatomic, copy) NSString* tokenUser;
@property (nonatomic, copy) NSString* emailUser;
@property (nonatomic, copy) NSString* idUser;
@property (nonatomic, copy) NSString* firstNameUser;
@property (nonatomic, copy) NSString* lastNameUser;
@property (nonatomic, copy) NSString* birthdayUser;
@property (nonatomic, copy) NSString* cityUser;
@property (nonatomic, copy) NSString* countryUser;
@property (nonatomic, copy) NSString* genderUser;
@property (nonatomic, copy) NSString* usernameUser;
@property (nonatomic, copy) NSString* avatarUser;
@property (nonatomic, copy) NSString* phoneUser;
@property (nonatomic, copy) NSString* zipcodeUser;


+(UserInfo*)sharedObject;
-(void)showLog;
@end
