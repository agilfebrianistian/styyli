 //
//  main.m
//  Styyli
//
//  Created by Reebonz on 12/26/13.
//  Copyright (c) 2013 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RHZAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RHZAppDelegate class]));
    }
}
