//
//  UserInfo.m
//  Applist
//
//  Created by Gean Ginanjar on 2/5/14.
//  Copyright (c) 2014 Agil Febrianistian. All rights reserved.
//

#import "UserInfo.h"

@implementation UserInfo
@synthesize tokenUser, emailUser, idUser;

static UserInfo* sharedObject = nil;

+(UserInfo*)sharedObject{
    @synchronized(self){
        if (sharedObject ==nil) {
            sharedObject = [[self alloc]init];
        }
    }
    return sharedObject;
}

-(void)showLog{
    NSLog(@"%@ %@ %@", self.tokenUser, self.emailUser, self.idUser);
}

@end
