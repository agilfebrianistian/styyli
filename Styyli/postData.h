//
//  postData.h
//  STYYLI
//
//  Created by Agil Febrianistian on 4/12/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface postData : NSObject


@property (nonatomic, copy) NSString* categoryId;
@property (nonatomic, copy) NSString* brandId;
@property (nonatomic, copy) NSString* itemId;
@property (nonatomic, copy) NSString* itemName;
@property (nonatomic, copy) NSData* pictureData;
@property (nonatomic, copy) NSString* step;


+(postData*)sharedObject;

@end
