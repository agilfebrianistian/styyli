//
//  RHZAppDelegate.h
//  Styyli
//
//  Created by Reebonz on 12/23/13.
//  Copyright (c) 2013 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RHZRootViewController.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "IFRootViewController.h"
#import "GTScrollNavigationBar.h"
#import "RHZProfileViewController.h"
#import "IIViewDeckController.h"
#import <FacebookSDK/FacebookSDK.h>
#import <Twitter/Twitter.h>
#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>



@class ICETutorialController;

@interface RHZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *profileController;
@property (strong, nonatomic) UINavigationController *timelineController;
@property (strong, nonatomic) UINavigationController *newsController;
@property (strong, nonatomic) UINavigationController *shopController;
@property (strong, nonatomic) UINavigationController *homeController;
@property (strong, nonatomic) UINavigationController *settingsController;
@property (strong, nonatomic) UINavigationController *stylePinController;
@property (strong, nonatomic) UINavigationController *collectionController;

@property (strong, nonatomic) UINavigationController *navController;
@property (strong, nonatomic) ICETutorialController *viewController;
@property (strong, nonatomic) RHZRootViewController *rootviewController;
@property (strong, nonatomic) IFRootViewController *IFRootviewController;

@property (strong, nonatomic) UIViewController *loginviewController;
@property (strong, nonatomic) UIViewController *registerviewController;

- (void)transitToHomeScreen;
- (void)transitToTutorialScreen;

@end
