//
//  RegisterViewController.m
//  Styyli
//
//  Created by Agil Febrianistian on 2/7/14.
//  Copyright (c) 2014 Agil Febrianistian. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController
@synthesize emailTextfield,usernameTextfield,passwordTextfield,firstNameTextfield,lastNameTextfield,phoneTextfield,birthdayTextfield,countryTextfield,genderSegment,pictureButtonImage;
@synthesize pickerInput,accessoryView,customInput;
@synthesize pickerArray,countryList,scroller;
@synthesize emailWarning,usernameWarning,passwordWarning,topWarning,firstnameWarning,lastnameWarning;
@synthesize emailChecklist,usernameChecklist;
@synthesize activityIndicator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (UIPickerView *)pickerInput {
    if (pickerInput == nil ) {
        pickerInput = [[UIPickerView alloc] init];
        pickerInput.delegate = self;
        pickerInput.dataSource = self;
        pickerInput.showsSelectionIndicator = YES;
        
    }
    return pickerInput;
}


- (UIToolbar *)accessoryView {
    if (accessoryView == nil ) {
        accessoryView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        accessoryView.barStyle = UIBarStyleDefault;
        UIBarButtonItem * doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(onLocationSelection)];
        doneButton.tintColor = [UIColor blackColor];
        [accessoryView setItems:[NSArray arrayWithObject:doneButton]];
    }
    
    return accessoryView;
}

- (IBAction)dateChanged:(id)sender {
    UIDatePicker *picker = (UIDatePicker *)sender;
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd-MM-yyyy"];
    
    NSString *entryDateInString = [outputFormatter stringFromDate:picker.date];
    
    [[self birthdayTextfield] setText: entryDateInString];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];

    CGFloat screenHeight = screenRect.size.height;
    
    if (screenHeight > 480)
       
        [scroller setScrollEnabled:false];
    else
        [scroller setScrollEnabled:true];
    
    [scroller setContentSize:CGSizeMake(320,568)];
    
    UIEdgeInsets leftEdges = UIEdgeInsetsMake(0, -10, 0, 10);
    UIImage *leftImage = [UIImage imageNamed:@"IconBackToTimeline"];
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton addTarget:self action:@selector(leftBarPressed:) forControlEvents:UIControlEventTouchUpInside];
    leftButton.bounds = CGRectMake( 0, 0, leftImage.size.width,  leftImage.size.height);
    [leftButton setImage:leftImage forState:UIControlStateNormal];
    [leftButton setImageEdgeInsets:leftEdges];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];

    
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc] initWithTitle:@"Join" style:UIBarButtonItemStyleBordered target:self action:@selector(rightBarPressed:)];
    
    [self.navigationItem setLeftBarButtonItem:leftItem];
    [self.navigationItem setRightBarButtonItem:searchButton animated:YES];
    
    CGRect frame = CGRectMake(0, 0, 0, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:19.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"Create Account";
    self.navigationItem.titleView = label;
    
    self.navigationItem.title = label.text;
   
    [self.navigationController setNavigationBarHidden:NO animated:TRUE];

    // country picker initiation
    NSLocale *locale = [NSLocale currentLocale];
    NSArray *countryArray = [NSLocale ISOCountryCodes];
    
    NSMutableArray *sortedCountryArray = [[NSMutableArray alloc] init];
    
    for (NSString *countryCode in countryArray) {
        NSString *displayNameString = [locale displayNameForKey:NSLocaleCountryCode value:countryCode];
        [sortedCountryArray addObject:displayNameString];
        
    }
    
    [sortedCountryArray sortUsingSelector:@selector(localizedCompare:)];
    
    countryList = [[NSArray alloc]initWithArray:sortedCountryArray];
    countryTextfield.inputView             = self.pickerInput;
    countryTextfield.inputAccessoryView    = self.accessoryView;
    
    //date picker initiation
    
    customInput = [[UIDatePicker alloc]init];
    [customInput setDatePickerMode:UIDatePickerModeDate];
    [customInput addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    
    birthdayTextfield.inputView             = self.customInput;
    birthdayTextfield.inputAccessoryView    = self.accessoryView;
    

    isEmailAvailable = TRUE;
    isUsernameAvailable = TRUE;


}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.view setAlpha:0];
    [UIView animateWithDuration:0.2
                          delay:0.1
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [self.view setAlpha:1.0];
                     }completion:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

    
}


- (IBAction) leftBarPressed:(id)sender
{
    NSLog(@"cancel");
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

    RHZAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    [appDelegate transitToTutorialScreen];
    
}

-(void)shake:(UIView *)theOneYouWannaShake
{
    [UIView animateWithDuration:0.03 animations:^
     {
         theOneYouWannaShake.transform = CGAffineTransformMakeTranslation(5*direction, 0);
     }
                     completion:^(BOOL finished)
     {
         if(shakes >= 10)
         {
             theOneYouWannaShake.transform = CGAffineTransformIdentity;
             return;
         }
         shakes++;
         direction = direction * -1;
         [self shake:theOneYouWannaShake];
     }];
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


- (IBAction) rightBarPressed:(id)sender
{
    [self dismissKeyboard];
    
    if (emailTextfield.text.length == 0 || usernameTextfield.text.length == 0 ||passwordTextfield.text.length == 0 || firstNameTextfield.text.length == 0 || lastNameTextfield.text.length == 0) {
        direction = 1;
        shakes = 0;
        [self shake:self.view];
        
        if (emailTextfield.text.length == 0) {
            emailWarning.text = @"*Please input your Email";
            emailTextfield.placeholder = @"";
            [emailWarning setHidden:NO];
        }
        if (usernameTextfield.text.length == 0) {
            usernameWarning.text = @"*Please input your Username";
             usernameTextfield.placeholder = @"";
            [usernameWarning setHidden:NO];
        }
        if (firstNameTextfield.text.length == 0) {
            firstnameWarning.text = @"*Please input your First Name";
            firstNameTextfield.placeholder = @"";
            [firstnameWarning setHidden:NO];
        }
        if (lastNameTextfield.text.length == 0) {
            lastnameWarning.text = @"*Please input your Last Name";
            lastNameTextfield.placeholder = @"";
            [lastnameWarning setHidden:NO];
        }
        
        if (passwordTextfield.text.length == 0) {
            passwordWarning.text = @"*Please input your Password";
            passwordTextfield.placeholder = @"";
            [passwordWarning setHidden:NO];
        }
        else if (passwordTextfield.text.length < 8)
        {
            passwordTextfield.placeholder = @"";
            passwordWarning.text = @"Password cannot be less than 8 characters";
            [passwordWarning setHidden:NO];
        }
    }
    
    else if (passwordTextfield.text.length < 8)
    {
        direction = 1;
        shakes = 0;
        [self shake:self.view];
        
        passwordTextfield.placeholder = @"";
        passwordWarning.text = @"Password cannot be less than 8 characters";
        [passwordWarning setHidden:NO];
    }
    else if (!isEmailAvailable || !isUsernameAvailable)
    {
        direction = 1;
        shakes = 0;
        [self shake:self.view];
    
    
    }
    
    else
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
        [self navigationItem].rightBarButtonItem = barButton;
        [activityIndicator startAnimating];
        
        [self doLogin];
    }
}



#pragma mark - URL REQUEST

-(void)doLogin{
    
    
    genderName = [NSString stringWithFormat:@"%ld",(long)[genderSegment selectedSegmentIndex]];
    
    NSString * deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"];
    if (deviceToken.length == 0) {
        deviceToken = @"";
    }

        
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key":API_KEY,
                             @"user[email]"     : emailTextfield.text,
                             @"user[password]"  : passwordTextfield.text,
                             @"user[password_confirmation]": passwordTextfield.text,
                             @"user[username]"  : usernameTextfield.text,
                             @"user[country]"   : countryTextfield.text,
                             @"user[first_name]": firstNameTextfield.text,
                             @"user[last_name]" : lastNameTextfield.text,
                             @"user[phone]"     : phoneTextfield.text,
                             @"user[birthday]"  : birthdayTextfield.text,
                             @"user[gender]"    : genderName,
                             @"device_id"       : deviceToken
                             };
    
    [manager POST:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_REGISTER] parameters:params
    constructingBodyWithBlock:^(id<AFMultipartFormData> formData){
        
        if (imageData != NULL) {
                [formData appendPartWithFileData:imageData name:@"user[image]" fileName:@"photoname.jpg" mimeType:@"image/jpeg"];
            
        }
    }
    success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        
        NSLog(@"JSON: %@", responseObject);
        
        if ([[responseObject valueForKey:@"success"]integerValue] == 1) {
            [activityIndicator stopAnimating];
            
            NSDictionary * userItem =[responseObject valueForKey:@"user"];
            
            NSLog(@"%@", [userItem valueForKey:@"first_name"]);
            
            [UserInfo sharedObject].tokenUser = [responseObject valueForKey:@"auth_token"];
            
            [UserInfo sharedObject].firstNameUser   = [userItem valueForKey:@"first_name"];
            [UserInfo sharedObject].lastNameUser    = [userItem valueForKey:@"last_name"];
            [UserInfo sharedObject].birthdayUser    = [userItem valueForKey:@"birthday"];
            [UserInfo sharedObject].cityUser        = [userItem valueForKey:@"city"];
            [UserInfo sharedObject].countryUser     = [userItem valueForKey:@"country"];
            [UserInfo sharedObject].genderUser      = [userItem valueForKey:@"gender"];
            [UserInfo sharedObject].usernameUser    = [userItem valueForKey:@"username"];
            [UserInfo sharedObject].emailUser       = [userItem valueForKey:@"email"];
            [UserInfo sharedObject].idUser          = [userItem valueForKey:@"id"];
            [UserInfo sharedObject].phoneUser       = [userItem valueForKey:@"phone"];
            [UserInfo sharedObject].zipcodeUser     = [userItem valueForKey:@"zipcode"];
            
//            NSDictionary * urlImage =[userItem valueForKey:@"image"];
//            [UserInfo sharedObject].avatarUser      = [urlImage valueForKey:@"url"];
            
            [UserInfo sharedObject].avatarUser      = [userItem valueForKey:@"image_small"];
            
            NSUserDefaults *login = [NSUserDefaults standardUserDefaults];
            [login setValue:[UserInfo sharedObject].tokenUser forKey:@"loginAuth"];
            [login synchronize];
            
            UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"" message:@"Welcome To STYYLI" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [message show];
            
            RHZAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
            [appDelegate transitToHomeScreen];
            
        }
        else
        {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [activityIndicator stopAnimating];
            
            UIBarButtonItem *searchButton = [[UIBarButtonItem alloc] initWithTitle:@"Join" style:UIBarButtonItemStyleBordered target:self action:@selector(rightBarPressed:)];
            [self.navigationItem setRightBarButtonItem:searchButton animated:YES];
            
            UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [message show];
        
        }
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [activityIndicator stopAnimating];
        
        UIBarButtonItem *searchButton = [[UIBarButtonItem alloc] initWithTitle:@"Join" style:UIBarButtonItemStyleBordered target:self action:@selector(rightBarPressed:)];
        [self.navigationItem setRightBarButtonItem:searchButton animated:YES];
        
        NSLog(@"Error: %@", error);
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
    }];
 
}

-(void)checkUsername : (NSString*)username {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key":API_KEY,
                             @"username": username,
                             };
    
    
                    NSLog(@"%@", [NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_CHECK_USERNAME]);
    
    [manager GET:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_CHECK_USERNAME] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {

        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
                NSLog(@"%@", responseObject);
        
        
        if ([[responseObject valueForKey:@"success"]integerValue] == 1) {
            
            isUsernameAvailable = TRUE;
            
            if(usernameChecklist.hidden == true)
            {
                usernameChecklist.hidden = false;
                [UIView animateWithDuration:0.5
                                      delay:0.0
                                    options:UIViewAnimationOptionCurveEaseIn
                                 animations:^ {
                                     usernameChecklist.alpha = 1.0;
                                 }
                                 completion:^(BOOL finished){
                                     
                                 }];
            }
        }
        else
        {
            
            isUsernameAvailable = FALSE;
        topWarning.text = [responseObject valueForKey:@"message"];
        
        if(topWarning.hidden == true)
        {
            topWarning.hidden = false;
            [UIView animateWithDuration:0.5
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseIn
                             animations:^ {
                                 topWarning.alpha = 1.0;
                             }
                             completion:^(BOOL finished){
                                 
                             }];
        }
        }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
  
              
              if (!AFNetworkReachabilityStatusNotReachable) {
                [self checkUsername:usernameTextfield.text];
              }

              
          }];
    
}

-(void)checkEmail : (NSString*)email {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key":API_KEY,
                             @"email": email,
                             };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_CHECK_EMAIL] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
                NSLog(@"%@", responseObject);
        
        if ([[responseObject valueForKey:@"success"]integerValue] == 1) {
            isEmailAvailable = TRUE;
            
            if(emailChecklist.hidden == true)
            {
                emailChecklist.hidden = false;
                [UIView animateWithDuration:0.5
                                      delay:0.0
                                    options:UIViewAnimationOptionCurveEaseIn
                                 animations:^ {
                                     emailChecklist.alpha = 1.0;
                                 }
                                 completion:^(BOOL finished){
                                     
                                 }];
            }
        }
        else
        {
            isEmailAvailable = FALSE;
            
        topWarning.text = [responseObject valueForKey:@"message"];
        if(topWarning.hidden == true)
        {
            topWarning.hidden = false;
            [UIView animateWithDuration:0.5
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseIn
                             animations:^ {
                                 topWarning.alpha = 1.0;
                             }
                             completion:^(BOOL finished){

                             }];
        }
        }
        
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              if (!AFNetworkReachabilityStatusNotReachable) {
                [self checkEmail:emailTextfield.text];
              }

          }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - picker view delegate/datasource

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [pickerArray objectAtIndex:row];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [pickerArray count];
}


#pragma mark - Keyboard delegate

- (IBAction)bgTapped:(id)sender {
    [self dismissKeyboard];
}

-(void)dismissKeyboard {
    if ([emailTextfield isFirstResponder])
        [emailTextfield resignFirstResponder];
    else if ([usernameTextfield isFirstResponder])
        [usernameTextfield resignFirstResponder];
    else if ([passwordTextfield isFirstResponder])
        [passwordTextfield resignFirstResponder];
    else if ([firstNameTextfield isFirstResponder])
        [firstNameTextfield resignFirstResponder];
    else if ([lastNameTextfield isFirstResponder])
        [lastNameTextfield resignFirstResponder];
    else if ([phoneTextfield isFirstResponder])
        [phoneTextfield resignFirstResponder];
    else if ([birthdayTextfield isFirstResponder])
        [birthdayTextfield resignFirstResponder];
    else if ([countryTextfield isFirstResponder])
    {
        [self onLocationSelection];
        [countryTextfield resignFirstResponder];
    }
}

- (void)onLocationSelection {
    NSInteger row = [self.pickerInput selectedRowInComponent:0];
    
    if ( [countryTextfield isFirstResponder] ) {
        countryTextfield.text = [pickerArray objectAtIndex:row];
        [self textFieldShouldReturn:countryTextfield];
    }
    else if ([birthdayTextfield isFirstResponder])
    {
        
        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
        [outputFormatter setDateFormat:@"dd-MM-yyyy"];
        
        NSString *entryDateInString = [outputFormatter stringFromDate:customInput.date];
        
        [[self birthdayTextfield] setText: entryDateInString];
        
        [self textFieldShouldReturn:birthdayTextfield];
    }
}

#pragma mark - Textfield Animation

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if ([emailTextfield isFirstResponder]) {
        
        if(emailChecklist.hidden == false)
        {
            
            [UIView animateWithDuration:0.5
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^ {
                                 emailChecklist.alpha = 0.0;
                             }
                             completion:^(BOOL finished){
                                 emailChecklist.hidden = true;
                             }];
        }
        
        if(topWarning.hidden == false)
        {
            
            [UIView animateWithDuration:0.5
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^ {
                                 topWarning.alpha = 0.0;
                             }
                             completion:^(BOOL finished){
                                 topWarning.hidden = true;
                             }];
        }
        
        emailTextfield.placeholder = @"Email Address";
        [emailWarning setHidden:YES];
    }
    else if ([usernameTextfield isFirstResponder]) {
        
        if(usernameChecklist.hidden == false)
        {
            
            [UIView animateWithDuration:0.5
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^ {
                                 usernameChecklist.alpha = 0.0;
                             }
                             completion:^(BOOL finished){
                                 usernameChecklist.hidden = true;
                             }];
        }
        
        if(topWarning.hidden == false)
        {
            
            [UIView animateWithDuration:0.5
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^ {
                                 topWarning.alpha = 0.0;
                             }
                             completion:^(BOOL finished){
                                 topWarning.hidden = true;
                             }];
        }
        
        usernameTextfield.placeholder = @"Username";
        [usernameWarning setHidden:YES];
    }
    else if ([passwordTextfield isFirstResponder]) {
        passwordTextfield.placeholder = @"Password (min. 8 chars)";
        [passwordWarning setHidden:YES];
    }

    else if ([firstNameTextfield isFirstResponder])
    {
        firstNameTextfield.placeholder = @"First Name";
        [firstnameWarning setHidden:YES];
        
    }
    
    else if ([lastNameTextfield isFirstResponder])
    {
        lastNameTextfield.placeholder = @"Last Name";
        [lastnameWarning setHidden:YES];
        
    }
    
    
    if (pickerArray != NULL) {
        [pickerArray removeAllObjects];
    }
    
    if ( [countryTextfield isFirstResponder] ) {

        pickerArray = [[NSMutableArray alloc]initWithArray:countryList];
    }
    
    [pickerInput reloadAllComponents];
    if ([firstNameTextfield isFirstResponder] || [lastNameTextfield isFirstResponder] || [phoneTextfield isFirstResponder] || [birthdayTextfield isFirstResponder] || [countryTextfield isFirstResponder]) {
        
        if (!isUp) {
            [self animateTextField:textField up: YES];
            isUp = YES;
        }
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == emailTextfield) {
        if (![self NSStringIsValidEmail:emailTextfield.text]) {
            emailWarning.text = @"*Please insert a valid Email address.";
            emailTextfield.placeholder = @"";
            [emailWarning setHidden:NO];
        }
        else
        {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            [self checkEmail:emailTextfield.text];
            
        }
    }
    
    else if (textField == usernameTextfield) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        [self checkUsername:usernameTextfield.text];
        
    }
    
    else if (textField == passwordTextfield) {
        if (passwordTextfield.text.length < 8)
        {
            passwordTextfield.placeholder = @"";
            passwordWarning.text = @"Password cannot be less than 8 characters";
            [passwordWarning setHidden:NO];
        }
    }
    
    
    if (isUp == YES) {
        [self animateTextField: textField up: NO];
        isUp = NO;
    }
    
}


- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 120; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if (textField == emailTextfield) {
        if (![self NSStringIsValidEmail:emailTextfield.text]) {
            emailWarning.text = @"*Please insert a valid Email address.";
            emailTextfield.placeholder = @"";
            [emailWarning setHidden:NO];
        }
        else
        {
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            [self checkEmail:emailTextfield.text];
        
        }
    }
    
    else if (textField == usernameTextfield) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            [self checkUsername:usernameTextfield.text];
        
    }
    
    else if (textField == passwordTextfield) {
        if (passwordTextfield.text.length < 8)
        {
            passwordTextfield.placeholder = @"";
            passwordWarning.text = @"Password cannot be less than 8 characters";
            [passwordWarning setHidden:NO];
        }
    }
    
    NSInteger nextTag = textField.tag + 1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    
    return NO;
}

#pragma mark - Picture Button Delegate

- (IBAction)pictureButton:(id)sender {
    [self dismissKeyboard];
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Upload Image"
                              message:@""
                              delegate:self
                              cancelButtonTitle:@"Cancel"
                              otherButtonTitles:  @"Camera",@"Gallery", nil];
    
    alertView.transform = CGAffineTransformTranslate( alertView.transform, 0.0, 100.0 );
    
    [alertView show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqual:[NSString stringWithFormat:@"Camera"]])
    {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = self;
        [self presentViewController:picker animated:YES completion:NULL];
 
    }
    else if([title isEqualToString:@"Gallery"])
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.delegate = self;
        [self presentViewController:picker animated:YES completion:NULL];

    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info; {

    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];

        [pictureButtonImage setBackgroundImage:image forState:UIControlStateNormal];

    
    [picker dismissViewControllerAnimated:YES completion:^{
        [self openEditor:nil];
        
    }];

}


- (IBAction)openEditor:(id)sender
{
    
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    [controller setCropAspectRatio:1];
    [controller setKeepingCropAspectRatio:true];
    
    controller.image = [pictureButtonImage backgroundImageForState:UIControlStateNormal];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:navigationController animated:YES completion:NULL];
    
}

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    
    
    [pictureButtonImage setBackgroundImage:croppedImage forState:UIControlStateNormal];
    
    NSString  *pngPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@.jpg",@"photoname"]];
    
    [UIImageJPEGRepresentation(croppedImage, 0) writeToFile:pngPath atomically:NO];
    
    imageName = pngPath;
    
    imageData = UIImageJPEGRepresentation([pictureButtonImage backgroundImageForState:UIControlStateNormal], 0.5);
    
}


- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}


@end
