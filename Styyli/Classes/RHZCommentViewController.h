//
//  RHZCommentViewController.h
//  Styyli
//
//  Created by R. Hafidhullah Zakariyya on 12/29/13.
//  Copyright (c) 2013 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAKeyboardControl.h"
#import "AFNetworking.h"
#import "CSActivityViewController.h"
#import "CommentCell.h"
#import "UIImageView+AFNetworking.h"
#import "Constant.h"
#import "UserInfo.h"
#import <CoreLocation/CoreLocation.h>
#import "RHZAppDelegate.h"

@interface RHZCommentViewController : UIViewController<UITableViewDelegate, UITableViewDataSource,CLLocationManagerDelegate>
{
    NSString *locationText;
    CLLocationManager *locationManager;
}

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

@property (weak, nonatomic) IBOutlet UITableView *commentTableView;

@property (strong, nonatomic) NSString* imageURL;

@property (nonatomic) int currId;

@property (strong, nonatomic) NSMutableArray* allTableData;


@property (strong, nonatomic) IBOutlet UITextField *textField;


@property (nonatomic) int commentType;





@end
