//
//  NotificationCell.h
//  STYYLI
//
//  Created by Agil Febrianistian on 9/17/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *notificationLabel;
@property (weak, nonatomic) IBOutlet UILabel *notificationTime;
@property (weak, nonatomic) IBOutlet UIImageView *notificationAvatar;
@property (weak, nonatomic) IBOutlet UIImageView *notificationMarker;
@property (weak, nonatomic) IBOutlet UIImageView *notificationImage;

@end
