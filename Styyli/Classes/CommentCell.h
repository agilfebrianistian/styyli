//
//  comentCell.h
//  Styyli
//
//  Created by Agil Febrianistian on 2/14/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AMAttributedHighlightLabel.h"
#import "AFNetworking.h"
#import "Constant.h"
#import "UserInfo.h"

@interface CommentCell : UITableViewCell<AMAttributedHighlightLabelDelegate>

@property (weak, nonatomic) IBOutlet AMAttributedHighlightLabel *contentText;
@property (weak, nonatomic) IBOutlet UILabel *timeText;
@property (weak, nonatomic) IBOutlet UIButton *avatarImage;
@property (weak, nonatomic) IBOutlet UIView *commentContainer;
@property (weak, nonatomic) IBOutlet UILabel *usernameText;

-(void)initText;

@end
