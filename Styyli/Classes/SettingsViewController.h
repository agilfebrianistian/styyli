//
//  SettingsViewController.h
//  STYYLI
//
//  Created by Agil Febrianistian on 5/18/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "UserInfo.h"
#import "RHZAppDelegate.h"
#import "ALToastView.h"
#import "FeedbackViewController.h"
#import "SupportViewController.h"
#import "ChangePasswordViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import <Twitter/Twitter.h>
#import <TwitterKit/TwitterKit.h>



@interface SettingsViewController : UITableViewController<UITableViewDelegate, UITableViewDataSource,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate,FBLoginViewDelegate>
{
    bool isUp;
    NSString * imageName;
    NSData *imageData;
}

@property (weak, nonatomic) IBOutlet UIButton *profileImage;
@property (weak, nonatomic) IBOutlet UITableView *infoTable;

@property (strong, nonatomic) IBOutlet UITableViewCell *fullNameCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *lastNameCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *userNameCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *passwordCell;

@property (strong, nonatomic) IBOutlet UITableViewCell *aboutMeCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *notificationCell;

@property (strong, nonatomic) IBOutlet UITableViewCell *facebookCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *twitterCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *instagramCell;


@property (strong, nonatomic) IBOutlet UITableViewCell *feedbackCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *supportCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *versionCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *logoutCell;


@property (weak, nonatomic) IBOutlet UITextField *fullnameTextfield;
@property (weak, nonatomic) IBOutlet UITextField *lastnameTextfield;
//@property (weak, nonatomic) IBOutlet UITextField *usernameTextfield;
//@property (weak, nonatomic) IBOutlet UITextField *passwordTextfield;


@property (weak, nonatomic) IBOutlet UIImageView *facebookButton;
@property (weak, nonatomic) IBOutlet UIImageView *twitterButton;
@property (weak, nonatomic) IBOutlet UIImageView *instagramButton;



- (IBAction)bgTapped:(id)sender;
- (IBAction)imageChange:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *headerView;


@end
