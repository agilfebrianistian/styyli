//
//  BrandContainerView.m
//  STYYLI
//
//  Created by Agil Febrianistian on 1/6/15.
//  Copyright (c) 2015 Mirosea. All rights reserved.
//

#import "BrandContainerView.h"
#import "EBPhotoPagesController.h"
#import "ItemsViewController.h"
#import "UIImageView+WebCache.h"

@interface BrandContainerView ()

{

    int end;

}

@end

@implementation BrandContainerView
@synthesize activityContainer,arrayData,brandCollection;

static  BrandContainerView *sharedObject=nil;


+(BrandContainerView *)sharedObject{
    @synchronized(self)
    {
        if (sharedObject == nil)
        {
            UIStoryboard *myStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            sharedObject = (BrandContainerView *)[myStoryboard instantiateViewControllerWithIdentifier:@"brandcontainer"];
        }
    }
    return sharedObject;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    end = -1;
    
    
    self.activityContainer.layer.cornerRadius = 10;
    self.view.layer.position =CGPointMake([[UIScreen mainScreen]bounds].size.width/2, [[UIScreen mainScreen]bounds].size.height/2);
}

- (void)viewDidUnload
{
    [self setActivityContainer:nil];
    [super viewDidUnload];
}

#pragma mark - gesture

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (CGRectContainsPoint(self.activityContainer.bounds, [touch locationInView:self.activityContainer])) {
        return NO;
    }
    else
    {
        return YES;
    }
    
}

-(IBAction)bgTapped:(id)sender
{
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"cancelData"
//                                                        object:self];
    
//    [self.view setUserInteractionEnabled:false];
    
    [self hideActivityView];
 
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(void)showActivityView
{
    UIWindow *window = [[UIApplication sharedApplication].delegate window];
    [window addSubview:self.view];
    
    brandCounter = 1;
    
    [brandCollection reloadData];
}

-(void)hideActivityView
{
    [self.view removeFromSuperview];
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    
    if ([[postData sharedObject].step isEqualToString:@"1"]) {
        return arrayData.count;
    }
    else
    {
        return 1;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if ([[postData sharedObject].step isEqualToString:@"1"]) {
        return [[[arrayData objectAtIndex:section] valueForKey:@"categories"] count];
    }
    else
    {
        NSLog(@"%d",arrayData.count);
        
        return arrayData.count;
        
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        BrandContainerHeader *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"BrandContainerHeader" forIndexPath:indexPath];
        
        [headerView.backButton addTarget:self action:@selector(backButtonPressed:event:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([[postData sharedObject].step isEqualToString:@"1"]) {
            headerView.headertitle.text = [[arrayData objectAtIndex:indexPath.section] valueForKey:@"name"];
            [headerView.backButton setHidden:true];
        }
        else if ([[postData sharedObject].step isEqualToString:@"2"])
        {
            headerView.headertitle.text = categoryTitle;
            [headerView.backButton setHidden:false];
        }
        else
        {
            headerView.headertitle.text = brandTitle;
            [headerView.backButton setHidden:false];
            
        }
        
        reusableview = headerView;
    }
    return reusableview;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([[postData sharedObject].step isEqualToString:@"1"]) {
        
        CategoryContainerCell *cell = (CategoryContainerCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"CategoryContainerCell" forIndexPath:indexPath];
        
        
        NSMutableArray  * items =[[[arrayData objectAtIndex:indexPath.section] valueForKey:@"categories"] objectAtIndex:indexPath.row];
        
        
        cell.itemtitle.text = [[items valueForKey:@"category"] valueForKey:@"name"];
        
        return cell;

    }
    else
    {
        BrandContainerCell *cell = (BrandContainerCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"BrandContainerCell" forIndexPath:indexPath];
        
        
        NSMutableArray  * items = [arrayData objectAtIndex:indexPath.row];
        
    
        cell.itemtitle.text = [items valueForKey:@"name"];
        
        __block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = cell.itemImage;
        
        [cell.itemImage sd_setImageWithURL:[items valueForKey:@"image_url"]
                          placeholderImage:PLACEHOLDER_IMAGE
                                   options:SDWebImageProgressiveDownload
                                  progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                      if (!activityIndicator) {
                                          [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite]];
                                          activityIndicator.center = weakImageView.center;
                                          [activityIndicator startAnimating];
                                      }
                                  }
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                     [activityIndicator removeFromSuperview];
                                     activityIndicator = nil;
                                 }];
        
            return cell;
    }
    

}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([[postData sharedObject].step isEqualToString:@"1"]) {
        
        NSMutableArray  * items =[[[arrayData objectAtIndex:indexPath.section] valueForKey:@"categories"] objectAtIndex:indexPath.row];
        categoryTitle = [[items valueForKey:@"category"] valueForKey:@"name"];
        
        
        [postData sharedObject].categoryId = [[items valueForKey:@"category"] valueForKey:@"id"];
        [[CSActivityViewController sharedObject] showActivityView];
        
        arrayData = [[NSMutableArray alloc]init];

        
        [self getBrands:brandCounter];
        
    }
    
    else if ([[postData sharedObject].step isEqualToString:@"2"]) {
        NSMutableArray  * items = [arrayData objectAtIndex:indexPath.row];
        
        brandTitle = [items valueForKey:@"name"];
        
        
        [postData sharedObject].brandId = [items valueForKey:@"id"];
        
        [[CSActivityViewController sharedObject] showActivityView];
        [self getItems];
        
    }
    
    else
    {
        NSMutableArray  * items = [arrayData objectAtIndex:indexPath.row];
        [postData sharedObject].itemName = [items valueForKey:@"name"];
        [postData sharedObject].itemId = [items valueForKey:@"id"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"postData"
                                                            object:self];
        
        
        [self hideActivityView];
    }
}


-(void) collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{

    end++;
    
    if ([[postData sharedObject].step isEqualToString:@"2"]) {

        if (end > arrayData.count && brandCounter<totalBrand) {
            
            NSLog(@"ea");
            brandCounter++;
            [self getBrands:brandCounter];
            end = -1;
        }
    
    }
}

//- (CGPoint)targetContentOffsetForProposedContentOffset:(CGPoint)offset
//                                 withScrollingVelocity:(CGPoint)velocity {
//    
//    CGRect cvBounds = self.brandCollection.bounds;
//    CGFloat halfWidth = cvBounds.size.width * 0.5f;
//    CGFloat proposedContentOffsetCenterX = offset.x + halfWidth;
//    
//    //NSArray* attributesArray = [self layoutAttributesForElementsInRect:cvBounds];
//    
//    UICollectionViewLayoutAttributes* candidateAttributes;
//    for (UICollectionViewLayoutAttributes* attributes in attributesArray) {
//        
//        // == Skip comparison with non-cell items (headers and footers) == //
//        if (attributes.representedElementCategory !=
//            UICollectionElementCategoryCell) {
//            continue;
//        }
//        
//        // == First time in the loop == //
//        if(!candidateAttributes) {
//            candidateAttributes = attributes;
//            continue;
//        }
//        
//        if (fabsf(attributes.center.x - proposedContentOffsetCenterX) <
//            fabsf(candidateAttributes.center.x - proposedContentOffsetCenterX)) {
//            candidateAttributes = attributes;
//        }
//    }
//    
//    return CGPointMake(candidateAttributes.center.x - halfWidth, offset.y);
//    
//}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    targetContentOffset->x = scrollView.contentOffset.x - 10;
    
    
    if(targetContentOffset->x >= scrollView.contentSize.height){
        
        
        NSLog(@"test 1");
        
    }
    

    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView  {
    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    if(bottomEdge >= scrollView.contentSize.height){
        
//        if(platformBatch * platformLoadPerBatch <= [allPlatforms count]){
//            int remaining = (int)([allPlatforms count] - (platformLoadPerBatch * platformBatch));
//            if( remaining >= 20)
//                [self loadPlatform:platformBatch loadCount:platformLoadPerBatch];
//            else
//                [self loadPlatform:platformBatch loadCount:remaining];
//        }
        
        
           NSLog(@"load new item");
        
    }
}

#pragma mark Request

- (void)getHeader
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token": [UserInfo sharedObject].tokenUser};
    
    [manager GET:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_CATEGORIES] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        arrayData = [[(NSDictionary *)responseObject objectForKey:@"categories"] valueForKey:@"category"];
        
        [postData sharedObject].step = @"1";
        
        [brandCollection reloadData];
        
        [[CSActivityViewController sharedObject] hideActivityView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [postData sharedObject].step = @"0";
        [[CSActivityViewController sharedObject] hideActivityView];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:@"postData"
                                                      object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:@"cancelData"
                                                      object:nil];
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}


- (void)getBrands:(int)counter
{
    
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
//        NSDictionary *params = @{@"api_key": API_KEY,
//                                 @"auth_token": [UserInfo sharedObject].tokenUser,
//                                 @"page":[NSString stringWithFormat:@"%d",counter]};
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token": [UserInfo sharedObject].tokenUser,
                             @"page":[NSString stringWithFormat:@"%d",counter]};
    
    
        [manager GET:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_BRANDS] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [[CSActivityViewController sharedObject] hideActivityView];
            
            NSArray *results = [(NSDictionary *)responseObject objectForKey:@"brands"];
            totalBrand = [[(NSDictionary *)responseObject objectForKey:@"total_page"] integerValue];
            for (NSDictionary * items in results) {
                NSDictionary * item = [items valueForKey:@"brand"];
                
                [arrayData addObject:item];
            }

            [postData sharedObject].step = @"2";
            
            [brandCollection reloadData];

             
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            NSLog(@"Error: %@", error);
            [postData sharedObject].step = @"0";
            [[CSActivityViewController sharedObject] hideActivityView];
            
            [[NSNotificationCenter defaultCenter] removeObserver:self
                                                            name:@"postData"
                                                          object:nil];
            
            [[NSNotificationCenter defaultCenter] removeObserver:self
                                                            name:@"cancelData"
                                                          object:nil];
            
            UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [message show];
            
        }];
    

}

- (void)getItems
{
    
    NSLog(@"%@",[postData sharedObject].brandId);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key"     : API_KEY,
                             @"auth_token"  : [UserInfo sharedObject].tokenUser,
                             @"category_id" : [postData sharedObject].categoryId,
                             @"brand_id"    : [postData sharedObject].brandId};
    
    [manager GET:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_ITEMS] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        
        NSLog(@"%@",responseObject);
        
        
        NSArray *results = [(NSDictionary *)responseObject objectForKey:@"items"];
        
        arrayData = [[NSMutableArray alloc]init];
        
        for (NSDictionary * items in results) {
            NSDictionary * item = [items valueForKey:@"item"];
            
            [arrayData addObject:item];
        }
        
        [postData sharedObject].step = @"3";
        
        
        [[CSActivityViewController sharedObject] hideActivityView];
        
        [brandCollection reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [postData sharedObject].step = @"0";
        [[CSActivityViewController sharedObject] hideActivityView];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:@"postData"
                                                      object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:@"cancelData"
                                                      object:nil];
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
    }];
}

- (void)backButtonPressed:(id)sender event:(id)event{
    
    if ([[postData sharedObject].step isEqualToString:@"2"]) {
        
        [postData sharedObject].step = @"1";
        [self getHeader];
        
    }
    
    else if ([[postData sharedObject].step isEqualToString:@"3"]) {
        
        arrayData = [[NSMutableArray alloc]init];
        
        brandCounter = 1;
        
        [postData sharedObject].step = @"2";
        [self getBrands:brandCounter];
        
    }
    
}
@end