//
//  SearchBrandsViewController.m
//  STYYLI
//
//  Created by Agil Febrianistian on 7/20/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import "SearchBrandsViewController.h"
#import "UIImageView+WebCache.h"

@interface SearchBrandsViewController ()

@end

@implementation SearchBrandsViewController
@synthesize allTableData,handlerText;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    allTableData = [[NSMutableArray alloc]init];
    
    [self addNavigationItem];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark Add Navigation Item

- (void)addNavigationItem
{
    UIImage *leftImage = [UIImage imageNamed:@"icon_menu"];
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton addTarget:self action:@selector(showLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    leftButton.bounds = CGRectMake( 0, 0, leftImage.size.width, leftImage.size.height );
    [leftButton setImage:leftImage forState:UIControlStateNormal];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    UIEdgeInsets buttonEdges = UIEdgeInsetsMake(0, LOGO_INSET_STRIP, 0, -LOGO_INSET_STRIP);
    UIImage *leftImagea = [UIImage imageNamed:@"signin-logo.png"];
    UIButton *leftButtona = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButtona.bounds = CGRectMake(0, 0, leftImagea.size.width/2, leftImagea.size.height/2);
    [leftButtona setImage:leftImagea forState:UIControlStateNormal];
    [leftButtona setImageEdgeInsets:buttonEdges];
    [leftButtona setUserInteractionEnabled:NO];
    UIBarButtonItem *leftItema = [[UIBarButtonItem alloc] initWithCustomView:leftButtona];
    
    self.navigationItem.leftBarButtonItems = @[leftItem,leftItema];
}

#pragma mark -
#pragma mark Button Function

- (void)showLeftMenu
{
    [self.viewDeckController performSelectorOnMainThread:@selector(toggleLeftView) withObject:nil waitUntilDone:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    [self refresh:NULL];
}

- (void)refresh:(id)sender
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser,
                             @"search":self.searchString
                             };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_SEARCH_BRANDS] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSLog(@"%@",responseObject);
        NSArray *results = [(NSDictionary *)responseObject objectForKey:@"brands"];
        
        if (results.count == 0) {
            
            handlerText.hidden = false;
        }
        else
        {
            
            handlerText.hidden = true;
            
            for (NSDictionary * items in results) {
                NSDictionary * item = [items valueForKey:@"brand"];
                
                [allTableData addObject:item];
            }
        }
        
        
        // Reload Table View
        [self.myCollection reloadData];
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        handlerText.hidden = false;
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return allTableData.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray  * items =[allTableData objectAtIndex:indexPath.row];
    
    ItemDetailsCell *cell = (ItemDetailsCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ItemDetailsCell" forIndexPath:indexPath];
    
    cell.itemBrand.text = [items valueForKey:@"brand_name"];
    cell.itemLabel.text = [items valueForKey:@"name"];
    cell.itemPrice.text = [items valueForKey:@"price"];
    
    
    __block UIActivityIndicatorView *activityIndicator;
    __weak UIImageView *weakImageView = cell.itemImage;
    
    [cell.itemImage sd_setImageWithURL:[items valueForKey:@"image_url"]
                      placeholderImage:PLACEHOLDER_IMAGE
                               options:SDWebImageProgressiveDownload
                              progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                  if (!activityIndicator) {
                                      [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite]];
                                      activityIndicator.center = weakImageView.center;
                                      [activityIndicator startAnimating];
                                  }
                              }
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                 [activityIndicator removeFromSuperview];
                                 activityIndicator = nil;
                             }];
   
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray  * items =[allTableData objectAtIndex:indexPath.row];
    
    
    
    
    SearchItemsViewController *searchItemsController =  [self.storyboard instantiateViewControllerWithIdentifier:@"searchitems"];
    searchItemsController.searchString = @"";
    searchItemsController.searchid = [items valueForKey:@"id"];
    
    
    [self.navigationController pushViewController:searchItemsController animated:YES];

 
}


@end
