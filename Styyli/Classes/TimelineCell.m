//
//  RHZTimelineCell.m
//  Styyli
//
//  Created by R. Hafidhullah Zakariyya on 12/30/13.
//  Copyright (c) 2013 Mirosea. All rights reserved.
//

#import "TimelineCell.h"
#import "UIImageView+WebCache.h"

@implementation TimelineCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
}

-(void)initText{
    self.captionAtributed.delegate = self;
    self.captionAtributed.userInteractionEnabled = YES;
    self.captionAtributed.numberOfLines = 0;
    self.captionAtributed.lineBreakMode = NSLineBreakByCharWrapping;
}

- (void)selectedMention:(NSString *)string {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

    NSArray *subStrings = [string componentsSeparatedByString:@"@"];
    [self getDataWithUsername:[subStrings objectAtIndex:1]];
    
}
- (void)selectedHashtag:(NSString *)string {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

    NSArray *subStrings = [string componentsSeparatedByString:@"#"];    
    
    [[NSUserDefaults standardUserDefaults] setInteger:3 forKey:@"timelineID"];
    [[NSUserDefaults standardUserDefaults] setValue:[subStrings objectAtIndex:1] forKey:@"hashtagString"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"hashtagData"
                                                        object:self];
    
}
- (void)selectedLink:(NSString *)string {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Selected" message:string delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}

- (void)getDataWithUsername:(NSString*)username
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser
                             };
    
    [manager GET:[NSString stringWithFormat:@"%@mention/%@.json",SERVICE_URL,username] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"%@",responseObject);
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if ([[responseObject valueForKey:@"success"]integerValue] == 1) {
            
            NSString * temp = [[responseObject valueForKey:@"user"]valueForKey:@"id"];
            
            [[NSUserDefaults standardUserDefaults] setValue:temp forKey:@"profileID"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DataUpdated"
                                                                object:self];
        }
        else
        {
            UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Sorry" message:@"User Not Found" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [message show];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}


-(void)initTagPopoverFrom : (NSMutableArray*)tags
{
    for (UIView *subView in self.mainPicture.subviews)
    {
        if (subView.tag == 99)
        {
            [subView removeFromSuperview];
        }
    }
    
    EBTagPopover * tagPopover = [[EBTagPopover alloc]init];
    
    if (tags.count > 0) {
        
        for (DEMOTag * a in tags) {
            
            tagPopover  = [[EBTagPopover alloc]initWithTag:a];
            
            [tagPopover setNormalizedArrowPoint:a.normalizedPosition];
            
            [tagPopover repositionInRect:self.mainPicture.frame];
            
            tagPopover.tag = 99;
            
            [self.mainPicture addSubview:tagPopover];
        }
    }
    
}


-(void)initItems
{
    [itemsList removeFromSuperview];
    
    itemsList = [[iCarousel alloc]initWithFrame:CGRectMake(0, 5, 320, 44)];
    [itemsList setScrollEnabled:FALSE];
    [itemsList setPagingEnabled:FALSE];
    itemsList.centerItemWhenSelected = FALSE;
    itemsList.delegate = self;
    itemsList.dataSource = self;
    itemsList.type=iCarouselTypeLinear;
    itemsList.viewpointOffset = CGSizeMake(125, 0);
    
    //[itemsList setUserInteractionEnabled:NO];
    
    [self.brandView addSubview:itemsList];
    
}


-(void)initLovers
{
    [loversList removeFromSuperview];
    
    loversList = [[iCarousel alloc]initWithFrame:CGRectMake(60, 0, 150, 40)];
    [loversList setScrollEnabled:FALSE];
    loversList.delegate = self;
    loversList.dataSource = self;
    loversList.centerItemWhenSelected = FALSE;
    loversList.type=iCarouselTypeLinear;
    loversList.viewpointOffset = CGSizeMake(60, 0);
    
    
    [self.likeView addSubview:loversList];
    
}


-(NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    if (carousel == itemsList) {
        return self.itemsData.count;
    }
    else
    {
        if (self.loversData.count > 4)
            return 4;
        else
            return self.loversData.count;
    }
    
    
}

- (UIView *) carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view{
    
    //    UILabel *label = nil;
    UIImageView * images = Nil;
    
    
    if (carousel == itemsList) {
        if (view == nil)
        {
            view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 44.0f, 44.0f)];
            view.contentMode = UIViewContentModeCenter;
            
            
            images = [[UIImageView alloc]initWithFrame:view.bounds];
            images.tag = 2;
            [images setContentMode:UIViewContentModeScaleAspectFit];
            
            
            [view addSubview:images];
        }
        else
        {
            images = (UIImageView *)[view viewWithTag:2];
        }
        
        
        NSArray * temp = [[self.itemsData objectAtIndex:index] valueForKey:@"item"];
        
        if([temp valueForKey:@"image_url"]==(id) [NSNull null])
            [images setImage:PLACEHOLDER_IMAGE];
        else
            [images sd_setImageWithURL:[NSURL URLWithString:[temp valueForKey:@"image_url"]] placeholderImage:PLACEHOLDER_IMAGE options:SDWebImageRefreshCached];
        
    }
    else
    {
        if (view == nil)
        {
            view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40.0f, 40.0f)];
            view.contentMode = UIViewContentModeCenter;
            
            images = [[UIImageView alloc]initWithFrame:view.bounds];
            
            images.layer.cornerRadius = 20.0f;
            images.layer.masksToBounds=YES;
            images.layer.borderColor=[[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f] CGColor];
            images.layer.borderWidth= 1.2f;
            
            
            images.tag = 2;
            
            [view addSubview:images];
        }
        else
        {
            images = (UIImageView *)[view viewWithTag:2];
            
        }
        
        
        NSArray * temp = [[self.loversData objectAtIndex:index] valueForKey:@"user"];
        
        
        if([temp valueForKey:@"image_url"]==(id) [NSNull null])
            [images setImage:PLACEHOLDER_AVATAR];
        else
            [images sd_setImageWithURL:[NSURL URLWithString:[temp valueForKey:@"image_url"]] placeholderImage:PLACEHOLDER_AVATAR options:SDWebImageRefreshCached];
    }
    
    
    return view;
    
    
}

- (BOOL)carousel:(iCarousel *)carousel shouldSelectItemAtIndex:(NSInteger)index{
    
    return YES;
}


#pragma mark -
#pragma mark iCarousel taps

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    if (carousel == itemsList) {
        
        
        NSString * temp = [[[self.itemsData objectAtIndex:index] valueForKey:@"item"]valueForKey:@"id"];
        
        [[NSUserDefaults standardUserDefaults] setValue:temp forKey:@"itemdetailsID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DataItemUpdated"
                                                            object:self];
        
        
    }
    else
    {
        NSString * temp = [[[self.loversData objectAtIndex:index] valueForKey:@"user"]valueForKey:@"id"];
        
        [[NSUserDefaults standardUserDefaults] setValue:temp forKey:@"profileID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DataUpdated"
                                                            object:self];
    }
    
}

- (CGFloat)carouselItemWidth:(iCarousel *)carousel
{
    if (carousel == itemsList) {
        return 50;
    }
    else
    {
        return 45;
    }
}

@end
