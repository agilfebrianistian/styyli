//
//  LoverListCell.h
//  STYYLI
//
//  Created by Agil Febrianistian on 5/26/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoverListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *avatarImage;

@property (weak, nonatomic) IBOutlet UIButton *followButton;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;

- (IBAction)followButton:(id)sender;

@end
