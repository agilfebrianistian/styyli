//
//  RHZLeftViewController.h
//  Styyli
//
//  Created by R. Hafidhullah Zakariyya on 12/23/13.
//  Copyright (c) 2013 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "ViewDeckSearchBar.h"
#import "LeftTableViewCell.h"

#import "SearchPeopleViewController.h"
#import "SearchNewsViewController.h"
#import "SearchItemsViewController.h"
#import "SearchBrandsViewController.h"

@interface RHZLeftViewController : UIViewController<UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate, UISearchDisplayDelegate>

{
    bool isSearching;
    int idRequest;
}


@property (weak, nonatomic) IBOutlet UIButton *profileButton;
@property (weak, nonatomic) IBOutlet UIButton *timelineButton;
@property (weak, nonatomic) IBOutlet UIButton *newsButton;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;
@property (weak, nonatomic) IBOutlet UIButton *settingButton;

@property (weak, nonatomic) IBOutlet ViewDeckSearchBar *userSearchBar;

@property (weak, nonatomic) IBOutlet UIButton *avatarUser;

@property (weak, nonatomic) IBOutlet UILabel *FirstNameUser;
@property (weak, nonatomic) IBOutlet UILabel *userNameUser;


@property (weak, nonatomic) IBOutlet UITableView *leftTable;

@property(nonatomic,strong) NSArray *items;
@property (nonatomic, retain) NSMutableArray *itemsInTable;


- (IBAction)profileButton:(id)sender;


@end
