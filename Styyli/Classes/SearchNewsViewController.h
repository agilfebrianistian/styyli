//
//  SearchNewsViewController.h
//  STYYLI
//
//  Created by Agil Febrianistian on 7/20/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "CSActivityViewController.h"
#import "UIImageView+AFNetworking.h"
#import "Constant.h"
#import "UserInfo.h"
#import "HomeCell.h"
#import "RHZAppDelegate.h"
#import "NewsDetailsViewController.h"

@interface SearchNewsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *newsListTable;

@property (nonatomic) int currId;

@property (strong, nonatomic) NSArray* allTableData;

@property (strong, nonatomic) NSString* searchString;

@end
