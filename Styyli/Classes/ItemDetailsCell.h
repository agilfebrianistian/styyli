//
//  ItemDetailsCell.h
//  STYYLI
//
//  Created by Agil Febrianistian on 7/13/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemDetailsCell : UICollectionViewCell


@property (weak, nonatomic) IBOutlet UIImageView *itemImage;
@property (weak, nonatomic) IBOutlet UILabel *itemBrand;
@property (weak, nonatomic) IBOutlet UILabel *itemPrice;
@property (weak, nonatomic) IBOutlet UILabel *itemLabel;

@end
