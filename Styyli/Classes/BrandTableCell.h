//
//  BrandTableCell.h
//  STYYLI
//
//  Created by Agil Febrianistian on 4/19/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BrandTableViewController;
@interface BrandTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnExpand;
@end
