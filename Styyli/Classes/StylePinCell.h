//
//  StylePinCell.h
//  STYYLI
//
//  Created by Agil Febrianistian on 7/17/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StylePinCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *itemImage;

@end
