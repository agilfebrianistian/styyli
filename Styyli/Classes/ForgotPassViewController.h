//
//  ForgotPassViewController.h
//  Styyli
//
//  Created by Agil Febrianistian on 2/23/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "CSActivityViewController.h"

@interface ForgotPassViewController : UIViewController<UITextFieldDelegate>
{
    bool isUp;
}


@property (retain, nonatomic) IBOutlet UIView *activityContainer;
+(ForgotPassViewController *)sharedObject;
-(void)showActivityView;
-(void)hideActivityView;


@property (weak, nonatomic) IBOutlet UITextField *emailTextfield;

- (IBAction)closeButton:(id)sender;
- (IBAction)sendButton:(id)sender;


@property (weak, nonatomic) IBOutlet UILabel *emailWarning;

@end
