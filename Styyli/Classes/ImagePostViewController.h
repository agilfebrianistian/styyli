//
//  ImagePostViewController.h
//  Styyli
//
//  Created by Agil Febrianistian on 2/14/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PECropViewController.h"
#import "AFNetworking.h"
#import "CSActivityViewController.h"
#import "RHZAppDelegate.h"
#import "ImageFilterViewController.h"
#import "postData.h"
#import "EBPhotoPagesDataSource.h"
#import "EBPhotoPagesDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import "Constant.h"
#import "UserInfo.h"
#import "MJAutoCompleteManager.h"
#import "MDCustomAutoCompleteCell.h"


@interface ImagePostViewController : UIViewController<UITextFieldDelegate,EBPhotoPagesDataSource, EBPhotoPagesDelegate,CLLocationManagerDelegate,MJAutoCompleteManagerDataSource, MJAutoCompleteManagerDelegate, UITextViewDelegate>
{
    bool isUp;
    NSData *imageData;
    CLLocationManager *locationManager;
    EBPhotoPagesController *photoPagesController;
    bool facebookpost;
    
    NSString *imagePath;

}

@property (weak, nonatomic) IBOutlet UIImageView *imageScreen;
@property (weak, nonatomic) IBOutlet UITextField *captiontext;
@property (weak, nonatomic) IBOutlet UIButton *imageButton;

@property (strong) NSArray *photos;
@property (assign) BOOL simulateLatency;

- (IBAction)postButton:(id)sender;
- (IBAction)bgTapped:(id)sender;
- (IBAction)addBrand:(id)sender;

@property (strong, nonatomic) IBOutlet UIScrollView *scroller;
@property (weak, nonatomic) IBOutlet UILabel *locationText;


@property (nonatomic, strong) NSMutableArray *tagsData;

@property (nonatomic) BOOL didStartDownload;

@property (strong, nonatomic) MJAutoCompleteManager *autoCompleteMgr;

@property (weak, nonatomic) IBOutlet UIView *autoCompleteContainer;

@property (weak, nonatomic) IBOutlet UIButton *facebookButton;
- (IBAction)facebookButtonPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *twitterButton;
- (IBAction)twitterButtonPressed:(id)sender;


@end
