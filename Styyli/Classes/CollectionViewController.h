//
//  CollectionViewController.h
//  STYYLI
//
//  Created by Agil Febrianistian on 7/17/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CollectionCell.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "Constant.h"
#import "UserInfo.h"
#import "CSActivityViewController.h"
#import "ProfileSettingViewController.h"
#import "ItemDetailsViewController.h"


@interface CollectionViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *myCollection;
@property (nonatomic, retain) NSMutableArray *allTableData;


@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UIImageView *profilePicture;
@property (weak, nonatomic) IBOutlet UILabel *profileName;
@property (weak, nonatomic) IBOutlet UILabel *profileStatus;
@property (weak, nonatomic) IBOutlet UILabel *profieCity;
@property (weak, nonatomic) IBOutlet UILabel *profileSite;

@property (weak, nonatomic) IBOutlet UIButton *profileButton;


@property (weak, nonatomic) IBOutlet UILabel *profileFollowing;
@property (weak, nonatomic) IBOutlet UILabel *profileFollower;

@property (weak, nonatomic) IBOutlet UILabel *profileStylesCount;
@property (weak, nonatomic) IBOutlet UILabel *profileWardrobeCount;
@property (weak, nonatomic) IBOutlet UILabel *profileFavoriteCount;


- (IBAction)segmentedChange:(id)sender;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedCategory;


@property (weak, nonatomic) IBOutlet UILabel *handlerText;


@end
