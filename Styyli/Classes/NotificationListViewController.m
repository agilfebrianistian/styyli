//
//  NotificationListViewController.m
//  STYYLI
//
//  Created by Agil Febrianistian on 8/10/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import "NotificationListViewController.h"

#import "UIImageView+WebCache.h"

@interface NotificationListViewController ()

@end

@implementation NotificationListViewController
@synthesize allTableData,notificationTable;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addNavigationItem];
    
    // Initialize Refresh Control
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    
    // Configure Refresh Control
    [refreshControl addTarget:self action:@selector(refreshComment:) forControlEvents:UIControlEventValueChanged];
    
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = self.notificationTable;
    
    tableViewController.refreshControl = refreshControl;
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self refreshComment:NULL];
    
}

#pragma mark -
#pragma mark Add Navigation Item

- (void)addNavigationItem
{
    UIImage *leftImage = [UIImage imageNamed:@"IconBackToTimeline"];
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton addTarget:self action:@selector(backToTimeline) forControlEvents:UIControlEventTouchUpInside];
    leftButton.bounds = CGRectMake( 0, 0, leftImage.size.width, leftImage.size.height );
    [leftButton setImage:leftImage forState:UIControlStateNormal];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];

    self.navigationItem.leftBarButtonItem = leftItem;
    
    
    CGRect frame = CGRectMake(0, 0, 0, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame] ;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:16.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"Notification";
    self.navigationItem.titleView = label;
}

#pragma mark -
#pragma mark Button Function

- (void)backToTimeline
{
    NSArray *tempViewControllers = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    [self.navigationController popToViewController:[tempViewControllers objectAtIndex:[tempViewControllers count]-2] animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)refreshComment:(id)sender
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser
                             };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_NOTIFICATIONS] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSLog(@"%@",responseObject);
        
        NSArray *results = [(NSDictionary *)responseObject objectForKey:@"notifications"];
        
        if (results == (id)[NSNull null]) {
            
        }
        else
        {
            self.allTableData = results;
            
            // Reload Table View
            [self.notificationTable reloadData];
            
            // End Refreshing
        }
        
        [(UIRefreshControl *)sender endRefreshing];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [(UIRefreshControl *)sender endRefreshing];
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
    }];
    
}

#pragma mark -
#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return allTableData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray  * items =[allTableData objectAtIndex:indexPath.row];
    NSDictionary * item = [items valueForKey:@"notification"];
    
    
    if ([item valueForKey:@"post_image"] != nil) {
      return 150;
    }
    else
    {
      return 72;
    }
    

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray* items =[allTableData objectAtIndex:indexPath.row];
    NSDictionary * item = [items valueForKey:@"notification"];
    
    NSString *cellIdentifier = @"NotificationCell";
    
    NotificationCell *cell =  (NotificationCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[NotificationCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.notificationLabel.text = [item valueForKey:@"message"];
    cell.notificationTime.text = [item valueForKey:@"created_at"];
    
    NSURL *imageUrl = [item valueForKey:@"post_image"];
    
    __block UIActivityIndicatorView *activityIndicator;
    __weak UIImageView *weakImageView = cell.notificationImage;
    
    [cell.notificationImage sd_setImageWithURL:imageUrl
                        placeholderImage:PLACEHOLDER_IMAGE
                                 options:SDWebImageProgressiveDownload
                                progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                    if (!activityIndicator) {
                                        [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge]];
                                        activityIndicator.center = weakImageView.center;
                                        [activityIndicator startAnimating];
                                    }
                                }
                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                   [activityIndicator removeFromSuperview];
                                   activityIndicator = nil;
                               }];
    
    
    
    cell.notificationImage.clipsToBounds = YES;
    cell.notificationImage.layer.borderColor=[[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f] CGColor];
    cell.notificationImage.layer.borderWidth= 1.2f;
    
    
    [cell.notificationAvatar sd_setImageWithURL:[NSURL URLWithString:[item valueForKey:@"avatar"]] placeholderImage:PLACEHOLDER_IMAGE options:SDWebImageRefreshCached];

    
    
    cell.notificationAvatar.layer.cornerRadius = 20.0f;
    cell.notificationAvatar.clipsToBounds = YES;
    cell.notificationAvatar.layer.borderColor=[[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f] CGColor];
    cell.notificationAvatar.layer.borderWidth= 1.2f;
    
    
    if ([[item valueForKey:@"read"]intValue] == 0)
    {
        [cell setBackgroundColor:[UIColor colorWithRed:232.0/255.0 green:223.0/255.0 blue:197.0/255.0 alpha:1.0]];
        [cell.notificationMarker setImage:[UIImage imageNamed:@"bullet_orange"]];
    }
    else
    {
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell.notificationMarker setImage:[UIImage imageNamed:@"bullet_black"]];
    }
        
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray  * items =[allTableData objectAtIndex:indexPath.row];
    NSDictionary * item = [items valueForKey:@"notification"];
    
    NSLog(@"%@",items);
    
    [self setReadwithID:[item valueForKey:@"id"] andPostId:[item valueForKey:@"post_id"] fromIndex:indexPath.row];
    
}


- (void)setReadwithID:(NSString*)idNotification andPostId:(NSString*)postId fromIndex:(int)indexRow
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser
                             };
    
    [manager GET:[NSString stringWithFormat:@"%@notifications/%@/%@",SERVICE_URL,idNotification,SERVICE_NOTIFICATIONS_READ] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSMutableArray  * items =[allTableData objectAtIndex:indexRow];
        NSDictionary * item = [items valueForKey:@"notification"];
        
        
        if ([item valueForKey:@"post_image"] != nil) {

            [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:@"timelineID"];
            [[NSUserDefaults standardUserDefaults] setValue:postId forKey:@"notificationPost"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            [self.navigationController popToRootViewControllerAnimated:YES];
            self.viewDeckController.centerController = SharedAppDelegate.timelineController;

        }
        else

        {
            
            [[NSUserDefaults standardUserDefaults] setValue:[item valueForKey:@"from_id"] forKey:@"profileID"];
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"profiletimelineID"];
            [[NSUserDefaults standardUserDefaults] synchronize];

            [self.navigationController popToRootViewControllerAnimated:YES];
            self.viewDeckController.centerController = SharedAppDelegate.profileController;
        }
        

        
//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//            TimelineViewController *timelineController = [storyboard instantiateViewControllerWithIdentifier:@"timeline"];
//            UINavigationController *controller = [[UINavigationController alloc] initWithNavigationBarClass:[GTScrollNavigationBar class] toolbarClass:nil];
//        
//            [controller setViewControllers:@[timelineController] animated:NO];
//        
//            [self.navigationController presentViewController:controller animated:YES completion:nil];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
    }];
    
}


@end
