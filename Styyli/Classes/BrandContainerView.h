//
//  BrandContainerView.h
//  STYYLI
//
//  Created by Agil Febrianistian on 1/6/15.
//  Copyright (c) 2015 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "postData.h"
#import "AFNetworking.h"
#import "CSActivityViewController.h"
#import "postData.h"
#import "Constant.h"
#import "UserInfo.h"
#import "CategoryContainerCell.h"
#import "BrandContainerCell.h"
#import "BrandContainerHeader.h"
#import "EBPhotoPagesDataSource.h"
#import "EBPhotoPagesDelegate.h"

@protocol BrandContainerDelegate;
@interface BrandContainerView : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,EBPhotoPagesDataSource,EBPhotoPagesDelegate>
{
    int brandCounter;
    int totalBrand;
    NSString *categoryTitle;
    NSString *brandTitle;
}

@property(nonatomic,strong)NSMutableArray *arrayData;
@property (retain, nonatomic) IBOutlet UIView *activityContainer;

@property (weak, nonatomic) IBOutlet UICollectionView *brandCollection;

+(BrandContainerView *)sharedObject;
-(void)showActivityView;
-(void)hideActivityView;


@end

