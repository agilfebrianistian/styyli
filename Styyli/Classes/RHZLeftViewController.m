//
//  RHZLeftViewController.m
//  Styyli
//
//  Created by R. Hafidhullah Zakariyya on 12/23/13.
//  Copyright (c) 2013 Mirosea. All rights reserved.
//

#import "RHZLeftViewController.h"
#import "IIViewDeckController.h"
#import "RHZAppDelegate.h"

#import "UIButton+WebCache.h"

@interface RHZLeftViewController ()
@property (nonatomic, strong) NSArray *menuItems;
@property (nonatomic, strong) NSMutableArray *menuFilter;
@property (nonatomic, strong) NSArray *iconName;
@property (nonatomic, strong) NSArray *iconNameActive;

@end

@implementation RHZLeftViewController
@synthesize menuItems,iconName,iconNameActive;
@synthesize userSearchBar,avatarUser,FirstNameUser,userNameUser;
@synthesize leftTable;


- (void)awakeFromNib
{
    NSDictionary *dict=[[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tempdata" ofType:@"plist"]];
	self.items=[dict valueForKey:@"Items"];
	self.itemsInTable=[[NSMutableArray alloc] init];
	[self.itemsInTable addObjectsFromArray:self.items];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.menuFilter = [[NSMutableArray alloc]init];
    
    [self fixSearchBarKeyboard:userSearchBar];
    
    //self.leftTable.tableHeaderView = self.userSearchBar;
    
}


- (void)fixSearchBarKeyboard:(UIView*)searchBarOrSubView {
    
    if([searchBarOrSubView conformsToProtocol:@protocol(UITextInputTraits)]) {
        if ([searchBarOrSubView respondsToSelector:@selector(setKeyboardAppearance:)])
            [(id<UITextInputTraits>)searchBarOrSubView setKeyboardAppearance:UIKeyboardAppearanceAlert];
        if ([searchBarOrSubView respondsToSelector:@selector(setReturnKeyType:)])
            [(id<UITextInputTraits>)searchBarOrSubView setReturnKeyType:UIReturnKeyDone];
        if ([searchBarOrSubView respondsToSelector:@selector(setEnablesReturnKeyAutomatically:)])
            [(id<UITextInputTraits>)searchBarOrSubView setEnablesReturnKeyAutomatically:NO];
    }
    
    for(UIView *subView in [searchBarOrSubView subviews]) {
        [self fixSearchBarKeyboard:subView];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    

    if([UserInfo sharedObject].avatarUser==(id) [NSNull null])
        [avatarUser setBackgroundImage:PLACEHOLDER_AVATAR forState:UIControlStateNormal] ;
    else
        [avatarUser sd_setBackgroundImageWithURL:[NSURL URLWithString:[UserInfo sharedObject].avatarUser] forState:UIControlStateNormal placeholderImage:PLACEHOLDER_AVATAR options:SDWebImageRefreshCached];
    
    
    avatarUser.layer.cornerRadius = 20.0f;
    avatarUser.clipsToBounds = YES;
    avatarUser.layer.borderColor=[[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f] CGColor];
    avatarUser.layer.borderWidth= 1.2f;
    
    if([UserInfo sharedObject].firstNameUser==(id) [NSNull null] || [[UserInfo sharedObject].firstNameUser length]==0 || [[UserInfo sharedObject].firstNameUser isEqualToString:@""])
        FirstNameUser.text = @"";
    else
        FirstNameUser.text = [UserInfo sharedObject].firstNameUser;
    
    userNameUser.text = [UserInfo sharedObject].usernameUser;
    
    isSearching = FALSE;
    [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"isSearching"];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
   


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark-
#pragma mark Table

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    if (isSearching)
	{
        if ([self.menuFilter count]>0)
            return [self.menuFilter count];
        else
            return 1;
        
    }
	else
	{
        return [self.itemsInTable count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (!isSearching) {
        
        NSString *Title= [[self.itemsInTable objectAtIndex:indexPath.row] valueForKey:@"Name"];
        NSString * IconName = [[self.itemsInTable objectAtIndex:indexPath.row] valueForKey:@"Icon"];
        
        return [self createCellWithTitle:Title image:[[self.itemsInTable objectAtIndex:indexPath.row] valueForKey:@"Image name"] indexPath:indexPath andIcon:IconName];
    }
    
    else
    {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"] ;
            
        }
        
        
        cell.backgroundColor = [UIColor clearColor];
        tableView.backgroundColor = [UIColor blackColor];
        
        cell.textLabel.textColor = [UIColor whiteColor];
        
        
        if ([self.menuFilter count]>0)
        {
            NSMutableArray * items = [self.menuFilter objectAtIndex:indexPath.row];
            
            cell.textLabel.text = [items valueForKey:@"keyword"];
        }
        else
        {
            cell.textLabel.text = @"No data found";
            cell.textLabel.textAlignment = NSTextAlignmentRight;
        }
        
        
        
        return cell;
    }
    
}

- (UITableViewCell*)createCellWithTitle:(NSString *)title image:(UIImage *)image  indexPath:(NSIndexPath*)indexPath andIcon:(NSString*)iconNames
{
    NSString *CellIdentifier = @"LeftCell";
    LeftTableViewCell* cell = [self.leftTable dequeueReusableCellWithIdentifier:CellIdentifier];
    
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = [UIColor grayColor];
    cell.selectedBackgroundView = bgView;
    cell.lblTitle.text = title;
    cell.lblTitle.textColor = [UIColor whiteColor];
    
    [cell setIndentationLevel:[[[self.itemsInTable objectAtIndex:indexPath.row] valueForKey:@"level"] intValue]];
    cell.indentationWidth = 25;
    
    float indentPoints = cell.indentationLevel * cell.indentationWidth;
    
    cell.contentView.frame = CGRectMake(indentPoints,cell.contentView.frame.origin.y,cell.contentView.frame.size.width - indentPoints,cell.contentView.frame.size.height);
    
    NSDictionary *d1=[self.itemsInTable objectAtIndex:indexPath.row] ;
    
    if([d1 valueForKey:@"SubItems"])
    {
        cell.btnExpand.alpha = 1.0;
        [cell.btnExpand addTarget:self action:@selector(showSubItems:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        cell.btnExpand.alpha = 0.0;
    }
    
    [leftTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    leftTable.backgroundColor = [UIColor clearColor];
    
    [cell.iconButton setHidden:FALSE];
    [cell.btnExpand setHidden:FALSE];
    
    [cell.iconButton setImage:[UIImage imageNamed:iconNames] forState:UIControlStateNormal];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (isSearching) {
        
        [tableView deselectRowAtIndexPath:indexPath animated:true];
        

        
        
        if ([self.menuFilter count]>0)
        {
            NSMutableArray * items = [self.menuFilter objectAtIndex:indexPath.row];
            
            userSearchBar.text = [items valueForKey:@"keyword"];
            
            [self searchBarSearchButtonClicked:userSearchBar];
        }

        
        
    }
    else
    {
        
        NSDictionary *dic=[self.itemsInTable objectAtIndex:indexPath.row];
        if([dic valueForKey:@"SubItems"])
        {
            NSArray *arr=[dic valueForKey:@"SubItems"];
            BOOL isTableExpanded=NO;
            
            for(NSDictionary *subitems in arr )
            {
                NSInteger index=[self.itemsInTable indexOfObjectIdenticalTo:subitems];
                isTableExpanded=(index>0 && index!=NSIntegerMax);
                if(isTableExpanded) break;
            }
            
            if(isTableExpanded)
            {
                [self CollapseRows:arr];
                
                [tableView deselectRowAtIndexPath:indexPath animated:true];
            }
            else
            {
                NSUInteger count=indexPath.row+1;
                NSMutableArray *arrCells=[NSMutableArray array];
                for(NSDictionary *dInner in arr )
                {
                    [arrCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
                    [self.itemsInTable insertObject:dInner atIndex:count++];
                }
                [self.leftTable insertRowsAtIndexPaths:arrCells withRowAnimation:UITableViewRowAnimationLeft];
            }
        }
        else
        {
            
            NSString * titleMenu = [[self.itemsInTable objectAtIndex:indexPath.row] valueForKey:@"Name"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"timelineItem"
                                                                object:self];
            
            if([titleMenu isEqualToString:@"Home"])
            {
                [self showHomeViewController];
            }
            else if ([titleMenu isEqualToString:@"My Profile"])
            {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isTimeline"];
                [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"profiletimelineID"];
                [self showProfileViewController];
            }
            else if ([titleMenu isEqualToString:@"Favorite Timeline"])
            {
                [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"profiletimelineID"];
                [self showProfileViewController];
            }
            else if ([titleMenu isEqualToString:@"Styyli Pins"])
            {
                [self showStylePinController];
            }
            else if ([titleMenu isEqualToString:@"Collections"])
            {
                [self showCollectionController];
            }
            else if ([titleMenu isEqualToString:@"StyleStream"])
            {
                
                [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"timelineID"];
                [self showTimelineViewController];
            }
            else if ([titleMenu isEqualToString:@"My Timeline"])
            {
                
                [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"timelineID"];
                [self showTimelineViewController];
            }
            else if ([titleMenu isEqualToString:@"Trendsetter"])
            {
                [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"timelineID"];
                [self showTimelineViewController];
            }
            
            else if ([titleMenu isEqualToString:@"Settings"])
            {
                [self showSettingsController];
            }
            
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [tableView deselectRowAtIndexPath:indexPath animated:true];
            
            [tableView reloadData];
        }
        
        
    }
    
    
}

-(void)CollapseRows:(NSArray*)ar
{
	for(NSDictionary *dInner in ar )
    {
		NSUInteger indexToRemove=[self.itemsInTable indexOfObjectIdenticalTo:dInner];
		NSArray *arInner=[dInner valueForKey:@"SubItems"];
		if(arInner && [arInner count]>0)
        {
			[self CollapseRows:arInner];
		}
		
		if([self.itemsInTable indexOfObjectIdenticalTo:dInner]!=NSNotFound)
        {
			[self.itemsInTable removeObjectIdenticalTo:dInner];
			[self.leftTable deleteRowsAtIndexPaths:[NSArray arrayWithObject:
                                                    [NSIndexPath indexPathForRow:indexToRemove inSection:0]
                                                    ]
                                  withRowAnimation:UITableViewRowAnimationLeft];
        }
	}
}

-(void)showSubItems :(id) sender
{
    UIButton *btn = (UIButton*)sender;
    CGRect buttonFrameInTableView = [btn convertRect:btn.bounds toView:self.leftTable];
    NSIndexPath *indexPath = [self.leftTable indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    if(btn.alpha==1.0)
    {
        if ([[btn imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"arrow_big"]])
        {
            [btn setImage:[UIImage imageNamed:@"arrow_small"] forState:UIControlStateNormal];
        }
        else
        {
            [btn setImage:[UIImage imageNamed:@"arrow_big"] forState:UIControlStateNormal];
        }
        
    }
    
    NSDictionary *d=[self.itemsInTable objectAtIndex:indexPath.row] ;
    NSArray *arr=[d valueForKey:@"SubItems"];
    if([d valueForKey:@"SubItems"])
    {
        BOOL isTableExpanded=NO;
        for(NSDictionary *subitems in arr )
        {
            NSInteger index=[self.itemsInTable indexOfObjectIdenticalTo:subitems];
            isTableExpanded=(index>0 && index!=NSIntegerMax);
            if(isTableExpanded) break;
        }
        
        if(isTableExpanded)
        {
            [self CollapseRows:arr];
        }
        else
        {
            NSUInteger count=indexPath.row+1;
            NSMutableArray *arrCells=[NSMutableArray array];
            for(NSDictionary *dInner in arr )
            {
                [arrCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
                [self.itemsInTable insertObject:dInner atIndex:count++];
            }
            [self.leftTable insertRowsAtIndexPaths:arrCells withRowAnimation:UITableViewRowAnimationLeft];
        }
    }
}

#pragma mark-
#pragma mark Show View Controller

- (void)showProfileViewController
{
    NSString *valueToSave = [UserInfo sharedObject].idUser;
    
    [[NSUserDefaults standardUserDefaults] setValue:valueToSave forKey:@"profileID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.viewDeckController toggleLeftViewAnimated:YES completion:^(IIViewDeckController *controller, BOOL success){
        self.viewDeckController.centerController = SharedAppDelegate.profileController;
    }];
}

- (void)showTimelineViewController
{
    [self.viewDeckController toggleLeftViewAnimated:YES completion:^(IIViewDeckController *controller, BOOL success){
        self.viewDeckController.centerController = SharedAppDelegate.timelineController;
        
    }];
}

- (void)showNewsViewController
{
    [self.viewDeckController toggleLeftViewAnimated:YES completion:^(IIViewDeckController *controller, BOOL success){
        self.viewDeckController.centerController = SharedAppDelegate.newsController;
    }];
}

- (void)showShopViewController
{
    [self.viewDeckController toggleLeftViewAnimated:YES completion:^(IIViewDeckController *controller, BOOL success){
        self.viewDeckController.centerController = SharedAppDelegate.shopController;
    }];
}

- (void)showHomeViewController
{
    [self.viewDeckController toggleLeftViewAnimated:YES completion:^(IIViewDeckController *controller, BOOL success){
        self.viewDeckController.centerController = SharedAppDelegate.homeController;
    }];
}

- (void)showSettingsController
{
    [self.viewDeckController toggleLeftViewAnimated:YES completion:^(IIViewDeckController *controller, BOOL success){
        self.viewDeckController.centerController = SharedAppDelegate.settingsController;
    }];
}

- (void)showStylePinController
{
    [self.viewDeckController toggleLeftViewAnimated:YES completion:^(IIViewDeckController *controller, BOOL success){
        self.viewDeckController.centerController = SharedAppDelegate.stylePinController;
    }];
}

- (void)showCollectionController
{
    [self.viewDeckController toggleLeftViewAnimated:YES completion:^(IIViewDeckController *controller, BOOL success){
        self.viewDeckController.centerController = SharedAppDelegate.collectionController;
    }];
}



- (void)settingViewController
{
    [self.viewDeckController removeFromParentViewController];
    
    RHZAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate transitToTutorialScreen];
    
}

#pragma mark -
#pragma mark get data from server

- (void)getDataWithRL:(NSString*)url andText:(NSString*)text
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser,
                             @"kind":[NSString stringWithFormat:@"%d",idRequest],
                             @"search":text
                             };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",SERVICE_URL,url] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"%@",responseObject);
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [[CSActivityViewController sharedObject]hideActivityView];
        
        NSArray *results = [(NSDictionary *)responseObject objectForKey:@"search_histories"];
        
        if (results.count == 0) {
            
            //handlerText.hidden = false;
        }
        else
        {
            
            //handlerText.hidden = true;
            
            for (NSDictionary * items in results) {
                NSDictionary * item = [items valueForKey:@"search"];
                
                [self.menuFilter addObject:item];
            }
        }
        
        
        [self.leftTable reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [[CSActivityViewController sharedObject]hideActivityView];
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}

#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
	
    [self.menuFilter removeAllObjects];
    
    if (![searchText isEqualToString:@""]) {
        
        
        if ([scope isEqualToString:@"People"]) {
            idRequest = 2;
        }
        else if ([scope isEqualToString:@"Brands"])
        {
            idRequest = 1;
        }
        else if ([scope isEqualToString:@"Item"])
        {
            idRequest = 3;
            
        }
        else if ([scope isEqualToString:@"News"])
        {
            idRequest = 5;
            
        }
        
        [self getDataWithRL:SERVICE_SEARCH_HISTORY andText:searchText];
    }
    else
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [[CSActivityViewController sharedObject]hideActivityView];
        [self.leftTable reloadData];
    }
}


-(void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [[CSActivityViewController sharedObject] showActivityView];
    
    [self filterContentForSearchText:searchBar.text scope:[[searchBar scopeButtonTitles]objectAtIndex:selectedScope]];
}

#pragma mark - Search Bar Delegate

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self filterContentForSearchText:searchBar.text scope:[[self.userSearchBar scopeButtonTitles] objectAtIndex:[self.userSearchBar selectedScopeButtonIndex]]];
}

- (void)searchBarSearchButtonClicked:(UISearchBar*)searchBar
{
    [searchBar resignFirstResponder];
    
    
    SearchPeopleViewController *searchPeopleController =  [self.storyboard instantiateViewControllerWithIdentifier:@"searchpeople"];
    searchPeopleController.searchString = searchBar.text;
    
    SearchNewsViewController *searchNewsController =  [self.storyboard instantiateViewControllerWithIdentifier:@"searchnews"];
    searchNewsController.searchString = searchBar.text;
    
    SearchItemsViewController *searchItemsController =  [self.storyboard instantiateViewControllerWithIdentifier:@"searchitems"];
    searchItemsController.searchString = searchBar.text;
    searchItemsController.searchid = @"";
    
    SearchBrandsViewController *searchBrandsController =  [self.storyboard instantiateViewControllerWithIdentifier:@"searchbrands"];
    searchBrandsController.searchString = searchBar.text;
    
    
    [self.viewDeckController toggleLeftViewAnimated:YES completion:^(IIViewDeckController *controller, BOOL success){
        
        
        switch (idRequest) {
            case 2:
            {
                self.viewDeckController.centerController = [[UINavigationController alloc] initWithRootViewController:searchPeopleController] ;
            }
                break;
            case 1:
            {
                self.viewDeckController.centerController = [[UINavigationController alloc] initWithRootViewController:searchBrandsController];
                
            }
                break;
            case 3:
            {
                self.viewDeckController.centerController = [[UINavigationController alloc] initWithRootViewController:searchItemsController];
            }
                break;
            case 5:
            {
                self.viewDeckController.centerController = [[UINavigationController alloc] initWithRootViewController:searchNewsController] ;
            }
                break;
                
            default:
                break;
        }
        
        searchBar.text = @"";
        
    }];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
    userSearchBar.scopeButtonTitles = nil;
    
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar;
{
    
    [self.menuFilter removeAllObjects];
    searchBar.text = @"";
    [searchBar resignFirstResponder];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isSearching"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"isSearching"
                                                        object:self];
    
    searchBar.showsCancelButton = YES;
    
    self.userSearchBar.scopeButtonTitles = @[@"People",
                                             @"Brands",
                                             @"Item",
                                             @"News"];
    
    self.userSearchBar.selectedScopeButtonIndex = 0;
    
    
    
    [searchBar setFrame:CGRectMake(0, 20, 320 - 80, 44)];
    
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
    [self.viewDeckController setLeftSize:320];
    
    searchBar.showsScopeBar = TRUE;
    
    [searchBar sizeToFit];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    [leftTable setFrame:CGRectMake(0, 102, screenWidth, 333)];
    
    [searchBar becomeFirstResponder];
    
    isSearching = TRUE;
    
    [self.leftTable reloadData];
    
    //[self filterContentForSearchText:searchBar.text scope:[[self.userSearchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"isSearching"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"isSearching"
                                                        object:self];
    
    self.viewDeckController.leftSize = 320-80;
    
    [leftTable setFrame:CGRectMake(0, 235, 240, 333)];
    
    [searchBar setFrame:CGRectMake(0, 63, 320 - 80, 44)];
    
    isSearching = FALSE;
    
    [self.leftTable reloadData];
}
- (IBAction)profileButton:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"profiletimelineID"];
    [self showProfileViewController];
    
}
@end
