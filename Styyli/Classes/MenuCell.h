//
//  MenuCell.h
//  STYYLI
//
//  Created by Agil Febrianistian on 2/25/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleText;
@property (weak, nonatomic) IBOutlet UIButton *iconButton;

@end
