//
//  ProfileSettingViewController.h
//  STYYLI
//
//  Created by Agil Febrianistian on 5/18/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "UserInfo.h"
#import "CSActivityViewController.h"
#import "PECropViewController.h"
#import "Constant.h"
#import "UserInfo.h"

@interface ProfileSettingViewController : UITableViewController<UITableViewDelegate, UITableViewDataSource,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate>
{
    bool isUp;
    NSString * imageName;
    NSData *imageData;
}

@property (weak, nonatomic) IBOutlet UIButton *profileImage;

@property (weak, nonatomic) IBOutlet UITableView *infoTable;

@property (strong, nonatomic) IBOutlet UITableViewCell *bioCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *locationCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *websiteCell;


@property (weak, nonatomic) IBOutlet UITextField *bioField;
@property (weak, nonatomic) IBOutlet UITextField *locationField;
@property (weak, nonatomic) IBOutlet UITextField *websiteField;

- (IBAction)bgTapped:(id)sender;
- (IBAction)imageChange:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (strong, nonatomic) NSString* idProfile;

@property (strong, nonatomic) NSString* bioText;
@property (strong, nonatomic) NSString* locationText;
@property (strong, nonatomic) NSString* websiteText;


@end
