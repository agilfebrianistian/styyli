//
//  POTDViewController.h
//  STYYLI
//
//  Created by Agil Febrianistian on 7/14/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemDetailsCell.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "Constant.h"
#import "UserInfo.h"
#import "ItemDetailsViewController.h"

@interface POTDViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>


@property (weak, nonatomic) IBOutlet UICollectionView *myCollection;
@property (nonatomic, retain) NSMutableArray *allTableData;


@property (weak, nonatomic) IBOutlet UILabel *handlerText;

@end
