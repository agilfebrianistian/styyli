//
//  RegisterViewController.h
//  Styyli
//
//  Created by Agil Febrianistian on 2/7/14.
//  Copyright (c) 2014 Agil Febrianistian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIKit+AFNetworking.h"
#import "PECropViewController.h"
#import "RHZAppDelegate.h"
#import "Constant.h"

@interface RegisterViewController : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate>{
    bool isUp;
    NSString * imageName;
    NSString * genderName;
    NSData *imageData;
    int direction;
    int shakes;
    
    BOOL isUsernameAvailable;
    BOOL isEmailAvailable;
    
}

@property (weak, nonatomic) IBOutlet UITextField *emailTextfield;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextfield;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextfield;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextfield;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextfield;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextfield;
@property (weak, nonatomic) IBOutlet UITextField *birthdayTextfield;
@property (weak, nonatomic) IBOutlet UITextField *countryTextfield;

@property (weak, nonatomic) IBOutlet UISegmentedControl *genderSegment;

- (IBAction)bgTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *pictureButtonImage;
- (IBAction)pictureButton:(id)sender;


@property (nonatomic, retain) IBOutlet UIToolbar *accessoryView;
@property (nonatomic, retain) IBOutlet UIPickerView *pickerInput;
@property (nonatomic, retain) IBOutlet UIDatePicker *customInput;

@property (strong, nonatomic) IBOutlet UIScrollView *scroller;

@property (strong, nonatomic) NSArray *countryList;
@property (strong, nonatomic) NSMutableArray *pickerArray;


//warning text
@property (weak, nonatomic) IBOutlet UILabel *emailWarning;
@property (weak, nonatomic) IBOutlet UILabel *usernameWarning;
@property (weak, nonatomic) IBOutlet UILabel *passwordWarning;
@property (weak, nonatomic) IBOutlet UILabel *topWarning;
@property (weak, nonatomic) IBOutlet UIImageView *emailChecklist;
@property (weak, nonatomic) IBOutlet UIImageView *usernameChecklist;
@property (weak, nonatomic) IBOutlet UILabel *firstnameWarning;
@property (weak, nonatomic) IBOutlet UILabel *lastnameWarning;

@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;




@end
