//
//  FeedbackViewController.h
//  STYYLI
//
//  Created by Agil Febrianistian on 8/13/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "CSActivityViewController.h"
#import "Constant.h"
#import "UserInfo.h"

@interface FeedbackViewController : UIViewController
{
    bool isUp;
}


@property (retain, nonatomic) IBOutlet UIView *activityContainer;
+(FeedbackViewController *)sharedObject;
-(void)showActivityView;
-(void)hideActivityView;


@property (weak, nonatomic) IBOutlet UITextView *feedbackText;

- (IBAction)closeButton:(id)sender;
- (IBAction)sendButton:(id)sender;

@end
