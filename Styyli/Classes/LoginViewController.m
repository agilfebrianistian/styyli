//
//  ViewController.m
//  Applist
//
//  Created by Agil Febrianistian on 2/4/14.
//  Copyright (c) 2014 Agil Febrianistian. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController
@synthesize usernameText,passwordText,activityIndicator;

- (void)viewDidLoad
{
   
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

    [self.navigationController setNavigationBarHidden:NO animated:TRUE];
    

    UIImage *leftImage = [UIImage imageNamed:@"IconBackToTimeline"];
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton addTarget:self action:@selector(leftBarPressed:) forControlEvents:UIControlEventTouchUpInside];
    leftButton.bounds = CGRectMake( 0, 0, leftImage.size.width,  leftImage.size.height);
    [leftButton setImage:leftImage forState:UIControlStateNormal];

    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    
    UIEdgeInsets buttonEdges = UIEdgeInsetsMake(0, LOGO_INSET_ARROW, 0, -LOGO_INSET_ARROW);
    UIImage *leftImagea = [UIImage imageNamed:@"signin-logo.png"];
    UIButton *leftButtona = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButtona.bounds = CGRectMake(0, 0, leftImagea.size.width/2, leftImagea.size.height/2);
    [leftButtona setImage:leftImagea forState:UIControlStateNormal];
    [leftButtona setImageEdgeInsets:buttonEdges];
    [leftButtona setUserInteractionEnabled:NO];
    UIBarButtonItem *leftItema = [[UIBarButtonItem alloc] initWithCustomView:leftButtona];
    

    [self.navigationItem setLeftBarButtonItems:@[leftItem,leftItema] animated:YES];


    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    [self navigationItem].rightBarButtonItem = barButton;
    
    [self.navigationController setNavigationBarHidden:NO animated:TRUE];
    
    [usernameText becomeFirstResponder];
    
}



- (void)viewWillAppear:(BOOL)animated
{
    [self.view setAlpha:0];
    [UIView animateWithDuration:0.2
                          delay:0.25
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [self.view setAlpha:1.0];
                     }completion:nil];
}

- (IBAction) leftBarPressed:(id)sender
{
    NSLog(@"cancel");
    
    RHZAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    [UIView animateWithDuration:0.75f
                          delay:0.0f
                        options:UIViewAnimationOptionTransitionFlipFromRight
                     animations:^{
                         // Do your animations here.
                     }
                     completion:^(BOOL finished){
                         if (finished) {
                              [appDelegate transitToTutorialScreen];
                         }
                     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)submitButton:(id)sender {
    [self dismissKeyboard];
 
//    usernameText.text = @"endrs.begin@gmail.com";
//    passwordText.text = @"1qaz2wsx";
    
//    usernameText.text = @"kuyainside";
//    passwordText.text = @"testing---";
    
    if (usernameText.text.length == 0) {
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please input you username" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
    }
    else if (passwordText.text.length == 0)
    {
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please input you password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
    }
    else{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        [activityIndicator startAnimating];
        
        [self doLogin];
    }
}
#pragma mark - Utilities

-(void)doLogin{

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    NSString * deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"];
    if (deviceToken.length == 0) {
        deviceToken = @"";
    }
    
    
    NSDictionary *params = @{@"api_key":API_KEY,
                             @"user[email]": usernameText.text,
                             @"user[password]": passwordText.text,
                             @"device_id": deviceToken};
    
    [manager POST:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_LOGIN] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         [activityIndicator stopAnimating];
        
        NSLog(@"%@", responseObject);
        
        if ([[responseObject valueForKey:@"success"]integerValue] == 1) {
            
            [UserInfo sharedObject].tokenUser = [responseObject valueForKey:@"auth_token"];
            
            NSDictionary * userItem =[responseObject valueForKey:@"user"];

            NSLog(@"%@", [userItem valueForKey:@"username"]);
            
            [UserInfo sharedObject].firstNameUser   = [userItem valueForKey:@"first_name"];
            [UserInfo sharedObject].lastNameUser    = [userItem valueForKey:@"last_name"];
            [UserInfo sharedObject].birthdayUser    = [userItem valueForKey:@"birthday"];
            [UserInfo sharedObject].cityUser        = [userItem valueForKey:@"city"];
            [UserInfo sharedObject].countryUser     = [userItem valueForKey:@"country"];
            [UserInfo sharedObject].genderUser      = [userItem valueForKey:@"gender"];
            [UserInfo sharedObject].usernameUser    = [userItem valueForKey:@"username"];
            [UserInfo sharedObject].emailUser       = [userItem valueForKey:@"email"];
            [UserInfo sharedObject].idUser          = [userItem valueForKey:@"id"];
            [UserInfo sharedObject].phoneUser       = [userItem valueForKey:@"phone"];
            [UserInfo sharedObject].zipcodeUser     = [userItem valueForKey:@"zipcode"];
            
            //            NSDictionary * urlImage =[userItem valueForKey:@"image"];
            //            [UserInfo sharedObject].avatarUser      = [urlImage valueForKey:@"url"];
            
            [UserInfo sharedObject].avatarUser      = [userItem valueForKey:@"image_small"];
            
            NSUserDefaults *login = [NSUserDefaults standardUserDefaults];
            [login setValue:[UserInfo sharedObject].tokenUser forKey:@"loginAuth"];
            [login synchronize];
            
            RHZAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
            [appDelegate transitToHomeScreen];
            
        }
        else
        {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [activityIndicator stopAnimating];
            
            UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:[[responseObject valueForKey:@"message"] objectAtIndex:0] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [message show];
        }
        

        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         [activityIndicator stopAnimating];
        NSLog(@"Error: %@", error);
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
    }];
}


#pragma mark - Keyboard controller

- (IBAction)bgTapped:(id)sender {
    [self dismissKeyboard];
}

- (IBAction)forgotPasswordBU:(id)sender {

 [[ForgotPassViewController sharedObject] showActivityView];

}



-(void)dismissKeyboard {
    if ([usernameText isFirstResponder])
    {
        [usernameText resignFirstResponder];
    }
    else if ([passwordText isFirstResponder])
    {
        [passwordText resignFirstResponder];
    }
}

#pragma mark - Textfield Animation

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}


- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 80; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    
    return NO;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    NSString *inputText = [[alertView textFieldAtIndex:0] text];
    
    if (buttonIndex !=0) {
        
        if([title isEqual:[NSString stringWithFormat:@"Ok"]])
        {
            
            NSLog(@"%@",inputText);
            
        }
    }
    
    
}

@end
