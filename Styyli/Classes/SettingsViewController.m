//
//  SettingsViewController.m
//  STYYLI
//
//  Created by Agil Febrianistian on 5/18/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import "SettingsViewController.h"
#import "UIButton+WebCache.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController
@synthesize profileImage,infoTable;
@synthesize fullnameTextfield,lastnameTextfield;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    profileImage.layer.cornerRadius = 50.0f;
    profileImage.layer.masksToBounds=YES;
    profileImage.layer.borderColor=[[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f] CGColor];
    profileImage.layer.borderWidth= 2.5f;
    
    
    if([UserInfo sharedObject].avatarUser==(id) [NSNull null])
        [profileImage setBackgroundImage:PLACEHOLDER_AVATAR forState:UIControlStateNormal] ;
    else
        [profileImage sd_setBackgroundImageWithURL:[NSURL URLWithString:[UserInfo sharedObject].avatarUser] forState:UIControlStateNormal placeholderImage:PLACEHOLDER_AVATAR options:SDWebImageRefreshCached];
    
    
    
    [self addNavigationItem];
    
     self.infoTable.tableHeaderView = self.headerView;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    if ([self checkFacebookSession]) {
       [self.facebookButton setImage:[UIImage imageNamed:@"switch_on"]];
    }
    
    
    [[CSActivityViewController sharedObject]showActivityView];
    [self getData];
    
}

#pragma mark -
#pragma mark get data from server

- (void)getData
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser,
                             };
    
    [manager GET:[NSString stringWithFormat:@"%@users/%@.json",SERVICE_URL,[UserInfo sharedObject].idUser] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"%@",responseObject);
        
        [[CSActivityViewController sharedObject]hideActivityView];
        
        NSDictionary * userItem =[responseObject valueForKey:@"user"];
        
        if ([userItem valueForKey:@"first_name"] != [NSNull null]) {
            fullnameTextfield.text       = [userItem valueForKey:@"first_name"];
        }
        
        if ([userItem valueForKey:@"last_name"] != [NSNull null]) {
            lastnameTextfield.text       = [userItem valueForKey:@"last_name"];
        }

        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [[CSActivityViewController sharedObject]hideActivityView];
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addNavigationItem
{
    UIImage *leftImage = [UIImage imageNamed:@"icon_menu"];
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton addTarget:self action:@selector(showLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    leftButton.bounds = CGRectMake( 0, 0, leftImage.size.width, leftImage.size.height );
    [leftButton setImage:leftImage forState:UIControlStateNormal];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    self.navigationItem.leftBarButtonItem = leftItem ;
    
    
    CGRect frame = CGRectMake(0, 0, 0, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame] ;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:16.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"Settings";
    self.navigationItem.titleView = label;
    
    
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(showRightMenu)];
    searchButton.tintColor = [UIColor whiteColor];
    
    [self.navigationItem setRightBarButtonItem:searchButton animated:YES];
}

- (void)showLeftMenu
{
    [self.viewDeckController performSelectorOnMainThread:@selector(toggleLeftView) withObject:nil waitUntilDone:YES];
}


- (void)showRightMenu
{
    [self dismissKeyboard];
    [[CSActivityViewController sharedObject]showActivityView];
    
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self sendData];
    });

}


-(void)sendData
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    NSDictionary *params = @{@"api_key"          : API_KEY,
                             @"auth_token"       :[UserInfo sharedObject].tokenUser,
                             @"user[first_name]" : fullnameTextfield.text,
                             @"user[last_name]"  : lastnameTextfield.text
                             };
    
    
    [manager POST:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_PROFILE_SETTING] parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         if (imageData != NULL) {
             [formData appendPartWithFileData:imageData name:@"user[image]" fileName:@"photoname.jpg" mimeType:@"image/jpeg"];
         }
     }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSLog(@"%@",responseObject);
              
              [[CSActivityViewController sharedObject]hideActivityView];
              
              [UserInfo sharedObject].firstNameUser = fullnameTextfield.text;
              
              [UserInfo sharedObject].lastNameUser = lastnameTextfield.text;
              
              [ALToastView toastInView:self.headerView withText:@"Data Saved"];
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              NSLog(@"Error: %@", error);
              [[CSActivityViewController sharedObject]hideActivityView];
              UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
              [message show];
              
          }];
}

#pragma mark -
#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 2;
            break;
        case 1:
            return 1;
            break;
        case 2:
            return 1;
            break;
        case 3:
            return 3;
            break;
        case 4:
            return 4;
            break;
            
        default:
            break;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    switch (indexPath.section) {
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                {
                    cell = self.fullNameCell;
                }
                    break;
                case 1:
                {
                    cell = self.lastNameCell;
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    cell = self.passwordCell;
                }
                    break;
                default:
                    break;
            }
        }
            break;
            
        case 2:
        {
            switch (indexPath.row) {
                case 0:
                {
                    cell = self.notificationCell;
                }
                    break;
                default:
                    break;
            }
        }
            break;
            
        case 3:
        {
            switch (indexPath.row) {
                case 0:
                {
                    cell = self.facebookCell;
                }
                    break;
                case 1:
                {
                    cell = self.twitterCell;
                }
                    break;
                case 2:
                {
                    cell = self.instagramCell;
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
        case 4:
        {
            switch (indexPath.row) {
                case 0:
                {
                    cell = self.feedbackCell;
                }
                    break;
                case 1:
                {
                    cell = self.supportCell;
                }
                    break;
                case 2:
                {
                    cell = self.versionCell;
                }
                    break;
                case 3:
                {
                    cell = self.logoutCell;
                }
                    break;
                default:
                    break;
            }
        }
            break;
            
        default:
            break;
    }
    
    return cell;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.infoTable]) {

        return NO;
    }
    
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 1 && indexPath.row == 0) {
        [[ChangePasswordViewController sharedObject] showActivityView];
    }
    else if (indexPath.section == 3 && indexPath.row == 0) {

        FBLoginView *loginview = [[FBLoginView alloc] init];
        loginview.hidden = true;
        loginview.delegate = self;
        
        [self.view addSubview:loginview];
        
        
        [loginview.subviews[0] sendActionsForControlEvents:UIControlEventTouchUpInside];
    
    
    }
    
    else if (indexPath.section == 3 && indexPath.row == 1) {
        
        
        TWTRLogInButton* logInButton =  [TWTRLogInButton
                                         buttonWithLogInCompletion:
                                         ^(TWTRSession* session, NSError* error) {
                                             if (session) {
                                                 NSLog(@"signed in as %@", [session userName]);
                                                 
                                                 [self.twitterButton setImage:[UIImage imageNamed:@"switch_on"]];
                                                 
                                             } else {
                                                 NSLog(@"error: %@", [error localizedDescription]);
                                             }
                                         }];
        logInButton.center = self.view.center;
        [logInButton setHidden:true];
        
        [self.view addSubview:logInButton];
        
        [logInButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        
    
    }
    
    else if (indexPath.section == 4 && indexPath.row == 0) {
        [[FeedbackViewController sharedObject] showActivityView];
    }
    
    else if (indexPath.section == 4 && indexPath.row == 1) {
        SupportViewController *controller =  [self.storyboard instantiateViewControllerWithIdentifier:@"support"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    else if (indexPath.section == 4 && indexPath.row == 3) {
        
        
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Information"
                                                          message:@"Are you sure want to logout?"
                                                         delegate:self
                                                cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:@"Yes",Nil];
        
        [message show];
        
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
}



#pragma mark - Utilities

-(void)doLogout{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key":API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser};
    
    [manager DELETE:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_LOGOUT] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

        
        NSLog(@"%@", responseObject);
        
        NSUserDefaults *login = [NSUserDefaults standardUserDefaults];
        [login setValue:NULL forKey:@"loginAuth"];
        [login synchronize];
        

        RHZAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        [appDelegate transitToTutorialScreen];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

        NSLog(@"Error: %@", error);
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
    }];
}


#pragma mark - Keyboard delegate

- (IBAction)bgTapped:(id)sender {
    [self dismissKeyboard];
}

- (IBAction)imageChange:(id)sender {
    
    [self dismissKeyboard];
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Upload Image"
                              message:@""
                              delegate:self
                              cancelButtonTitle:@"Cancel"
                              otherButtonTitles:  @"Camera",@"Gallery", nil];
    
    alertView.transform = CGAffineTransformTranslate( alertView.transform, 0.0, 100.0 );
    
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqual:[NSString stringWithFormat:@"Camera"]])
    {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = self;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    else if([title isEqualToString:@"Gallery"])
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.delegate = self;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    else if([title isEqualToString:@"Yes"])
    {
        [self.viewDeckController removeFromParentViewController];
        
        [self doLogout];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info; {
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    [profileImage setBackgroundImage:image forState:UIControlStateNormal];
    
    
    [picker dismissViewControllerAnimated:YES completion:^{
        [self openEditor:nil];
        
    }];
    
}


- (IBAction)openEditor:(id)sender
{
    
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    [controller setCropAspectRatio:1];
    [controller setKeepingCropAspectRatio:true];
    
    controller.image = [profileImage backgroundImageForState:UIControlStateNormal];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:navigationController animated:YES completion:NULL];
    
}

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    
    
    [profileImage setBackgroundImage:croppedImage forState:UIControlStateNormal];
    
    NSString  *pngPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@.jpg",@"photoname"]];
    
    [UIImageJPEGRepresentation(croppedImage, 0) writeToFile:pngPath atomically:NO];
    
    imageName = pngPath;
    
    imageData = UIImageJPEGRepresentation([profileImage backgroundImageForState:UIControlStateNormal], 0.5);
    
}


- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}



-(void)dismissKeyboard {
    if ([fullnameTextfield isFirstResponder])
        [fullnameTextfield resignFirstResponder];
    else if ([lastnameTextfield isFirstResponder])
        [lastnameTextfield resignFirstResponder];
}


#pragma mark - Textfield Animation

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (!isUp) {
        [self animateTextField: textField up: YES];
        isUp = YES;
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    textField.layer.borderColor=[[UIColor clearColor]CGColor];
    
    if (isUp == YES) {
        [self animateTextField: textField up: NO];
        isUp = NO;
    }
    
}



- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 80; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    
    return NO;
}

#pragma mark - FBLoginViewDelegate

-(BOOL)checkFacebookSession
{
    if([FBSession activeSession].state == FBSessionStateCreatedTokenLoaded)
    {
        return YES;
    }
    else{
        return NO;
    }
}


- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {

    
    [self.facebookButton setImage:[UIImage imageNamed:@"switch_on"]];

}

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {

}

- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
//    FBLinkShareParams *p = [[FBLinkShareParams alloc] init];
//    p.link = [NSURL URLWithString:@"http://developers.facebook.com/ios"];

    [self.facebookButton setImage:[UIImage imageNamed:@"switch_off"]];
    

}

- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error {
    NSLog(@"FBLoginView encountered an error=%@", error);
}
@end
