//
//  comentCell.m
//  Styyli
//
//  Created by Agil Febrianistian on 2/14/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import "CommentCell.h"

@implementation CommentCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)initText{
    self.contentText.delegate = self;
    self.contentText.userInteractionEnabled = YES;
    self.contentText.numberOfLines = 0;
    self.contentText.lineBreakMode = NSLineBreakByCharWrapping;
}

- (void)selectedMention:(NSString *)string {
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Selected" message:string delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//    [alert show];
    
    NSArray *subStrings = [string componentsSeparatedByString:@"@"];
    [self getDataWithUsername:[subStrings objectAtIndex:1]];
}
- (void)selectedHashtag:(NSString *)string {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSArray *subStrings = [string componentsSeparatedByString:@"#"];
    
    [[NSUserDefaults standardUserDefaults] setInteger:3 forKey:@"timelineID"];
    [[NSUserDefaults standardUserDefaults] setValue:[subStrings objectAtIndex:1] forKey:@"hashtagString"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"hashtagData"
                                                        object:self];
}
- (void)selectedLink:(NSString *)string {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Selected" message:string delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}

- (void)getDataWithUsername:(NSString*)username
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser
                             };
    
    [manager GET:[NSString stringWithFormat:@"%@mention/%@.json",SERVICE_URL,username] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"%@",responseObject);
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if ([[responseObject valueForKey:@"success"]integerValue] == 1) {
            
            NSString * temp = [[responseObject valueForKey:@"user"]valueForKey:@"id"];
            
            [[NSUserDefaults standardUserDefaults] setValue:temp forKey:@"profileID"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DataUpdated"
                                                                object:self];
        }
        else
        {
            UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Sorry" message:@"User Not Found" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [message show];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}

@end
