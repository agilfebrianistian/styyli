//
//  RHZNewsViewController.m
//  Styyli
//
//  Created by R. Hafidhullah Zakariyya on 12/23/13.
//  Copyright (c) 2013 Mirosea. All rights reserved.
//

#import "RHZNewsViewController.h"
#import "IIViewDeckController.h"

@interface RHZNewsViewController ()

@end

@implementation RHZNewsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self addNavigationItem];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Add Navigation Item

- (void)addNavigationItem
{
    UIImage *leftImage = [UIImage imageNamed:@"IconItemLeftProfile"];
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton addTarget:self action:@selector(showLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    leftButton.bounds = CGRectMake( 0, 0, leftImage.size.width, leftImage.size.height );
    [leftButton setImage:leftImage forState:UIControlStateNormal];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    

    
    self.navigationItem.leftBarButtonItem = leftItem;

}

#pragma mark -
#pragma mark Button Function

- (void)showLeftMenu
{
    [self.viewDeckController performSelectorOnMainThread:@selector(toggleLeftView) withObject:nil waitUntilDone:YES];
}

@end
