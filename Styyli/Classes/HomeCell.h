//
//  HomeCell.h
//  STYYLI
//
//  Created by Agil Febrianistian on 7/6/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *newsImage;
@property (weak, nonatomic) IBOutlet UILabel *newsText;


@end
