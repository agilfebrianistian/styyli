//
//  SearchItemsViewController.h
//  STYYLI
//
//  Created by Agil Febrianistian on 7/20/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemDetailsCell.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "Constant.h"
#import "UserInfo.h"
#import "RHZAppDelegate.h"

@interface SearchItemsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *myCollection;
@property (nonatomic, retain) NSMutableArray *allTableData;


@property (weak, nonatomic) IBOutlet UILabel *handlerText;

@property (strong, nonatomic) NSString* searchString;

@property (strong, nonatomic) NSString* searchid;
@end
