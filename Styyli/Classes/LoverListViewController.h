//
//  LoverListViewController.h
//  STYYLI
//
//  Created by Agil Febrianistian on 5/26/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoverListCell.h"
#import "AFNetworking.h"
#import "CSActivityViewController.h"
#import "UIImageView+AFNetworking.h"
#import "Constant.h"
#import "UserInfo.h"
#import "RHZAppDelegate.h"

@interface LoverListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>


@property (weak, nonatomic) IBOutlet UITableView *loverListTable;

@property (nonatomic) int currId;

@property (strong, nonatomic) NSArray* allTableData;

@property (strong, nonatomic) NSString* searchString;

@end
