//
//  TimelineViewController.h
//  STYYLI
//
//  Created by Agil Febrianistian on 3/10/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "TimelineHeaderCell.h"
#import "TimelineCell.h"
#import "RHZCommentViewController.h"
#import "RHZAppDelegate.h"
#import "ImagePostViewController.h"
#import "ImageFilterViewController.h"
#import "IFFiltersViewController.h"
#import "postData.h"
#import "LoverListViewController.h"
#import "DEMOPhoto.h"
#import "DEMOTag.h"
#import "CHDraggingCoordinator.h"
#import "ALToastView.h"
#import "ItemDetailsViewController.h"
#import "UIImageView+LBBlurredImage.h"

enum {
    kTimelineRow = 12,
};


@interface TimelineViewController : UITableViewController<UIAlertViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPickerViewAccessibilityDelegate,UIPickerViewDelegate,IIViewDeckControllerDelegate,CHDraggingCoordinatorDelegate,UIActionSheetDelegate>
{
    
    BOOL isLeftOpen;
    int moreID;
    int currentPage;
    int totalPage;
}



@property (strong, nonatomic)  CHDraggableView *draggableView;

@property (strong, nonatomic) NSMutableArray* allTableData;

@property (weak, nonatomic) IBOutlet UITableView *myTableView;

@property NSUInteger countRefresh;

//@property (weak, nonatomic) IBOutlet UIButton *cameraButton;

@property (nonatomic, strong) NSMutableArray *selectedIndexPaths;

@property (nonatomic, strong) NSMutableArray *selectedImagePaths;

@property (strong) NSArray *photos;

@property (strong, nonatomic) CHDraggingCoordinator *draggingCoordinator;

@property int timelineType;

@end
