//
//  NotificationListViewController.h
//  STYYLI
//
//  Created by Agil Febrianistian on 8/10/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "CSActivityViewController.h"
#import "UIImageView+AFNetworking.h"
#import "Constant.h"
#import "UserInfo.h"
#import "NotificationCell.h"
#import "TimelineViewController.h"

@interface NotificationListViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *notificationTable;


@property (strong, nonatomic) NSArray* allTableData;

@end
