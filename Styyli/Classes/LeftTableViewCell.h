//
//  LeftTableViewCell.h
//  STYYLI
//
//  Created by Agil Febrianistian on 6/13/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LeftTableViewCell;
@interface LeftTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnExpand;
@property (weak, nonatomic) IBOutlet UIButton *iconButton;

@end
