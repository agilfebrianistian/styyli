//
//  RHZTimelineCell.h
//  Styyli
//
//  Created by R. Hafidhullah Zakariyya on 12/30/13.
//  Copyright (c) 2013 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "IIViewDeckController.h"
#import "RHZAppDelegate.h"
#import "DEMOPhoto.h"
#import "DEMOTag.h"
#import "EBTagPopover.h"
#import "EBTagPopoverDelegate.h"
#import "AMAttributedHighlightLabel.h"
#import "TimelineViewController.h"

@interface TimelineCell : UITableViewCell<iCarouselDataSource,iCarouselDelegate,AMAttributedHighlightLabelDelegate>
{
    iCarousel *itemsList;
    iCarousel *loversList;
}


@property (weak, nonatomic) IBOutlet UIButton *moreButton;
@property (weak, nonatomic) IBOutlet UIButton *tagButton;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UIButton *viewLoveButton;
@property (weak, nonatomic) IBOutlet UIImageView *mainPicture;
@property (weak, nonatomic) IBOutlet UILabel *captionText;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIView *brandView;
@property (weak, nonatomic) IBOutlet UIView *likeView;
@property (weak, nonatomic) IBOutlet UIView *captionView;
@property (weak, nonatomic) IBOutlet UIView *buttonBarView;
@property (weak, nonatomic) IBOutlet UIButton *pictButton;

@property (weak, nonatomic) IBOutlet AMAttributedHighlightLabel *captionAtributed;

@property (strong, nonatomic) NSArray* itemsData;
@property (strong, nonatomic) NSArray* loversData;


-(void)initText;
-(void)initItems;
-(void)initLovers;
-(void)initTagPopoverFrom : (NSMutableArray*)tags;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundCell;


@end
