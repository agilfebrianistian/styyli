//
//  ExpandableTableViewController.h
//  ExpandableTable
//
//  Created by Manpreet Singh on 06/12/13.
//  Copyright (c) 2013 Manpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "ImagePostViewController.h"
#import "CSActivityViewController.h"

@interface CategoryTableViewController : UITableViewController
{
    BOOL isexpanded;
    
}

@property (nonatomic, retain) NSMutableArray *topCategory;

@property (strong, nonatomic) IBOutlet UITableView *menuTableView;

@end
