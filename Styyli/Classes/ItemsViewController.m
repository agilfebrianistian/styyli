//
//  ItemsViewController.m
//  STYYLI
//
//  Created by Agil Febrianistian on 7/13/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import "ItemsViewController.h"
#import "UIImageView+WebCache.h"
#import "CSActivityViewController.h"


@interface ItemsViewController ()

@end

@implementation ItemsViewController
@synthesize allTableData;
@synthesize filteredTableData;
@synthesize searchBar;
@synthesize isFiltered;
@synthesize myCollection;
@synthesize categoryButton;
@synthesize handlerText;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    allTableData = [[NSMutableArray alloc]init];
    
    [self addNavigationItem];
    
    isFiltered = FALSE;
    searchBar.text = @"";
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark Add Navigation Item

- (void)addNavigationItem
{
    
    UIImage *leftImage = [UIImage imageNamed:@"IconBackToTimeline"];
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton addTarget:self action:@selector(showLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    leftButton.bounds = CGRectMake( 0, 0, leftImage.size.width, leftImage.size.height );
    [leftButton setImage:leftImage forState:UIControlStateNormal];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    
    self.navigationItem.leftBarButtonItem = leftItem;

    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    CGRect frame = CGRectMake(0, 0, 0, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame] ;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:16.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"Items";
    self.navigationItem.titleView = label;
    
}

- (void)showLeftMenu
{
    NSArray *tempViewControllers = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    [self.navigationController popToViewController:[tempViewControllers objectAtIndex:[tempViewControllers count]-2] animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    itemCategory = 0;
    [categoryButton setTitle:@"ALL" forState:UIControlStateNormal];

    [[CSActivityViewController sharedObject] showActivityView];
    [self refreshWithStatus:itemCategory];
}

- (void)refreshWithStatus:(int)status
{
    

    NSString * shop     = @"false";
    NSString * deal     = @"false";
    NSString * loved    = @"false";

    if (status == 1) {

        shop = @"true";
    }
    else if (status == 2) {

        deal = @"true";
    }
    else if (status == 3) {

        loved = @"true";
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token": [UserInfo sharedObject].tokenUser,
                             @"search" : @"",
                             @"shop" :shop,
                             @"deals" : deal,
                             @"lovers" : loved
                             };
    
    
    NSLog(@"%@",params);
    
    [manager GET:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_ITEMS] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [[CSActivityViewController sharedObject] hideActivityView];
        
        allTableData = [[NSMutableArray alloc]init];
        
        NSArray *results = [(NSDictionary *)responseObject objectForKey:@"items"];
        
        if (results.count == 0) {
            
            handlerText.hidden = false;
        }
        else
        {
            
            handlerText.hidden = true;
            
            for (NSDictionary * items in results) {
                NSDictionary * item = [items valueForKey:@"item"];
                
                [allTableData addObject:item];
            }
        }
        
        
        // Reload Table View
        [self.myCollection reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[CSActivityViewController sharedObject] hideActivityView];
        NSLog(@"%@",error);
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    int rowCount;
    if(self.isFiltered)
        rowCount = filteredTableData.count;
    else
        rowCount = allTableData.count;
    
    return rowCount;
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray  * items =[allTableData objectAtIndex:indexPath.row];
    
    ItemDetailsCell *cell = (ItemDetailsCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ItemDetailsCell" forIndexPath:indexPath];
    
    if(isFiltered)
        items = [filteredTableData objectAtIndex:indexPath.row];
    else
        items = [allTableData objectAtIndex:indexPath.row];
    
    NSLog(@"%@",items);
    
    
    cell.itemBrand.text = [items valueForKey:@"brand_name"];
    cell.itemLabel.text = [items valueForKey:@"name"];
    cell.itemPrice.text = [items valueForKey:@"price"];
    
    __block UIActivityIndicatorView *activityIndicator;
    __weak UIImageView *weakImageView = cell.itemImage;
    
    [cell.itemImage sd_setImageWithURL:[items valueForKey:@"image_url"]
                      placeholderImage:PLACEHOLDER_IMAGE
                               options:SDWebImageProgressiveDownload
                              progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                  if (!activityIndicator) {
                                      [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite]];
                                      activityIndicator.center = weakImageView.center;
                                      [activityIndicator startAnimating];
                                  }
                              }
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                 [activityIndicator removeFromSuperview];
                                 activityIndicator = nil;
                             }];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)searchBarSearchButtonClicked:(UISearchBar*)searchBar
{
    [self.searchBar resignFirstResponder];
}
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    self.searchBar.showsCancelButton = NO;
    
    return YES;
}


- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    
    [self.searchBar resignFirstResponder];
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar;
{
    
    isFiltered = FALSE;
    [myCollection reloadData];
    
    self.searchBar.text = @"";
    [self.searchBar resignFirstResponder];
    
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    self.searchBar.showsCancelButton = YES;
    return YES;
}

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    
    
    if(text.length == 0)
    {
        isFiltered = FALSE;
    }
    else
    {
        isFiltered = true;
        filteredTableData = [[NSMutableArray alloc] init];
        
        for (NSMutableArray  * items in allTableData)
        {
            
            NSRange nameRange           = [[items valueForKey:@"name"] rangeOfString:text options:NSCaseInsensitiveSearch];
            NSRange subtitleRange       = [[items valueForKey:@"brand_name"] rangeOfString:text options:NSCaseInsensitiveSearch];
            
            if(nameRange.location != NSNotFound || subtitleRange.location != NSNotFound)
            {
                [filteredTableData addObject:items];
            }
        }
    }
    
    [self.myCollection reloadData];
}




- (IBAction)categoryButton:(id)sender {
    
    NSArray *menuItems =
    @[
      
      [KxMenuItem menuItem:@"ALL"
                     image:nil
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"SHOP"
                     image:nil
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"DEAL"
                     image:nil
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"MOST LOVED"
                     image:nil
                    target:self
                    action:@selector(pushMenuItem:)]
      ];
    

    for (KxMenuItem *title in menuItems) {
        
        title.alignment = NSTextAlignmentCenter;

    }
    
    [KxMenu showMenuInView:self.view fromRect:categoryButton.frame  menuItems:menuItems];
 
    
}



- (void) pushMenuItem:(id)sender
{

    KxMenuItem * menu = (KxMenuItem*)sender;
    [categoryButton setTitle:menu.title forState:UIControlStateNormal];
    
    if ([menu.title isEqualToString:@"ALL"]) {
        itemCategory = 0;
    }
    else if ([menu.title isEqualToString:@"SHOP"]) {
        itemCategory = 1;
    }
    else if ([menu.title isEqualToString:@"DEAL"]) {
        itemCategory = 2;
    }
    else if ([menu.title isEqualToString:@"MOST LOVED"]) {
        itemCategory = 3;
    }
    
    [[CSActivityViewController sharedObject] showActivityView];
    [self refreshWithStatus:itemCategory];

}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray  * items =[allTableData objectAtIndex:indexPath.row];
    
    if(isFiltered)
    {
        items = [filteredTableData objectAtIndex:indexPath.row];
    }
    else
    {
        items = [allTableData objectAtIndex:indexPath.row];
    }
    
    ItemDetailsViewController *controller =  [self.storyboard instantiateViewControllerWithIdentifier:@"itemdetails"];
        controller.itemId = [items valueForKey:@"id"];
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
}

@end
