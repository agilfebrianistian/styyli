//
//  RHZCommentViewController.m
//  Styyli
//
//  Created by R. Hafidhullah Zakariyya on 12/29/13.
//  Copyright (c) 2013 Mirosea. All rights reserved.
//

#import "RHZCommentViewController.h"
#import "UIImageView+LBBlurredImage.h"
#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"


@interface RHZCommentViewController ()

@end

@implementation RHZCommentViewController
@synthesize currId,allTableData,commentTableView,textField,commentType;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addNavigationItem];
    
    [self.backgroundImage sd_setImageWithURL:[NSURL URLWithString:self.imageURL] placeholderImage:PLACEHOLDER_IMAGE options:SDWebImageRefreshCached];

    
    [self.backgroundImage setImageToBlur:self.backgroundImage.image
                              blurRadius:kLBBlurredImageDefaultBlurRadius
                         completionBlock:nil];
    
    double delayInSeconds = 0.2;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [self initKeyboard];
        
    });

    
    // Initialize Refresh Control
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    
    // Configure Refresh Control
    [refreshControl addTarget:self action:@selector(refreshComment:) forControlEvents:UIControlEventValueChanged];
    
    // Configure View Controller
    //[self setRefreshControl:refreshControl];
    
    
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = self.commentTableView;

    tableViewController.refreshControl = refreshControl;
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUpdatedData:)
                                                 name:@"DataUpdated"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hashtagRefresh:)
                                                 name:@"hashtagData"
                                               object:nil];
    
    locationText = @"";
    
    [self locationCheck];

    
    //init keyboard
    [self refreshComment:NULL];

}


-(void)viewWillDisappear:(BOOL)animated

{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"DataUpdated"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"hashtagData"
                                                  object:nil];
    

    [self.view removeKeyboardControl];
    
    
}

-(void)handleUpdatedData:(NSNotification *)notification {
    NSLog(@"recieved");
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"profiletimelineID"];
    self.viewDeckController.centerController = SharedAppDelegate.profileController;
}

-(void)hashtagRefresh:(NSNotification *)notification {
        [self.navigationController popViewControllerAnimated:YES];
}

-(void)initKeyboard
{
    
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f,
                                                                     self.view.bounds.size.height - 40.0f,
                                                                     self.view.bounds.size.width,
                                                                     40.0f)];
    toolBar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:toolBar];

    textField = [[UITextField alloc] initWithFrame:CGRectMake(10.0f, 6.0f,toolBar.bounds.size.width - 20.0f - 68.0f, 30.0f)];
    textField.borderStyle           = UITextBorderStyleRoundedRect;
    textField.autoresizingMask      = UIViewAutoresizingFlexibleWidth;
    textField.returnKeyType         = UIReturnKeyNext;
    textField.clearButtonMode       = UITextFieldViewModeWhileEditing;
    textField.autocorrectionType    = UITextAutocorrectionTypeNo;
    textField.keyboardAppearance    = UIKeyboardAppearanceAlert;
    textField.keyboardType          = UIKeyboardTypeDefault;
    textField.textAlignment         = NSTextAlignmentLeft;
    
    [toolBar addSubview:textField];
    
    UIButton *sendButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    sendButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [sendButton setTitle:@"Send" forState:UIControlStateNormal];
    sendButton.frame = CGRectMake(toolBar.bounds.size.width - 68.0f,
                                  6.0f,
                                  58.0f,
                                  29.0f);
    
    [sendButton addTarget:self action:@selector(postdata) forControlEvents:UIControlEventTouchUpInside];
    [toolBar addSubview:sendButton];
    
    
    self.view.keyboardTriggerOffset = toolBar.bounds.size.height;
    
    [self.view addKeyboardPanningWithActionHandler:^(CGRect keyboardFrameInView) {
        
        CGRect toolBarFrame = toolBar.frame;
        toolBarFrame.origin.y = keyboardFrameInView.origin.y - toolBarFrame.size.height;
        toolBar.frame = toolBarFrame;
        
    }];

}

#pragma mark - LOCATION

-(void)locationCheck
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    [locationManager stopUpdatingLocation];
    CLLocation *location = [locations lastObject];
    
    CLGeocoder * geocoder = [[CLGeocoder alloc] init];
    
    [geocoder reverseGeocodeLocation: location completionHandler:
     ^(NSArray *placemarks, NSError *error) {
         
         //Get nearby address
         CLPlacemark *placemark = [placemarks objectAtIndex:0];
         
         //String to hold address
         NSString* locatedAtcountry = placemark.country;
         NSString* locatedAtcity = placemark.locality;
         
         locationText = [NSString stringWithFormat:@"%@, %@",locatedAtcity,locatedAtcountry];
         
     }];
}


- (void)refreshComment:(id)sender
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser,
                             };
    
    
    NSString * tempurl = [NSString stringWithFormat:@"%@posts/%@/%@",SERVICE_URL,[NSString stringWithFormat:@"%d",currId],SERVICE_COMMENTS];
    
    if (commentType == 1) {
        tempurl = [NSString stringWithFormat:@"%@items/%@/%@",SERVICE_URL,[NSString stringWithFormat:@"%d",currId],SERVICE_COMMENTS];
    }
    
    
    

    [manager GET:tempurl  parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"%@",responseObject);
        

//        NSArray *results = [[[(NSDictionary *)responseObject valueForKey:@"post"]valueForKey:@"comments"] objectAtIndex:0];
        
        
    NSArray *results = [(NSDictionary *)responseObject valueForKey:@"comments"];
        
        if (results == (id)[NSNull null]) {
            
//            UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Information" message:@"This post has no comment" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [message show];

        }
        else
        {
            self.allTableData = results;
            
            // Reload Table View
            [self.commentTableView reloadData];
            
            // End Refreshing

        }
        
         [(UIRefreshControl *)sender endRefreshing];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [(UIRefreshControl *)sender endRefreshing];
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)postdata
{
    [self.view hideKeyboard];
    
    [[CSActivityViewController sharedObject] showActivityView];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSLog(@"%@ %@", [NSString stringWithFormat:@"%d",currId],textField.text);
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser,
                             @"comment[location]":locationText,
                             @"comment[content]": textField.text
                             };
    
    NSString * tempurl = [NSString stringWithFormat:@"%@posts/%@/%@",SERVICE_URL,[NSString stringWithFormat:@"%d",currId],SERVICE_COMMENTS];
    
    if (commentType == 1) {
        tempurl = [NSString stringWithFormat:@"%@items/%@/%@",SERVICE_URL,[NSString stringWithFormat:@"%d",currId],SERVICE_COMMENTS];
    }
    
    
    
    [manager POST:tempurl  parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [[CSActivityViewController sharedObject] hideActivityView];
        
        textField.text = @"";
        
        [self refreshComment:NULL];

        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[CSActivityViewController sharedObject] hideActivityView];
        
        NSLog(@"Error: %@", error);
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
    }];
    
}


#pragma mark -
#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return allTableData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 72
    ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSMutableArray* items =[allTableData objectAtIndex:indexPath.row];
    NSDictionary * item = [items valueForKey:@"comment"];
    
    NSString *cellIdentifier = @"CommentCell";
    
    CommentCell *cell =  (CommentCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[CommentCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"CommentCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    
    NSLog(@"%@",item);
    
    [cell initText];
    
    [cell.contentText setString:[item valueForKey:@"content"]];
    
    

    if ([item valueForKey:@"location"] == (id)[NSNull null] || [[item valueForKey:@"location"] isEqualToString:@""])
    {
         cell.timeText.text = [item valueForKey:@"created"];

    }
    else
    {
        cell.timeText.text = [NSString stringWithFormat:@"%@ from %@",[item valueForKey:@"created"],[item valueForKey:@"location"]] ;
    }
    
    if([item valueForKey:@"user_image_url"]==(id) [NSNull null])
        [cell.avatarImage setBackgroundImage:PLACEHOLDER_AVATAR forState:UIControlStateNormal];
    else
        [cell.avatarImage sd_setBackgroundImageWithURL:[NSURL URLWithString:[item valueForKey:@"user_image_url"]] forState:UIControlStateNormal placeholderImage:PLACEHOLDER_AVATAR options:SDWebImageRefreshCached];
    
    
    [cell.avatarImage addTarget:self action:@selector(profileButton:event:) forControlEvents:UIControlEventTouchUpInside];
    
    
    cell.avatarImage.layer.cornerRadius = 20.0f;
    cell.avatarImage.clipsToBounds = YES;
    cell.avatarImage.layer.borderColor=[[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f] CGColor];
    cell.avatarImage.layer.borderWidth= 1.2f;
    
    
    cell.usernameText.text = [NSString stringWithFormat:@"%@",[item valueForKey:@"username"]];
    
    cell.commentContainer.layer.cornerRadius = 3.f;
    cell.commentContainer.clipsToBounds = YES;
    
    
    
    return cell;
}

- (void)profileButton:(id)sender event:(id)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.commentTableView];
    NSIndexPath *indexPath = [self.commentTableView indexPathForRowAtPoint:touchPoint];
    
    NSArray  * items =[self.allTableData objectAtIndex:indexPath.section];
    NSDictionary * item = [items valueForKey:@"comment"];
    

    NSString * temp = [item valueForKey:@"user_id"];
    [[NSUserDefaults standardUserDefaults] setValue:temp forKey:@"profileID"];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"profiletimelineID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.viewDeckController.centerController = SharedAppDelegate.profileController;
    
}

#pragma mark -
#pragma mark Add Navigation Item

- (void)addNavigationItem
{
    UIImage *leftImage = [UIImage imageNamed:@"IconBackToTimeline"];
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton addTarget:self action:@selector(backToTimeline) forControlEvents:UIControlEventTouchUpInside];
    leftButton.bounds = CGRectMake( 0, 0, leftImage.size.width, leftImage.size.height );
    [leftButton setImage:leftImage forState:UIControlStateNormal];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    self.navigationItem.leftBarButtonItems = @[leftItem];

    CGRect frame = CGRectMake(0, 0, 0, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame] ;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:16.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"Comments";
    self.navigationItem.titleView = label;

}

#pragma mark -
#pragma mark Button Function

- (void)backToTimeline
{
    [self.backgroundImage setHidden:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
