//
//  NewsListViewController.m
//  STYYLI
//
//  Created by Agil Febrianistian on 7/6/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import "NewsListViewController.h"
#import "UIImageView+WebCache.h"


@implementation NewsListViewController
@synthesize newsListTable;
@synthesize currId,allTableData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addNavigationItem];
    
    // Initialize Refresh Control
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    
    // Configure Refresh Control
    [refreshControl addTarget:self action:@selector(refreshComment:) forControlEvents:UIControlEventValueChanged];
    
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = self.newsListTable;
    
    tableViewController.refreshControl = refreshControl;
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self refreshComment:NULL];
    
}

#pragma mark -
#pragma mark Add Navigation Item

- (void)addNavigationItem
{
    UIImage *leftImage = [UIImage imageNamed:@"IconBackToTimeline"];
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton addTarget:self action:@selector(backToTimeline) forControlEvents:UIControlEventTouchUpInside];
    leftButton.bounds = CGRectMake( 0, 0, leftImage.size.width, leftImage.size.height );
    [leftButton setImage:leftImage forState:UIControlStateNormal];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    UIEdgeInsets buttonEdges = UIEdgeInsetsMake(0, LOGO_INSET_ARROW, 0, -LOGO_INSET_ARROW);
    UIImage *leftImagea = [UIImage imageNamed:@"signin-logo.png"];
    UIButton *leftButtona = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButtona.bounds = CGRectMake(0, 0, leftImagea.size.width/2, leftImagea.size.height/2);
    [leftButtona setImage:leftImagea forState:UIControlStateNormal];
    [leftButtona setImageEdgeInsets:buttonEdges];
    [leftButtona setUserInteractionEnabled:NO];
    UIBarButtonItem *leftItema = [[UIBarButtonItem alloc] initWithCustomView:leftButtona];
    
    self.navigationItem.leftBarButtonItems = @[leftItem,leftItema];
}

#pragma mark -
#pragma mark Button Function

- (void)backToTimeline
{
    NSArray *tempViewControllers = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    [self.navigationController popToViewController:[tempViewControllers objectAtIndex:[tempViewControllers count]-2] animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)refreshComment:(id)sender
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser
                             };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_NEWS] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"%@",responseObject);
        
       NSArray *results = [(NSDictionary *)responseObject objectForKey:@"newses"];
        
        if (results == (id)[NSNull null]) {
            
            
        }
        else
        {
            self.allTableData = results;
            
            // Reload Table View
            [self.newsListTable reloadData];
            
            // End Refreshing
            
        }
        
        [(UIRefreshControl *)sender endRefreshing];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [(UIRefreshControl *)sender endRefreshing];
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
    }];
    
}

#pragma mark -
#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return allTableData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 72
    ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray* items =[allTableData objectAtIndex:indexPath.row];
    NSDictionary * item = [items valueForKey:@"news"];
    
    NSString *cellIdentifier = @"HomeCell";
    
    HomeCell *cell =  (HomeCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[HomeCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    
    cell.newsText.text = [item valueForKey:@"description"];
    
    if([[[item valueForKey:@"image"] valueForKey:@"image"]valueForKey:@"url"] ==(id) [NSNull null])
        cell.newsImage.image  = PLACEHOLDER_IMAGE;
    else
        [cell.newsImage  sd_setImageWithURL:[NSURL URLWithString:[[[item valueForKey:@"image"] valueForKey:@"image"]valueForKey:@"url"]] placeholderImage:PLACEHOLDER_IMAGE options:SDWebImageRefreshCached];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    NSMutableArray  * items =[allTableData objectAtIndex:indexPath.row];
    
    
    NewsDetailsViewController *controller =  [self.storyboard instantiateViewControllerWithIdentifier:@"newsdetails"];
    controller.itemId = [[items valueForKey:@"news"] valueForKey:@"id"];
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
}



@end
