//
//  ChangePasswordViewController.m
//  STYYLI
//
//  Created by Agil Febrianistian on 8/17/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import "ChangePasswordViewController.h"

@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController
@synthesize activityContainer;
@synthesize emailNewTextfield,emailOldTextfield,emailReTextfield;


static  ChangePasswordViewController *sharedObject=nil;


+(ChangePasswordViewController *)sharedObject{
    @synchronized(self)
    {
        if (sharedObject == nil)
        {
            UIStoryboard *myStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            sharedObject = (ChangePasswordViewController *)[myStoryboard instantiateViewControllerWithIdentifier:@"changepassword"];
        }
    }
    return sharedObject;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor colorWithHue:0.0 saturation:0.0 brightness:0.0 alpha:0.5]];
    self.activityContainer.layer.cornerRadius = 10;
    
    self.view.layer.position =CGPointMake([[UIScreen mainScreen]bounds].size.width/2, [[UIScreen mainScreen]bounds].size.height/2);
    
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [emailOldTextfield becomeFirstResponder];
}

- (void)viewDidUnload
{
    [self setActivityContainer:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)showActivityView
{
    UIWindow *window = [[UIApplication sharedApplication].delegate window];
    [window addSubview:self.view];
    
    emailNewTextfield.text=@"";
    emailOldTextfield.text=@"";
    emailReTextfield.text=@"";
}

-(void)hideActivityView
{
    [self.view removeFromSuperview];
}

#pragma mark - Textfield Animation

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (!isUp) {
        [self animateTextField: textField up: YES];
        isUp = YES;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if (isUp == YES) {
        [self animateTextField: textField up: NO];
        isUp = NO;
    }
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 15; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
        [[CSActivityViewController sharedObject]showActivityView];
        [self SubmitData];
    }
    
    return NO;
}

- (IBAction)closeButton:(id)sender {
    
    [self hideActivityView];
}

- (IBAction)sendButton:(id)sender {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [[CSActivityViewController sharedObject]showActivityView];
    [self SubmitData];
    
}

-(void)SubmitData{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser,
                             @"old_password" : emailOldTextfield.text,
                             @"new_password" : emailNewTextfield.text,
                             @"confirm_password" : emailReTextfield.text
                             };
    
    [manager POST:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_CHANGE_PASSWORD]  parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"%@",responseObject);
        
        [[CSActivityViewController sharedObject]hideActivityView];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if ([[responseObject valueForKey:@"success"]intValue] == 1) {
            UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Success" message:[responseObject valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [message show];
            
            [self hideActivityView];
        }
        else
        {
            UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:[responseObject valueForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [message show];
            
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        NSLog(@"Error: %@", error);
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
    }];
}

@end