//
//  ProfileSettingViewController.m
//  STYYLI
//
//  Created by Agil Febrianistian on 5/18/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import "ProfileSettingViewController.h"
#import "UIButton+AFNetworking.h"
#import "UIButton+WebCache.h"

@interface ProfileSettingViewController ()

@end

@implementation ProfileSettingViewController
@synthesize profileImage,infoTable;
@synthesize bioField,locationField,websiteField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    profileImage.layer.cornerRadius = 50.0f;
    profileImage.layer.masksToBounds=YES;
    profileImage.layer.borderColor=[[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f] CGColor];
    profileImage.layer.borderWidth= 2.5f;

    [self addNavigationItem];
    
     self.infoTable.tableHeaderView = self.headerView;
    
    
    if([UserInfo sharedObject].avatarUser==(id) [NSNull null])
        [profileImage setBackgroundImage:PLACEHOLDER_AVATAR forState:UIControlStateNormal] ;
    else
        [profileImage sd_setBackgroundImageWithURL:[NSURL URLWithString:[UserInfo sharedObject].avatarUser] forState:UIControlStateNormal placeholderImage:PLACEHOLDER_AVATAR options:SDWebImageRefreshCached];
    
    
    bioField.text = self.bioText;
    locationField.text = self.locationText;
    websiteField.text = self.websiteText;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)addNavigationItem
{
    UIImage *leftImage = [UIImage imageNamed:@"IconBackToTimeline"];
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton addTarget:self action:@selector(showLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    leftButton.bounds = CGRectMake( 0, 0, leftImage.size.width, leftImage.size.height );
    [leftButton setImage:leftImage forState:UIControlStateNormal];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    self.navigationItem.leftBarButtonItem = leftItem ;
    
    
    CGRect frame = CGRectMake(0, 0, 0, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame] ;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:16.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"About Me";
    self.navigationItem.titleView = label;
    
    
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(showRighttMenu)];
    searchButton.tintColor = [UIColor whiteColor];
    
    [self.navigationItem setRightBarButtonItem:searchButton animated:YES];
}

- (void)showLeftMenu
{
    NSArray *tempViewControllers = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    [self.navigationController popToViewController:[tempViewControllers objectAtIndex:[tempViewControllers count]-2] animated:YES];
}

- (void)showRighttMenu
{
    [self dismissKeyboard];
    
    [[CSActivityViewController sharedObject]showActivityView];
    
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self sendData];
    });
    
}

-(void)sendData
{

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser,
                             @"user[description]": bioField.text,
                             @"user[city]": locationField.text,
                             @"user[website]": websiteField.text
                             };
    
    [manager POST:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_PROFILE_SETTING] parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         if (imageData != NULL) {
             [formData appendPartWithFileData:imageData name:@"user[image]" fileName:@"photoname.jpg" mimeType:@"image/jpeg"];
         }
     }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSLog(@"%@",responseObject);
              
              [[CSActivityViewController sharedObject]hideActivityView];
              
              //        "image_small" = "http://s3.amazonaws.com/styylistaging2/uploads/avatars/user/image/15/small_photoname.jpg";
              
              NSArray *tempViewControllers = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
              [self.navigationController popToViewController:[tempViewControllers objectAtIndex:[tempViewControllers count]-2] animated:YES];
              
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              NSLog(@"Error: %@", error);
              [[CSActivityViewController sharedObject]hideActivityView];
              UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
              [message show];
              
          }];

}


#pragma mark - Keyboard delegate

- (IBAction)bgTapped:(id)sender {
    [self dismissKeyboard];
}

- (IBAction)imageChange:(id)sender {
    
    [self dismissKeyboard];
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Upload Image"
                              message:@""
                              delegate:self
                              cancelButtonTitle:@"Cancel"
                              otherButtonTitles:  @"Camera",@"Gallery", nil];
    
    alertView.transform = CGAffineTransformTranslate( alertView.transform, 0.0, 100.0 );
    
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqual:[NSString stringWithFormat:@"Camera"]])
    {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = self;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    else if([title isEqualToString:@"Gallery"])
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.delegate = self;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info; {
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    [profileImage setBackgroundImage:image forState:UIControlStateNormal];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        [self openEditor:nil];
        
    }];
    
}


- (IBAction)openEditor:(id)sender
{
    
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    [controller setCropAspectRatio:1];
    [controller setKeepingCropAspectRatio:true];
    
    controller.image = [profileImage backgroundImageForState:UIControlStateNormal];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:navigationController animated:YES completion:NULL];
    
}

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    
    
    [profileImage setBackgroundImage:croppedImage forState:UIControlStateNormal];
    
    NSString  *pngPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@.jpg",@"photoname"]];
    
    [UIImageJPEGRepresentation(croppedImage, 1) writeToFile:pngPath atomically:NO];
    
    imageName = pngPath;
    
    imageData = UIImageJPEGRepresentation([profileImage backgroundImageForState:UIControlStateNormal], 1);
    
}


- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}



-(void)dismissKeyboard {
    if ([bioField isFirstResponder])
        [bioField resignFirstResponder];
    else if ([locationField isFirstResponder])
        [locationField resignFirstResponder];
    else if ([websiteField isFirstResponder])
        [websiteField resignFirstResponder];

}


#pragma mark -
#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    
    switch (indexPath.row) {
        case 0:
        {
            cell = self.bioCell;
        }
            break;
        case 1:
        {
            cell = self.locationCell;
        }
            break;
        case 2:
        {
            cell = self.websiteCell;
        }
            break;
            
        default:
            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
}

#pragma mark - Textfield Animation

- (void)textFieldDidBeginEditing:(UITextField *)textField
{

    
        if (!isUp) {
            [self animateTextField: textField up: YES];
            isUp = YES;
        }
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
        textField.layer.borderColor=[[UIColor clearColor]CGColor];
    
        if (isUp == YES) {
            [self animateTextField: textField up: NO];
            isUp = NO;
        }
    
}



- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 80; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    
    return NO;
}

@end
