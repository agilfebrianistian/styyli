//
//  HomeViewController.m
//  STYYLI
//
//  Created by Agil Febrianistian on 6/14/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import "HomeViewController.h"
#import "IIViewDeckController.h"
#import "RHZAppDelegate.h"

#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"

@interface HomeViewController ()

@end

@implementation HomeViewController
@synthesize allTableData,homeTable,tableArray,pageArray,carouselView,itemsArray,itemsButton,scroller;
@synthesize pageControlMenu;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [scroller setScrollEnabled:true];
    [scroller setContentSize:CGSizeMake(320,568)];
    
    [self.view bringSubviewToFront:self.pageControlMenu];
    
    [self addNavigationItem];
    
    sliderCounter = 0;
    
    
    NSTimer *timer;
    timer = [NSTimer scheduledTimerWithTimeInterval: 5
                                             target: self
                                           selector: @selector(handleTimer)
                                           userInfo: nil
                                            repeats: YES];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[CSActivityViewController sharedObject]showActivityView];
    
    [self getData];
    
    [self getNotification];
    
}

#pragma mark -
#pragma mark get notification

- (void)getNotification
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser,
                             @"id":[UserInfo sharedObject].idUser
                             };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_NOTIFICATIONS_UNREAD] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([[responseObject valueForKey:@"counter"]intValue] > 0) {
            UIImage *rightImage1 = [UIImage imageNamed:@"IconItemRightNotification"];
            UIButton *rightButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
            [rightButton1 addTarget:self action:@selector(showNotificationPage) forControlEvents:UIControlEventTouchUpInside];
            rightButton1.bounds = CGRectMake( 0, 0, rightImage1.size.width + 20, rightImage1.size.height );
            [rightButton1 setTitle:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"counter"]] forState:UIControlStateNormal];
            rightButton1.titleLabel.font = [UIFont systemFontOfSize:11.f];
            [rightButton1 setImage:rightImage1 forState:UIControlStateNormal];
            UIBarButtonItem *rightNotificationItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton1];
            self.navigationItem.rightBarButtonItem = rightNotificationItem;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [[CSActivityViewController sharedObject]hideActivityView];
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}


- (void)handleTimer
{
    
    
    while (sliderCounter < 3) {
        
         [carouselView  scrollToItemAtIndex:sliderCounter animated:YES];
        
        if (sliderCounter == 2)
            sliderCounter = 0;
        else
            sliderCounter++;
        break;
        
    }

}

- (IBAction)updatePage:(UIPageControl *)pageControl
{

    [pageControlMenu setNumberOfPages:self.pageArray.count];
    [carouselView  scrollToItemAtIndex:pageControl.currentPage animated:YES];
}

#pragma mark -
#pragma mark Add Navigation Item


- (void)addNavigationItem
{
    UIImage *leftImage = [UIImage imageNamed:@"icon_menu"];
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton addTarget:self action:@selector(showLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    leftButton.bounds = CGRectMake( 0, 0, leftImage.size.width, leftImage.size.height );
    [leftButton setImage:leftImage forState:UIControlStateNormal];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    UIImage *rightImage1 = [UIImage imageNamed:@"IconItemRightNotification"];
    UIButton *rightButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton1 addTarget:self action:@selector(showNotificationPage) forControlEvents:UIControlEventTouchUpInside];
    rightButton1.bounds = CGRectMake( 0, 0, rightImage1.size.width, rightImage1.size.height );
    [rightButton1 setImage:rightImage1 forState:UIControlStateNormal];
    UIBarButtonItem *rightNotificationItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton1];
    
    UIEdgeInsets buttonEdges = UIEdgeInsetsMake(0, LOGO_INSET_STRIP, 0, -LOGO_INSET_STRIP);
    UIImage *leftImagea = [UIImage imageNamed:@"signin-logo.png"];
    UIButton *leftButtona = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButtona.bounds = CGRectMake(0, 0, leftImagea.size.width/2, leftImagea.size.height/2);
    [leftButtona setImage:leftImagea forState:UIControlStateNormal];
    [leftButtona setImageEdgeInsets:buttonEdges];
    [leftButtona setUserInteractionEnabled:NO];
    UIBarButtonItem *leftItema = [[UIBarButtonItem alloc] initWithCustomView:leftButtona];
    
    self.navigationItem.leftBarButtonItems = @[leftItem,leftItema];
    self.navigationItem.rightBarButtonItem = rightNotificationItem;
}

#pragma mark -
#pragma mark Button Function

- (void)showLeftMenu
{
    [self.viewDeckController performSelectorOnMainThread:@selector(toggleLeftView) withObject:nil waitUntilDone:YES];
}

- (void)showNotificationPage
{
    UIViewController *filtersViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"notificationlist"];
    [self.navigationController pushViewController:filtersViewController animated:YES];
}


#pragma mark -
#pragma mark get data from server

- (void)getData
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser
                             };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_NEWS]   parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        [[CSActivityViewController sharedObject]hideActivityView];
        
        
        NSLog(@"%@",responseObject);
        
        
       NSArray *results = [(NSDictionary *)responseObject objectForKey:@"newses"];
        
        if (results == (id)[NSNull null]) {
            
            UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [message show];
            
        }
        else if (results.count>0)
        {
            self.allTableData = results;
            
            
            pageArray = [allTableData subarrayWithRange:NSMakeRange(0, 3)];

            tableArray = [allTableData subarrayWithRange:NSMakeRange(3, 3)];
            
            itemsArray = [allTableData subarrayWithRange:NSMakeRange(0, 1)];
            
            [self initNewItems];
            [self initItems];
            
            [self updatePage:pageControlMenu];
            
            [carouselView reloadData];
            
            [self.homeTable reloadData];
        }
        else
        {
            
        }

        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [[CSActivityViewController sharedObject]hideActivityView];
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}

#pragma mark -
#pragma mark new items

-(void)initNewItems
{
    
       NSArray * item = [[self.pageArray objectAtIndex:0] valueForKey:@"news"];
    
    if([[[item valueForKey:@"image"] valueForKey:@"image"]valueForKey:@"url"]==(id) [NSNull null])
        [itemsButton setBackgroundImage:PLACEHOLDER_IMAGE forState:UIControlStateNormal] ;
    else
    [itemsButton sd_setBackgroundImageWithURL:[NSURL URLWithString:[[[item valueForKey:@"image"] valueForKey:@"image"]valueForKey:@"url"]] forState:UIControlStateNormal placeholderImage:PLACEHOLDER_IMAGE options:SDWebImageRefreshCached];
    

    
}


//placeholder_image

#pragma mark -
#pragma mark iCarousel

-(void)initItems
{
    carouselView.delegate = self;
    carouselView.dataSource = self;
    carouselView.type=iCarouselTypeLinear;
    
}




-(NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
      return self.pageArray.count;
}

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel
{
    
    pageControlMenu.currentPage = carousel.currentItemIndex ;
}

- (UIView *) carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view{
    
    UIImageView * images = Nil;
    UIImageView * bgTransparent = Nil;
    UILabel * newsText;
    UILabel * newslabelText;
    
        if (view == nil)
        {
            view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320.0f, 185.0f)];
            view.contentMode = UIViewContentModeCenter;
            
            images = [[UIImageView alloc]initWithFrame:view.bounds];
            images.tag = 1;
            [images setContentMode:UIViewContentModeScaleAspectFill];
            
            [view addSubview:images];
            
            bgTransparent = [[UIImageView alloc]initWithFrame:view.bounds];
            bgTransparent.tag = 2;
            bgTransparent.backgroundColor=[UIColor blackColor];
            bgTransparent.alpha = 0.25f;
            [bgTransparent setContentMode:UIViewContentModeScaleAspectFit];
            
            [view addSubview:bgTransparent];
            
            newslabelText = [[UILabel alloc]initWithFrame:CGRectMake(15, 15, 300.0f, 20.0f)];
            newslabelText.tag = 3;
            newslabelText.adjustsFontSizeToFitWidth = true;
            newslabelText.adjustsLetterSpacingToFitWidth = true;
            newslabelText.textColor = [UIColor whiteColor];
            newslabelText.numberOfLines = 3;
            newslabelText.font = [UIFont fontWithName:@"Helvetica Light" size:14.0f];
            
            newslabelText.text = @"NEWS";
            
            [view addSubview:newslabelText];
            
            
            newsText = [[UILabel alloc]initWithFrame:CGRectMake(15, 33, 300.0f, 40.0f)];
            newsText.tag = 4;
            //newsText.adjustsFontSizeToFitWidth = true;
            newsText.adjustsLetterSpacingToFitWidth = true;
            newsText.textColor = [UIColor whiteColor];
            newsText.numberOfLines = 3;
            newsText.font = [UIFont fontWithName:@"Helvetica Bold" size:17.0f];
            
            [view addSubview:newsText];
   
            
        }
        else
        {
            images = (UIImageView *)[view viewWithTag:1];
            bgTransparent = (UIImageView *)[view viewWithTag:2];
            newslabelText = (UILabel *)[view viewWithTag:3];
            newsText = (UILabel *)[view viewWithTag:4];
        
        }
        
        
        NSArray * item = [[self.pageArray objectAtIndex:index] valueForKey:@"news"];
    
      newsText.text = [[item valueForKey:@"title"] uppercaseString];
    
    
        if([[[[item valueForKey:@"image"] valueForKey:@"image"] valueForKey:@"medium"] valueForKey:@"url"]==(id) [NSNull null])
            [images setImage:PLACEHOLDER_IMAGE];
        else
            [images sd_setImageWithURL:[NSURL URLWithString:[[[[item valueForKey:@"image"] valueForKey:@"image"] valueForKey:@"medium"] valueForKey:@"url"]] placeholderImage:PLACEHOLDER_IMAGE options:SDWebImageRefreshCached];

    
    
    return view;
    
    
}

- (BOOL)carousel:(iCarousel *)carousel shouldSelectItemAtIndex:(NSInteger)index{
    
    return YES;
}



#pragma mark -
#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return tableArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 72
    ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray* items =[tableArray objectAtIndex:indexPath.row];
    NSDictionary * item = [items valueForKey:@"news"];
    
    NSString *cellIdentifier = @"HomeCell";
    
    HomeCell *cell =  (HomeCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[HomeCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    
    cell.newsText.text = [item valueForKey:@"description"];
    
    if([[[[item valueForKey:@"image"] valueForKey:@"image"] valueForKey:@"thumb"] valueForKey:@"url"] ==(id) [NSNull null])
        cell.newsImage.image  = PLACEHOLDER_AVATAR;
    else
        [cell.newsImage sd_setImageWithURL:[NSURL URLWithString:[[[[item valueForKey:@"image"] valueForKey:@"image"] valueForKey:@"thumb"] valueForKey:@"url"]]  placeholderImage:PLACEHOLDER_AVATAR options:SDWebImageRefreshCached];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray  * items =[allTableData objectAtIndex:indexPath.row];
    
    
    NewsDetailsViewController *controller =  [self.storyboard instantiateViewControllerWithIdentifier:@"newsdetails"];
    controller.itemId = [[items valueForKey:@"news"] valueForKey:@"id"];
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    
    NSMutableArray  * items =[allTableData objectAtIndex:index];
    
    NewsDetailsViewController *controller =  [self.storyboard instantiateViewControllerWithIdentifier:@"newsdetails"];
    controller.itemId = [[items valueForKey:@"news"] valueForKey:@"id"];
    
    [self.navigationController pushViewController:controller animated:YES];
    
}
- (IBAction)moreNewsButton:(id)sender {
    UIViewController *controller =  [self.storyboard instantiateViewControllerWithIdentifier:@"newslist"];
    [self.navigationController pushViewController:controller animated:YES];
    
}

- (IBAction)feedButton:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"timelineID"];
    [self showTimelineViewController];
}

- (IBAction)POTDButton:(id)sender {
    
    POTDViewController *filtersViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"potd"];
    [self.navigationController pushViewController:filtersViewController animated:YES];
    
}

- (IBAction)newItemsButton:(id)sender {
    
    ItemsViewController *filtersViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"items"];
    [self.navigationController pushViewController:filtersViewController animated:YES];

}

- (void)showTimelineViewController
{
    [self.viewDeckController toggleTopViewAnimated:YES completion:^(IIViewDeckController *controller, BOOL success){
        self.viewDeckController.centerController = SharedAppDelegate.timelineController;
    }];
}
@end

