//
//  CollectionCell.h
//  STYYLI
//
//  Created by Agil Febrianistian on 7/17/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *itemImage;

@property (weak, nonatomic) IBOutlet UILabel *brandName;

@property (weak, nonatomic) IBOutlet UILabel *itemName;

@end
