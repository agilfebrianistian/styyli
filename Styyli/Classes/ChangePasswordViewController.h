//
//  ChangePasswordViewController.h
//  STYYLI
//
//  Created by Agil Febrianistian on 8/17/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "CSActivityViewController.h"
#import "Constant.h"
#import "UserInfo.h"
@interface ChangePasswordViewController : UIViewController
{
    bool isUp;
}


@property (retain, nonatomic) IBOutlet UIView *activityContainer;
+(ChangePasswordViewController *)sharedObject;
-(void)showActivityView;
-(void)hideActivityView;

@property (weak, nonatomic) IBOutlet UITextField *emailOldTextfield;
@property (weak, nonatomic) IBOutlet UITextField *emailNewTextfield;
@property (weak, nonatomic) IBOutlet UITextField *emailReTextfield;


- (IBAction)closeButton:(id)sender;
- (IBAction)sendButton:(id)sender;
@end
