//
//  BrandTableViewController.h
//  STYYLI
//
//  Created by Agil Febrianistian on 4/19/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "ImagePostViewController.h"
#import "CSActivityViewController.h"

@interface BrandTableViewController : UITableViewController
{
    BOOL isexpanded;
    
}

@property(nonatomic,strong) NSArray *items;
@property (nonatomic, retain) NSMutableArray *topCategory;

@property (strong, nonatomic) IBOutlet UITableView *menuTableView;

@end
