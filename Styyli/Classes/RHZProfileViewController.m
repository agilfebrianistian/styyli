//
//  RHZProfileViewController.m
//  Styyli
//
//  Created by R. Hafidhullah Zakariyya on 12/23/13.
//  Copyright (c) 2013 Mirosea. All rights reserved.
//

#import "RHZProfileViewController.h"
#import "IIViewDeckController.h"

#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"

@interface RHZProfileViewController ()

@end

@implementation RHZProfileViewController
@synthesize profilePicture,profileName,profileStatus,profieCity,profileSite;
@synthesize profileButton;
@synthesize profileFollowing,profileFollower;
@synthesize profileStylesCount,profileWardrobeCount,profileFavoriteCount;
@synthesize userData,allTableData,myTableView,selectedIndexPaths;
@synthesize headerView,profileType;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    
    
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = self.myTableView;
    
    tableViewController.refreshControl = refreshControl;
    
    self.myTableView.tableHeaderView = headerView;
    
    allTableData = [[NSMutableArray alloc]init];
    
    currentPage = 0;
    
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self addNavigationItem];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUpdatedData:)
                                                 name:@"DataUpdated"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUpdatedDataItem:)
                                                 name:@"DataItemUpdated"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hashtagRefresh:)
                                                 name:@"hashtagData"
                                               object:nil];
    
    
    self.idProfile = [[NSUserDefaults standardUserDefaults] stringForKey:@"profileID"];
    
    
        if ([self.idProfile intValue] == [[UserInfo sharedObject].idUser intValue]) {
    
            self.idProfile = @"0";
        }
    
    [[CSActivityViewController sharedObject]showActivityView];
    
    [self getNotification];
    
    
    
    [self refresh:NULL];
    
    [self.myTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    
}

#pragma mark -
#pragma mark get notification

- (void)getNotification
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser,
                             @"id":[UserInfo sharedObject].idUser
                             };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_NOTIFICATIONS_UNREAD] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"%@",responseObject);
        
        if ([[responseObject valueForKey:@"counter"]intValue] > 0) {
            UIImage *rightImage1 = [UIImage imageNamed:@"IconItemRightNotification"];
            UIButton *rightButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
            [rightButton1 addTarget:self action:@selector(showNotificationPage) forControlEvents:UIControlEventTouchUpInside];
            rightButton1.bounds = CGRectMake( 0, 0, rightImage1.size.width + 20, rightImage1.size.height );
            [rightButton1 setTitle:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"counter"]] forState:UIControlStateNormal];
            rightButton1.titleLabel.font = [UIFont systemFontOfSize:11.f];
            [rightButton1 setImage:rightImage1 forState:UIControlStateNormal];
            UIBarButtonItem *rightNotificationItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton1];
            self.navigationItem.rightBarButtonItem = rightNotificationItem;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [[CSActivityViewController sharedObject]hideActivityView];
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"DataUpdated"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"DataItemUpdated"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"hashtagData"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"timelineItem"
                                                  object:nil];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Add Navigation Item

- (void)addNavigationItem
{
    isTimeline = [[NSUserDefaults standardUserDefaults] boolForKey:@"isTimeline"];
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if (isTimeline ) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleTimeline:)
                                                     name:@"timelineItem"
                                                   object:nil];
        
        UIImage *leftImage = [UIImage imageNamed:@"IconBackToTimeline"];
        [leftButton addTarget:self action:@selector(showbackMenu) forControlEvents:UIControlEventTouchUpInside];
        leftButton.bounds = CGRectMake( 0, 0, leftImage.size.width, leftImage.size.height );
        [leftButton setImage:leftImage forState:UIControlStateNormal];
        
    }
    else
    {
        UIImage *leftImage = [UIImage imageNamed:@"icon_menu"];
        [leftButton addTarget:self action:@selector(showLeftMenu) forControlEvents:UIControlEventTouchUpInside];
        leftButton.bounds = CGRectMake( 0, 0, leftImage.size.width, leftImage.size.height );
        [leftButton setImage:leftImage forState:UIControlStateNormal];
        
    }
    
    
    if (self.navigationController.viewControllers.count == 1)
    {
        UIImage *leftImage = [UIImage imageNamed:@"icon_menu"];
        [leftButton addTarget:self action:@selector(showLeftMenu) forControlEvents:UIControlEventTouchUpInside];
        leftButton.bounds = CGRectMake( 0, 0, leftImage.size.width, leftImage.size.height );
        [leftButton setImage:leftImage forState:UIControlStateNormal];
        
    }
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    
    UIImage *rightImage1 = [UIImage imageNamed:@"IconItemRightNotification"];
    UIButton *rightButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton1 addTarget:self action:@selector(showNotificationPage) forControlEvents:UIControlEventTouchUpInside];
    rightButton1.bounds = CGRectMake( 0, 0, rightImage1.size.width, rightImage1.size.height );
    [rightButton1 setImage:rightImage1 forState:UIControlStateNormal];
    UIBarButtonItem *rightNotificationItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton1];
    
    self.navigationItem.rightBarButtonItem = rightNotificationItem;
    
    self.navigationItem.leftBarButtonItem = leftItem;
    
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isTimeline"];
}

#pragma mark -
#pragma mark Button Function

- (void)showLeftMenu
{
    [self.viewDeckController performSelectorOnMainThread:@selector(toggleLeftView) withObject:nil waitUntilDone:YES];
}

- (void)showbackMenu
{
    
    NSMutableArray * tempData = [[[NSUserDefaults standardUserDefaults] objectForKey:@"currentID"] mutableCopy];
    
    if (!tempData.count > 0) {
        tempData = [[NSMutableArray alloc]init];
    }
    
    NSArray *tempViewControllers = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    
    if (numberOfViewControllers < 2)
        [self.navigationController popToRootViewControllerAnimated:YES];
    else
    {
        NSMutableArray * tempData = [[[NSUserDefaults standardUserDefaults] objectForKey:@"currentID"] mutableCopy];
        
        NSString * tempID = [tempData lastObject];
        
        [tempData removeLastObject];
        
        [[NSUserDefaults standardUserDefaults] setObject:tempData forKey:@"currentID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        NSLog(@"%@",tempData);
        
        [[NSUserDefaults standardUserDefaults] setValue:tempID forKey:@"profileID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.navigationController popToViewController:[tempViewControllers objectAtIndex:[tempViewControllers count]-2] animated:YES];
    }
    
}

- (void)showNotificationPage
{
    UIViewController *filtersViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"notificationlist"];
    [self.navigationController pushViewController:filtersViewController animated:YES];
}



#pragma mark -
#pragma mark get data from server

- (void)getData
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser
                             };
    
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@users/%@/%@",SERVICE_URL,[[NSUserDefaults standardUserDefaults] stringForKey:@"profileID"],SERVICE_PROFILE]);
    
    
    [manager GET:[NSString stringWithFormat:@"%@users/%@/%@",SERVICE_URL,[[NSUserDefaults standardUserDefaults] stringForKey:@"profileID"],SERVICE_PROFILE]
      parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
          
          
          [[CSActivityViewController sharedObject]hideActivityView];
          
          profileFollower.text = [NSString stringWithFormat:@"%@ followers",[responseObject valueForKey:@"followers"]];
          profileFollowing.text = [NSString stringWithFormat:@"%@ following",[responseObject valueForKey:@"followings"]];
          
          profileFavoriteCount.text = [NSString stringWithFormat:@"%@",[responseObject valueForKey:@"favorites"]];
          profileStylesCount.text = [NSString stringWithFormat:@"%@",[responseObject valueForKey:@"posts_count"]];
          profileWardrobeCount.text = [NSString stringWithFormat:@"%@",[responseObject valueForKey:@"items"]];
          
          NSDictionary * userItem =[responseObject valueForKey:@"user"];
          
          if ([userItem valueForKey:@"city"] == (id)[NSNull null])
              profieCity.text = @"";
          else
              profieCity.text = [NSString stringWithFormat:@"%@",[userItem valueForKey:@"city"]];
          
          if ([userItem valueForKey:@"website"]  == (id)[NSNull null])
              profileSite.text = @"";
          else
              profileSite.text = [NSString stringWithFormat:@"%@",[userItem valueForKey:@"website"]];
          
          if ([userItem valueForKey:@"description"]  == (id)[NSNull null])
              profileStatus.text = @"";
          else
              profileStatus.text = [NSString stringWithFormat:@"%@",[userItem valueForKey:@"description"]];
          
          
          CGRect frame = CGRectMake(0, 0, 0, 44);
          UILabel *label = [[UILabel alloc] initWithFrame:frame] ;
          label.backgroundColor = [UIColor clearColor];
          label.font = [UIFont boldSystemFontOfSize:16.0];
          label.textAlignment = NSTextAlignmentCenter;
          label.textColor = [UIColor whiteColor];
          label.text = [userItem valueForKey:@"username"];
          self.navigationItem.titleView = label;
          
          profileName.text = [userItem valueForKey:@"fullname"];
          
          if([userItem valueForKey:@"image_small"]==(id) [NSNull null])
              self.profilePicture.image  = PLACEHOLDER_AVATAR;
          else
              [self.profilePicture sd_setImageWithURL:[NSURL URLWithString:[userItem valueForKey:@"image_small"]]  placeholderImage:PLACEHOLDER_AVATAR options:SDWebImageRefreshCached];

          self.profilePicture.layer.cornerRadius = 50.0f;
          self.profilePicture.layer.masksToBounds=YES;
          self.profilePicture.layer.borderColor=[[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f] CGColor];
          self.profilePicture.layer.borderWidth= 2.5f;
          
          
          switch ([[responseObject valueForKey:@"followed_by_me"] integerValue]) {
              case 0:
              {
                  [profileButton setImage:[UIImage imageNamed:@"icon_edit"] forState:UIControlStateNormal];
              }
                  break;
              case 1:
              {
                  [profileButton setImage:[UIImage imageNamed:@"icon_sudah_difollow"] forState:UIControlStateNormal];
              }
                  break;
              case 2:
              {
                  [profileButton setImage:[UIImage imageNamed:@"icon_belum_difollow"] forState:UIControlStateNormal];
              }
                  break;
                  
              default:
                  break;
          }
          
          profileButton.tag = [[responseObject valueForKey:@"followed_by_me"] integerValue];
          [profileButton addTarget:self action:@selector(editButton) forControlEvents:UIControlEventTouchUpInside];
          
      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          
          NSLog(@"Error: %@", error);
          [[CSActivityViewController sharedObject]hideActivityView];
          UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
          [message show];
          
      }];
}


-(void)editButton
{
    
    if (profileButton.tag == 0) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ProfileSettingViewController *filtersViewController = [storyboard instantiateViewControllerWithIdentifier:@"profilesetting"];
        
        filtersViewController.idProfile     = [[NSUserDefaults standardUserDefaults] stringForKey:@"profileID"];
        filtersViewController.bioText       = profileStatus.text;
        filtersViewController.locationText  = profieCity.text;
        filtersViewController.websiteText   = profileSite.text;
        
        
        [self.navigationController pushViewController:filtersViewController animated:YES];
    }
    else
    {
        [self followBtn];
    }
    
}

-(void)followBtn
{
    
    NSDictionary * params = @{@"api_key":API_KEY,
                              @"auth_token":[UserInfo sharedObject].tokenUser                              };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager GET:[NSString stringWithFormat:@"%@users/%@/%@",SERVICE_URL,self.idProfile,SERVICE_FOLLOW] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        
        [self getData];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
    
}

- (void)refresh:(id)sender
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    profileType = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"profiletimelineID"];
    
    NSDictionary *params = @{@"api_key":API_KEY,
               @"auth_token":[UserInfo sharedObject].tokenUser,
               @"search":@"",
               @"id": self.idProfile,
               @"page":[NSString stringWithFormat:@"%d",currentPage]
               };
    
    
    NSString *tempUrl;
    
    if (profileType== 0)
    {
        tempUrl = [NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_TIMELINE];
        
        
    }
    else
    {
        tempUrl = [NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_PROFILE_FAVORITE];
        
        params = @{@"api_key":API_KEY,
                   @"auth_token":[UserInfo sharedObject].tokenUser
                   };
        
    }

    
    
    [manager GET:tempUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
    
        allTableData = [[NSMutableArray alloc]init];
        NSMutableArray* temp = [[(NSMutableArray*)responseObject valueForKey:@"posts"] mutableCopy];
        
        totalPage = [[responseObject objectForKey:@"total_page"] intValue];

        
        for (NSMutableArray*items in temp) {
            NSMutableArray* tempitem = [items mutableCopy];
            [allTableData addObject:tempitem];
        }
        
    
        [self.myTableView reloadData];
        [(UIRefreshControl *)sender endRefreshing];
    
        
        [self getData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [(UIRefreshControl *)sender endRefreshing];
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //return [allTableData count] ? [allTableData count] : 0;
    
    if (currentPage == 0) {
        return [allTableData count] ? [allTableData count] : 0;
    }
    
    return self.allTableData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSArray  * items =[self.allTableData objectAtIndex:section];
    NSDictionary * item = [items valueForKey:@"post"];
    NSString *cellIdentifier = @"TimelineHeaderCell";
    TimelineHeaderCell *header =  (TimelineHeaderCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    NSURL *urlImage = [item valueForKey:@"user_avatar"];
    [header.timelineAvatar sd_setBackgroundImageWithURL:urlImage forState:UIControlStateNormal placeholderImage:PLACEHOLDER_AVATAR options:SDWebImageRefreshCached];
    
    [header.timelineAvatar addTarget:self action:@selector(profileButton:event:) forControlEvents:UIControlEventTouchUpInside];
    header.timelineAvatar.layer.cornerRadius = 20.0f;
    header.timelineAvatar.clipsToBounds = YES;
    header.timelineAvatar.layer.borderColor=[[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f] CGColor];
    header.timelineAvatar.layer.borderWidth= 1.2f;
    
    header.dateView.layer.cornerRadius = 5;
    
    header.timelineFullName.text    = [item valueForKey:@"fullname"];
    header.timelineUsername.text    = [item valueForKey:@"username"];
    header.timelineTime.text        = [item valueForKey:@"created_at"];
    header.timelinePostdate.text    = [item valueForKey:@"created"];
    
    
    return header;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray* items =[self.allTableData objectAtIndex:indexPath.section];
    NSDictionary * item = [items valueForKey:@"post"];
    
    if (![self.selectedIndexPaths containsObject:indexPath]) {
        if ([[item valueForKey:@"caption"] isEqualToString:@""])
        {
            return 420;
        }
        else
        {
            return 460;
        }
        
    }
    else
    {
        if ([[item valueForKey:@"caption"] isEqualToString:@""])
        {
            return 460;
        }
        else
        {
            return 500;
        }
    }
    
}

- (UITableViewCell *)timelineCellForIndexPath:(NSIndexPath *)indexPath {
    
    NSArray* items =[self.allTableData objectAtIndex:indexPath.section];
    NSDictionary * item = [items valueForKey:@"post"];
    NSString *cellIdentifier = @"TimelineCell";
    
    TimelineCell *cell =  (TimelineCell *)[myTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[TimelineCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"TimelineCell"];
        
    }
    
    cell.clipsToBounds = true;
    
    [cell.commentButton addTarget:self action:@selector(showCommentView:event:) forControlEvents:UIControlEventTouchUpInside];
    [cell.commentButton setTitle:[NSString stringWithFormat:@" %@",[item valueForKey:@"comments_count"]] forState:UIControlStateNormal];
    
    [cell initText];
    
    [cell.captionAtributed setString:[item valueForKey:@"caption"]];
    
    [cell.likeButton setTitle:[NSString stringWithFormat:@" %@",[item valueForKey:@"likes_count"] ] forState:UIControlStateNormal];
    
    if ([[item valueForKey:@"liked_by_me"]integerValue] > 0)
        [cell.likeButton setImage:[UIImage imageNamed:@"nglike"] forState:UIControlStateNormal];
    else
        [cell.likeButton setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
    
    
    
    NSURL *imageUrl = [item valueForKey:@"image"];
    
    __block UIActivityIndicatorView *activityIndicator;
    __weak UIImageView *weakImageView = cell.mainPicture;
    
    [cell.mainPicture sd_setImageWithURL:imageUrl
                        placeholderImage:PLACEHOLDER_IMAGE
                                 options:SDWebImageProgressiveDownload
                                progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                    if (!activityIndicator) {
                                        [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge]];
                                        activityIndicator.center = weakImageView.center;
                                        [activityIndicator startAnimating];
                                    }
                                }
                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                   [activityIndicator removeFromSuperview];
                                   activityIndicator = nil;
                               }];
    
    NSArray * tempTag = [item valueForKey:@"items"];
    
    NSMutableArray * tags = [[NSMutableArray alloc]init];
    
    for (NSArray * tagItem in tempTag) {
        
        NSDictionary * dictTag = [tagItem valueForKey:@"item"];
        
        float tempX = 0.0f;
        float tempY = 0.0f;
        
        if (![[dictTag valueForKey:@"x"] isEqual:[NSNull null]] | ![[dictTag valueForKey:@"y"] isEqual:[NSNull null]]) {
            tempX = [[dictTag valueForKey:@"x"]floatValue];
            tempY = [[dictTag valueForKey:@"y"]floatValue];
        }
        
        [tags addObject: [DEMOTag tagWithProperties:@{@"tagPosition" : [NSValue valueWithCGPoint:CGPointMake(tempX,tempY)],
                                                      @"tagText" : [dictTag valueForKey:@"name"]}]];
    }
    
    [cell initTagPopoverFrom:tags];
    
    cell.itemsData = [item valueForKey:@"items"];
    [cell initItems];
    
    cell.loversData = [item valueForKey:@"likers"];
    [cell initLovers];
    
    if ([[item valueForKey:@"items"] count] > 0) {
        
        [cell.tagButton setHidden:FALSE];
    }else
    {
        [cell.tagButton setHidden:TRUE];
    }
    
    if ([[item valueForKey:@"likers"] count]>4)
        [cell.viewLoveButton setHidden:NO];
    else
        [cell.viewLoveButton setHidden:YES];
    
    [cell.viewLoveButton addTarget:self action:@selector(showAllLovers:event:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.tagButton addTarget:self action:@selector(showBrandView:event:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.likeButton addTarget:self action:@selector(showLikeView:event:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.moreButton addTarget:self action:@selector(showMoreButtons:event:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.pictButton addTarget:self action:@selector(showTagView:event:) forControlEvents:UIControlEventTouchUpInside];
    
    
    //image button tapped
    
    BOOL isSelectedimage = [self.selectedImagePaths containsObject:indexPath];
    
    if (!isSelectedimage) {
        for (UIView *subView in cell.mainPicture.subviews)
        {
            if (subView.tag == 99)
            {
                [subView setHidden:true];
            }
        }
    }
    else
    {
        [self.view bringSubviewToFront:cell.mainPicture];
        
        for (UIView *subView in cell.mainPicture.subviews)
        {
            if (subView.tag == 99)
            {
                [subView setHidden:false];
            }
        }
        
    }
    
    //tag button tapped
    BOOL isSelected = [self.selectedIndexPaths containsObject:indexPath];
    
    if (!isSelected) {
        
        [cell.likeView setFrame:CGRectMake(cell.likeView.frame.origin.x,
                                           328,
                                           cell.likeView.frame.size.width,
                                           cell.likeView.frame.size.height)];
        
        
        if ([[item valueForKey:@"caption"] isEqualToString:@""]) {
            [cell.captionView setHidden:YES];
            
            [cell.buttonBarView setFrame:CGRectMake(cell.buttonBarView.frame.origin.x,
                                                    383,
                                                    cell.buttonBarView.frame.size.width,
                                                    cell.buttonBarView.frame.size.height)];
        }
        else
        {
            [cell.captionView setHidden:NO];
            
            [cell.captionView setFrame:CGRectMake(cell.captionView.frame.origin.x,
                                                  380,
                                                  cell.captionView.frame.size.width,
                                                  cell.captionView.frame.size.height)];
            
            [cell.buttonBarView setFrame:CGRectMake(cell.buttonBarView.frame.origin.x,
                                                    420,
                                                    cell.buttonBarView.frame.size.width,
                                                    cell.buttonBarView.frame.size.height)];
        }
        
        [cell.brandView setHidden:YES];
        [cell.tagButton setSelected:NO];
        
    }
    else
    {
        
        [cell.likeView setFrame:CGRectMake(cell.likeView.frame.origin.x,
                                           376,
                                           cell.likeView.frame.size.width,
                                           cell.likeView.frame.size.height)];
        
        
        if ([[item valueForKey:@"caption"] isEqualToString:@""]) {
            [cell.captionView setHidden:YES];
            
            [cell.buttonBarView setFrame:CGRectMake(cell.buttonBarView.frame.origin.x,
                                                    423,
                                                    cell.buttonBarView.frame.size.width,
                                                    cell.buttonBarView.frame.size.height)];
        }
        else
        {
            [cell.captionView setHidden:NO];
            
            [cell.captionView setFrame:CGRectMake(cell.captionView.frame.origin.x,
                                                  423,
                                                  cell.captionView.frame.size.width,
                                                  cell.captionView.frame.size.height)];
            
            [cell.buttonBarView setFrame:CGRectMake(cell.buttonBarView.frame.origin.x,
                                                    462,
                                                    cell.buttonBarView.frame.size.width,
                                                    cell.buttonBarView.frame.size.height)];
        }
        
        [cell.brandView setHidden:NO];
        [cell.tagButton setSelected:YES];
        
    }
    
    return cell;
    
    
}

- (UITableViewCell *)loadingCell {
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] ;
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = cell.center;
    [cell addSubview:activityIndicator];
    
    [activityIndicator startAnimating];
    
    cell.tag = kTimelineRow;
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int total = allTableData.count;
    
    if (currentPage<totalPage) {
        total = allTableData.count-1;
    }
    if (indexPath.section < total) {
        return [self timelineCellForIndexPath:indexPath];
    } else {
        return [self loadingCell];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (cell.tag == kTimelineRow) {
        currentPage++;
        [self refresh:NULL];
    }
}

-(void)handleTimeline:(NSNotification *)notification {
    NSLog(@"recieved");
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

-(void)handleUpdatedData:(NSNotification *)notification {
    NSLog(@"recieved");
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"profiletimelineID"];

    
    NSString * tempID = [[NSUserDefaults standardUserDefaults] stringForKey:@"profileID"];
    
    if ([tempID isEqualToString:self.idProfile]) {
        //do nothing men
    }
    else
    {
        
        NSMutableArray * tempData = [[[NSUserDefaults standardUserDefaults] objectForKey:@"currentID"] mutableCopy];
        
        if (!tempData.count > 0) {
            tempData = [[NSMutableArray alloc]init];
        }
        
        [tempData addObject:self.idProfile];
        
        [[NSUserDefaults standardUserDefaults] setObject:tempData forKey:@"currentID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        RHZProfileViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"RHZProfileViewController"];
        
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    //    self.viewDeckController.centerController = SharedAppDelegate.profileController;
    
    
}

-(void)handleUpdatedDataItem:(NSNotification *)notification {
    NSLog(@"recieved");
    
    ItemDetailsViewController *controller =  [self.storyboard instantiateViewControllerWithIdentifier:@"itemdetails"];
    controller.itemId = [[NSUserDefaults standardUserDefaults] valueForKey:@"itemdetailsID"];
    
    [self.navigationController pushViewController:controller animated:YES];
}


-(void)hashtagRefresh:(NSNotification *)notification {
    [self.navigationController popViewControllerAnimated:YES];
    self.viewDeckController.centerController = SharedAppDelegate.timelineController;
}


- (void)profileButton:(id)sender event:(id)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.myTableView];
    NSIndexPath *indexPath = [self.myTableView indexPathForRowAtPoint:touchPoint];
    
    NSArray  * items =[self.allTableData objectAtIndex:indexPath.section];
    NSDictionary * item = [items valueForKey:@"post"];
    
    
    NSString * temp = [item valueForKey:@"user_id"];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"profiletimelineID"];
    [[NSUserDefaults standardUserDefaults] setValue:temp forKey:@"profileID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isTimeline"];
    
    //    self.viewDeckController.centerController = SharedAppDelegate.profileController;
    
    
    NSString * tempID = [[NSUserDefaults standardUserDefaults] stringForKey:@"profileID"];
    
    if ([tempID isEqualToString:self.idProfile]) {
        //do nothing men
    }
    else
    {
        
        NSMutableArray * tempData = [[[NSUserDefaults standardUserDefaults] objectForKey:@"currentID"] mutableCopy];
        
        if (!tempData.count > 0) {
            tempData = [[NSMutableArray alloc]init];
        }
        
        [tempData addObject:self.idProfile];
        
        [[NSUserDefaults standardUserDefaults] setObject:tempData forKey:@"currentID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        RHZProfileViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"RHZProfileViewController"];
        
        [self.navigationController pushViewController:controller animated:YES];
    }
    

    
}


- (void)showBrandView:(id)sender event:(id)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.myTableView];
    NSIndexPath *indexPath = [self.myTableView indexPathForRowAtPoint:touchPoint];
    
    
    if (!self.selectedIndexPaths) {
        self.selectedIndexPaths = [NSMutableArray new];
    }
    
    BOOL containsIndexPath = [self.selectedIndexPaths containsObject:indexPath];
    
    if (containsIndexPath) {
        [self.selectedIndexPaths removeObject:indexPath];
    }else{
        [self.selectedIndexPaths addObject:indexPath];
    }
    
    [self.myTableView  beginUpdates];
    
    //[self.myTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
    [self.myTableView reloadData];
    
    [self.myTableView  endUpdates];
    
}

- (void)showTagView:(id)sender event:(id)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.myTableView];
    NSIndexPath *indexPath = [self.myTableView indexPathForRowAtPoint:touchPoint];
    
    
    if (!self.selectedImagePaths) {
        self.selectedImagePaths = [NSMutableArray new];
    }
    
    BOOL containsIndexPath = [self.selectedImagePaths containsObject:indexPath];
    
    if (containsIndexPath) {
        [self.selectedImagePaths removeObject:indexPath];
    }else{
        [self.selectedImagePaths addObject:indexPath];
    }
    
    [self.myTableView  beginUpdates];
    
    [self.myTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    
    //[self.tableView reloadData];
    
    [self.myTableView  endUpdates];
    
}

- (void)showLikeView:(id)sender event:(id)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.myTableView];
    NSIndexPath *indexPath = [self.myTableView indexPathForRowAtPoint:touchPoint];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSArray* items =[self.allTableData objectAtIndex:indexPath.section];
    NSDictionary * item = [[items valueForKey:@"post"] copy];
    
    NSString *cellIdentifier = @"TimelineCell";
    TimelineCell *cell =  (TimelineCell *)[myTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    int likesCount = [[item valueForKey:@"likes_count"]intValue];
    
    NSMutableDictionary* newItem = [NSMutableDictionary dictionaryWithDictionary:item];
    
    if ([[item valueForKey:@"liked_by_me"]integerValue] > 0)
    {
        
        [cell.likeButton setImage:[UIImage imageNamed:@"nglike"] forState:UIControlStateNormal];
        likesCount--;
        
        [newItem setValue:@"0" forKey:@"liked_by_me"];
        
    }
    else
    {
        [cell.likeButton setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
        likesCount++;
        
        [newItem setValue:@"1" forKey:@"liked_by_me"];
    }
    
    [newItem setValue:[NSString stringWithFormat:@"%d",likesCount] forKey:@"likes_count"];
    
    [[allTableData objectAtIndex:indexPath.section] setValue:newItem forKeyPath:@"post"];
    
    [self.myTableView  beginUpdates];
    
    [self.myTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    
    [self.myTableView  endUpdates];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser};
    
    [manager GET:[NSString stringWithFormat:@"%@posts/%@/%@",SERVICE_URL,[item valueForKey:@"id"],SERVICE_LIKE]  parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        NSLog(@"%@",responseObject);
        
        [self.myTableView  beginUpdates];
        
        [self refresh:NULL];
        
        [self.myTableView  endUpdates];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self.myTableView  beginUpdates];
        
        [self refresh:NULL];
        
        [self.myTableView  endUpdates];
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
    
}


- (void)showCommentView:(id)sender event:(id)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.myTableView];
    NSIndexPath *indexPath = [self.myTableView indexPathForRowAtPoint:touchPoint];
    
    
    NSArray* items =[self.allTableData objectAtIndex:indexPath.section];
    NSDictionary * item = [items valueForKey:@"post"];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RHZCommentViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"RHZCommentViewController"];
    
    controller.currId = [[item valueForKey:@"id"]intValue];
    
    
    NSLog(@"%@",item);
    
    controller.imageURL = [item valueForKey:@"image"];
    
    //controller.currentData = [items valueForKey:@"timeline"];
    
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)showAllLovers:(id)sender event:(id)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.myTableView];
    NSIndexPath *indexPath = [self.myTableView indexPathForRowAtPoint:touchPoint];
    
    
    NSArray* items =[self.allTableData objectAtIndex:indexPath.section];
    NSDictionary * item = [items valueForKey:@"post"];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LoverListViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"loverlist"];
    
    controller.currId = [[item valueForKey:@"id"]intValue];
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)showMoreButtons:(id)sender event:(id)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.myTableView];
    NSIndexPath *indexPath = [self.myTableView indexPathForRowAtPoint:touchPoint];
    
    NSArray* items =[self.allTableData objectAtIndex:indexPath.section];
    NSDictionary * item = [items valueForKey:@"post"];
    
    
    moreID = [[item valueForKey:@"id"]intValue];
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] init];
    actionSheet.delegate = self;
    
    if ([[item valueForKey:@"favorite"]intValue] == 1 )
        [actionSheet  addButtonWithTitle:@"Unfavorite"];
    else
        [actionSheet addButtonWithTitle:@"Favorite"];
    
    
    if ([[item valueForKey:@"enable_delete"]intValue] == 1 )
        [actionSheet addButtonWithTitle:@"Delete"];
    else
        [actionSheet addButtonWithTitle:@"Report"];
    
    
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];
    
    
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Favorite"]) {
        [self setFavorite:0];
    }
    else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Unfavorite"])
    {
        [self setFavorite:1];
    }
    
    else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Report"])
    {
        NSLog(@"report");
    }
    else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Delete"])
    {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Information"
                                                          message:@"Are you sure want to delete this post?"
                                                         delegate:self
                                                cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:@"Yes",Nil];
        
        [message show];
    }
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Yes"])
    {
        [self setDelete];
    }
}

- (void)setFavorite:(int)favoriteType
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    NSDictionary *params = @{@"api_key":API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser
                             };
    
    [manager POST:[NSString stringWithFormat:@"%@posts/%d/%@",SERVICE_URL,moreID,SERVICE_PROFILE_FAVORITE] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (favoriteType == 0)
            [ALToastView toastInView:self.view withText:@"Post has been added to favorites"];
        else
            [ALToastView toastInView:self.view withText:@"Post has been removed from favorites"];
        
        
        NSLog(@"%@",responseObject);
        [self refresh:NULL];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}

- (void)setDelete
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    NSDictionary *params = @{@"api_key":API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser
                             };
    
    [manager DELETE:[NSString stringWithFormat:@"%@posts/%d",SERVICE_URL,moreID] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        [ALToastView toastInView:self.view withText:@"Post has been deleted"];
        
        NSLog(@"%@",responseObject);
        [self refresh:NULL];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}

@end
