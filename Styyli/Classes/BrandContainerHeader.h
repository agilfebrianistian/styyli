//
//  BrandContainerHeader.h
//  STYYLI
//
//  Created by Agil Febrianistian on 1/6/15.
//  Copyright (c) 2015 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrandContainerHeader : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UILabel *headertitle;

@property (weak, nonatomic) IBOutlet UIButton *backButton;

@end
