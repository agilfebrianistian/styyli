//
//  TimelineViewController.m
//  STYYLI
//
//  Created by Agil Febrianistian on 3/10/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import "TimelineViewController.h"
#import "EBTagPopover.h"
#import "EBTagPopoverDelegate.h"
#import "CHDraggableView.h"
#import "CHDraggableView+Avatar.h"

#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"


@interface TimelineViewController()<EBTagPopoverDelegate>

@property (readwrite) NSArray *tagPopovers;

@end

@implementation TimelineViewController
@synthesize allTableData,myTableView;
@synthesize draggableView;
@synthesize timelineType;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self registerDevice];
    
    // Initialize Refresh Control
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    
    // Configure Refresh Control
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    
    // Configure View Controller
    [self setRefreshControl:refreshControl];
    
    self.myTableView.backgroundColor = [UIColor blackColor];
    
    allTableData = [[NSMutableArray alloc]init];
    
   
}

-(void)registerDevice
{
    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
    
    
    NSString * deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"];
    if (deviceToken.length == 0) {
        deviceToken = @"";
    }
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key"     : API_KEY,
                             @"auth_token"  : [userdefault valueForKey:@"loginAuth"],
                             @"token"       : deviceToken};
    
    [manager POST:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_REGISTER_DEVICE] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        NSLog(@"%@", responseObject);
        
        if ([[responseObject valueForKey:@"success"]integerValue] == 1) {
            
            
            
        }
        else
        {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:[[responseObject valueForKey:@"message"] objectAtIndex:0] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [message show];
        }
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSLog(@"Error: %@", error);
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
    }];
    
}


- (void)viewDeckController:(IIViewDeckController*)viewDeckController willOpenViewSide:(IIViewDeckSide)viewDeckSide animated:(BOOL)animated
{
    isLeftOpen = YES;
    
    _draggingCoordinator.snappingEdge = CHSnappingEdgeRight;
    [_draggingCoordinator draggableViewReleased:draggableView];
    draggableView.delegate = _draggingCoordinator;
}

- (void)viewDeckController:(IIViewDeckController*)viewDeckController willCloseViewSide:(IIViewDeckSide)viewDeckSide animated:(BOOL)animated
{
    isLeftOpen = NO;
    
    _draggingCoordinator.snappingEdge = CHSnappingEdgeBoth;
    draggableView.delegate = _draggingCoordinator;
    
    if (timelineType != (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"timelineID"]) {
        
        [self refresh:NULL];
    }
    
}



-(void)btnclicked
{
    
    [self.viewDeckController performSelectorOnMainThread:@selector(closeLeftView) withObject:nil waitUntilDone:YES];
    
    //self.navigationController.scrollNavigationBar.scrollView = nil;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    __block ImageFilterViewController *filtersViewController = [storyboard instantiateViewControllerWithIdentifier:@"imagefilter"];
    
    filtersViewController.shouldLaunchAsAVideoRecorder = NO;
    filtersViewController.shouldLaunchAshighQualityVideo = NO;
    filtersViewController.istimeline = YES;
    
    [self.navigationController pushViewController:filtersViewController animated:YES];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
    
    [self.viewDeckController performSelectorOnMainThread:@selector(closeLeftView) withObject:nil waitUntilDone:YES];
    
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    draggableView = [CHDraggableView draggableViewWithImage:[UIImage imageNamed:@"timeline-camera"]];
    
    draggableView.tag = 1;
    
    _draggingCoordinator = [[CHDraggingCoordinator alloc] initWithWindow:window  draggableViewBounds:draggableView.bounds];
    _draggingCoordinator.delegate = self;
    _draggingCoordinator.snappingEdge = CHSnappingEdgeBoth;
    
    draggableView.delegate = _draggingCoordinator;
    
    [window addSubview:draggableView];
}


- (UIViewController *)draggingCoordinator:(CHDraggingCoordinator *)coordinator viewControllerForDraggableView:(CHDraggableView *)draggableView
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *timelineController = [storyboard instantiateViewControllerWithIdentifier:@"timeline"];
    return timelineController;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
     currentPage = 1;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUpdatedData:)
                                                 name:@"DataUpdated"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUpdatedDataItem:)
                                                 name:@"DataItemUpdated"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleCamera:)
                                                 name:@"cameraTouched"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleSearch:)
                                                 name:@"isSearching"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hashtagRefresh:)
                                                 name:@"hashtagData"
                                               object:nil];
    
    self.viewDeckController.delegate = self;
    
    self.navigationController.scrollNavigationBar.scrollView = self.myTableView;
    
    [self addNavigationItem];
    
    [self getNotification];
    
    [self refresh:NULL];
    
}



- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [draggableView removeFromSuperview];
    
    self.navigationController.scrollNavigationBar.scrollView = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"DataUpdated"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"DataItemUpdated"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"cameraTouched"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"isSearching"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"hashtagData"
                                                  object:nil];
    
}


#pragma mark -
#pragma mark get notification

- (void)getNotification
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser,
                             @"id":[UserInfo sharedObject].idUser
                             };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_NOTIFICATIONS_UNREAD] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        if ([[responseObject valueForKey:@"counter"]intValue] > 0) {
            
            UIImage *rightImage1 = [UIImage imageNamed:@"IconItemRightNotification"];
            UIButton *rightButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
            [rightButton1 addTarget:self action:@selector(showNotificationPage) forControlEvents:UIControlEventTouchUpInside];
            rightButton1.bounds = CGRectMake( 0, 0, rightImage1.size.width + 20, rightImage1.size.height );
            [rightButton1 setTitle:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"counter"]] forState:UIControlStateNormal];
            rightButton1.titleLabel.font = [UIFont systemFontOfSize:11.f];
            [rightButton1 setImage:rightImage1 forState:UIControlStateNormal];
            UIBarButtonItem *rightNotificationItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton1];
            self.navigationItem.rightBarButtonItem = rightNotificationItem;
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [[CSActivityViewController sharedObject]hideActivityView];
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}


#pragma mark -
#pragma mark Add Navigation Item

- (void)addNavigationItem
{
    CGRect frame = CGRectMake(0, 0, 0, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame] ;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:16.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"";
    self.navigationItem.titleView = label;
    
    
    UIImage *leftImage = [UIImage imageNamed:@"icon_menu"];
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton addTarget:self action:@selector(showLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    leftButton.bounds = CGRectMake( 0, 0, leftImage.size.width, leftImage.size.height );
    [leftButton setImage:leftImage forState:UIControlStateNormal];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    UIEdgeInsets buttonEdges = UIEdgeInsetsMake(0, LOGO_INSET_STRIP, 0, -LOGO_INSET_STRIP);
    UIImage *leftImagea = [UIImage imageNamed:@"signin-logo.png"];
    UIButton *leftButtona = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButtona.bounds = CGRectMake(0, 0, leftImagea.size.width/2, leftImagea.size.height/2);
    [leftButtona setImage:leftImagea forState:UIControlStateNormal];
    [leftButtona setImageEdgeInsets:buttonEdges];
    [leftButtona setUserInteractionEnabled:NO];
    UIBarButtonItem *leftItema = [[UIBarButtonItem alloc] initWithCustomView:leftButtona];
    
    self.navigationItem.leftBarButtonItems = @[leftItem,leftItema];
    
    UIImage *rightImage1 = [UIImage imageNamed:@"IconItemRightNotification"];
    UIButton *rightButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton1 addTarget:self action:@selector(showNotificationPage) forControlEvents:UIControlEventTouchUpInside];
    rightButton1.bounds = CGRectMake( 0, 0, rightImage1.size.width, rightImage1.size.height );
    [rightButton1 setImage:rightImage1 forState:UIControlStateNormal];
    UIBarButtonItem *rightNotificationItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton1];
    
    self.navigationItem.rightBarButtonItem = rightNotificationItem;
    
}

- (void)hashtagNavigation
{
    UIImage *leftImage = [UIImage imageNamed:@"icon_menu"];
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton addTarget:self action:@selector(showLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    leftButton.bounds = CGRectMake( 0, 0, leftImage.size.width, leftImage.size.height );
    [leftButton setImage:leftImage forState:UIControlStateNormal];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    UIBarButtonItem *leftItema = [[UIBarButtonItem alloc] init];
    
    self.navigationItem.leftBarButtonItems = @[leftItem,leftItema];
    
    CGRect frame = CGRectMake(0, 0, 0, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame] ;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:16.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = [NSString stringWithFormat:@"%@%@",@"#",[[NSUserDefaults standardUserDefaults] valueForKey:@"hashtagString"]];
    self.navigationItem.titleView = label;
    
}


- (void)refresh:(id)sender
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params;
    timelineType = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"timelineID"];
    
    if (timelineType == 4)  {
        
        params = @{@"api_key": API_KEY,
                   @"auth_token":[UserInfo sharedObject].tokenUser
                   };
        
        [manager GET:[NSString stringWithFormat:@"%@posts/%@.json",SERVICE_URL,[[NSUserDefaults standardUserDefaults] valueForKey:@"notificationPost"]] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSLog(@"%@",responseObject);
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            
            allTableData = [[NSMutableArray alloc]init];
            
            NSMutableArray* temp = (NSMutableArray*)responseObject;
            
            [allTableData addObject:[temp mutableCopy]];
            
            [self.myTableView reloadData];
            [(UIRefreshControl *)sender endRefreshing];
            
            [self.myTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
            
            [self.navigationController.scrollNavigationBar resetToDefaultPosition:YES];
            
            [self addNavigationItem];
            
            [self getNotification];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            NSLog(@"Error: %@", error);
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [message show];
            
        }];
        
    }
    else if (timelineType == 3)  {
        
        params = @{@"api_key": API_KEY,
                   @"auth_token":[UserInfo sharedObject].tokenUser
                   };
        
        [manager GET:[NSString stringWithFormat:@"%@hashtags/%@.json",SERVICE_URL,[[NSUserDefaults standardUserDefaults] valueForKey:@"hashtagString"]] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSLog(@"%@",responseObject);
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            allTableData = [[NSMutableArray alloc]init];
            
            NSMutableArray* temp = [[(NSMutableArray*)responseObject valueForKey:@"posts"] mutableCopy];
            
            for (NSMutableArray*items in temp) {
                NSMutableArray* tempitem = [items mutableCopy];
                [allTableData addObject:tempitem];
            }
            
            [self.myTableView reloadData];
            [(UIRefreshControl *)sender endRefreshing];
            
            [self.myTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
            
            [self.navigationController.scrollNavigationBar resetToDefaultPosition:YES];
            
            [self hashtagNavigation];
            
            [self getNotification];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            NSLog(@"Error: %@", error);
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [message show];
            
        }];
        
    }
    else if (timelineType == 2) {
        
        params = @{@"api_key":API_KEY,
                   @"auth_token":[UserInfo sharedObject].tokenUser
                   };
        
        [manager GET:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_TRENDSETTER] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            
 
            
            allTableData = [[NSMutableArray alloc]init];
            
            NSMutableArray* temp = [[(NSMutableArray*)responseObject valueForKey:@"posts"] mutableCopy];
            
            for (NSMutableArray*items in temp) {
                NSMutableArray* tempitem = [items mutableCopy];
                [allTableData addObject:tempitem];
            }
            
            [self.myTableView reloadData];
            [(UIRefreshControl *)sender endRefreshing];
            [self addNavigationItem];
            
            [self getNotification];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            NSLog(@"Error: %@", error);
            [(UIRefreshControl *)sender endRefreshing];
            
            
            UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [message show];
            
        }];
    }
    
    else
    {
        
        
        if (timelineType == 0) {
            
            params = @{@"api_key":API_KEY,
                       @"auth_token":[UserInfo sharedObject].tokenUser,
                       @"search":@"",
                       @"page":[NSString stringWithFormat:@"%d",currentPage]
                       };
            
        }
        else if (timelineType == 1)
        {
            params = @{@"api_key":API_KEY,
                       @"auth_token":[UserInfo sharedObject].tokenUser,
                       @"search":@"",
                       @"id": [UserInfo sharedObject].idUser
                       };
        }
        
        
        [manager GET:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_TIMELINE] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            allTableData = [[NSMutableArray alloc]init];
            
            NSMutableArray* temp = [[(NSMutableArray*)responseObject valueForKey:@"posts"] mutableCopy];
            
           // NSLog(@"%@",responseObject);
  
            NSLog(@"result: %@", responseObject);
            
            
            totalPage = [[responseObject objectForKey:@"total_page"] intValue];

            
            for (NSMutableArray*items in temp) {
                NSMutableArray* tempitem = [items mutableCopy];
                [allTableData addObject:tempitem];
            }
            
            // Reload Table View
            [self.myTableView reloadData];
            
            // End Refreshing
            [(UIRefreshControl *)sender endRefreshing];
            
            //[self addNavigationItem];
            
            [self getNotification];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            
            NSLog(@"Error: %@", error);
            [(UIRefreshControl *)sender endRefreshing];
            UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [message show];
            
        }];
    }
}




#pragma mark -
#pragma mark Button Function

- (void)showLeftMenu
{
    [self.viewDeckController performSelectorOnMainThread:@selector(toggleLeftView) withObject:nil waitUntilDone:YES];
}

- (void)showNotificationPage
{
    UIViewController *filtersViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"notificationlist"];
    [self.navigationController pushViewController:filtersViewController animated:YES];
}

#pragma mark - Picture Button Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Yes"])
    {
        [self setDelete];
    }
    else
    {
        [self reportAbuse:title];
    }
    
}




- (void)showImageFilter
{
    [draggableView removeFromSuperview];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    ImagePostViewController *navcon = [storyboard instantiateViewControllerWithIdentifier:@"imagepost"];
    
    [self.navigationController pushViewController:navcon animated:YES];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //return [allTableData count] ? [allTableData count] : 0;
    
    if (currentPage == 1) {
        return [allTableData count] ? [allTableData count] : 0;
    }
    return self.allTableData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSArray  * items =[self.allTableData objectAtIndex:section];
    NSDictionary * item = [items valueForKey:@"post"];
    NSString *cellIdentifier = @"TimelineHeaderCell";
    TimelineHeaderCell *header =  (TimelineHeaderCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    NSURL *urlImage = [item valueForKey:@"user_avatar"];
    [header.timelineAvatar sd_setBackgroundImageWithURL:urlImage forState:UIControlStateNormal placeholderImage:PLACEHOLDER_AVATAR options:SDWebImageRefreshCached];
    
    [header.timelineAvatar addTarget:self action:@selector(profileButton:event:) forControlEvents:UIControlEventTouchUpInside];
    header.timelineAvatar.layer.cornerRadius = 20.0f;
    header.timelineAvatar.clipsToBounds = YES;
    header.timelineAvatar.layer.borderColor=[[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f] CGColor];
    header.timelineAvatar.layer.borderWidth= 1.2f;
    
    header.dateView.layer.cornerRadius = 5;
    
    header.timelineFullName.text    = [item valueForKey:@"fullname"];
    header.timelineUsername.text    = [item valueForKey:@"username"];
    header.timelineTime.text        = [item valueForKey:@"created_at"];
    header.timelinePostdate.text    = [item valueForKey:@"created"];
    
    
    return header;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    NSArray* items =[self.allTableData objectAtIndex:indexPath.section];
    NSDictionary * item = [items valueForKey:@"post"];
    
    if (![self.selectedIndexPaths containsObject:indexPath]) {
        if ([[item valueForKey:@"caption"] isEqualToString:@""])
        {
            return 420;
        }
        else
        {
            return 460;
        }
        
    }
    else
    {
        if ([[item valueForKey:@"caption"] isEqualToString:@""])
        {
            return 460;
        }
        else
        {
            return 500;
        }
    }
}

- (UITableViewCell *)timelineCellForIndexPath:(NSIndexPath *)indexPath {

    NSArray* items =[self.allTableData objectAtIndex:indexPath.section];
    NSDictionary * item = [items valueForKey:@"post"];
    NSString *cellIdentifier = @"TimelineCell";
    
    TimelineCell *cell =  (TimelineCell *)[myTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[TimelineCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"TimelineCell"];
        
    }
    
    cell.clipsToBounds = true;
    
    [cell.commentButton addTarget:self action:@selector(showCommentView:event:) forControlEvents:UIControlEventTouchUpInside];
    [cell.commentButton setTitle:[NSString stringWithFormat:@" %@",[item valueForKey:@"comments_count"]] forState:UIControlStateNormal];
    
    [cell initText];
    
    [cell.captionAtributed setString:[item valueForKey:@"caption"]];
    
    [cell.likeButton setTitle:[NSString stringWithFormat:@" %@",[item valueForKey:@"likes_count"] ] forState:UIControlStateNormal];
    
    if ([[item valueForKey:@"liked_by_me"]integerValue] > 0)
        [cell.likeButton setImage:[UIImage imageNamed:@"nglike"] forState:UIControlStateNormal];
    else
        [cell.likeButton setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
    
    
    
    NSURL *imageUrl = [item valueForKey:@"image"];
    
    __block UIActivityIndicatorView *activityIndicator;
    __weak UIImageView *weakImageView = cell.mainPicture;
    
    [cell.mainPicture sd_setImageWithURL:imageUrl
                        placeholderImage:PLACEHOLDER_IMAGE
                                 options:SDWebImageProgressiveDownload
                                progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                    if (!activityIndicator) {
                                        [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge]];
                                        activityIndicator.center = weakImageView.center;
                                        [activityIndicator startAnimating];
                                    }
                                }
                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                   [activityIndicator removeFromSuperview];
                                   activityIndicator = nil;
                               }];
    
    NSArray * tempTag = [item valueForKey:@"items"];
    
    NSMutableArray * tags = [[NSMutableArray alloc]init];
    
    for (NSArray * tagItem in tempTag) {
        
        NSDictionary * dictTag = [tagItem valueForKey:@"item"];
        
        float tempX = 0.0f;
        float tempY = 0.0f;
        
        if (![[dictTag valueForKey:@"x"] isEqual:[NSNull null]] | ![[dictTag valueForKey:@"y"] isEqual:[NSNull null]]) {
            tempX = [[dictTag valueForKey:@"x"]floatValue];
            tempY = [[dictTag valueForKey:@"y"]floatValue];
        }
        
        [tags addObject: [DEMOTag tagWithProperties:@{@"tagPosition" : [NSValue valueWithCGPoint:CGPointMake(tempX,tempY)],
                                                      @"tagText" : [dictTag valueForKey:@"name"]}]];
    }
    
    [cell initTagPopoverFrom:tags];
    
    cell.itemsData = [item valueForKey:@"items"];
    [cell initItems];
    
    cell.loversData = [item valueForKey:@"likers"];
    [cell initLovers];
    
    if ([[item valueForKey:@"items"] count] > 0) {
        
        [cell.tagButton setHidden:FALSE];
    }else
    {
        [cell.tagButton setHidden:TRUE];
    }
    
    if ([[item valueForKey:@"likers"] count]>4)
        [cell.viewLoveButton setHidden:NO];
    else
        [cell.viewLoveButton setHidden:YES];
    
    [cell.viewLoveButton addTarget:self action:@selector(showAllLovers:event:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.tagButton addTarget:self action:@selector(showBrandView:event:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.likeButton addTarget:self action:@selector(showLikeView:event:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.moreButton addTarget:self action:@selector(showMoreButtons:event:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.pictButton addTarget:self action:@selector(showTagView:event:) forControlEvents:UIControlEventTouchUpInside];
    
    
    //image button tapped
    
    BOOL isSelectedimage = [self.selectedImagePaths containsObject:indexPath];
    
    if (!isSelectedimage) {
        for (UIView *subView in cell.mainPicture.subviews)
        {
            if (subView.tag == 99)
            {
                [subView setHidden:true];
            }
        }
    }
    else
    {
        [self.view bringSubviewToFront:cell.mainPicture];
        
        for (UIView *subView in cell.mainPicture.subviews)
        {
            if (subView.tag == 99)
            {
                [subView setHidden:false];
            }
        }
        
    }
    
    //tag button tapped
    BOOL isSelected = [self.selectedIndexPaths containsObject:indexPath];
    
    if (!isSelected) {
        
        [cell.likeView setFrame:CGRectMake(cell.likeView.frame.origin.x,
                                           328,
                                           cell.likeView.frame.size.width,
                                           cell.likeView.frame.size.height)];
        
        
        if ([[item valueForKey:@"caption"] isEqualToString:@""]) {
            [cell.captionView setHidden:YES];
            
            [cell.buttonBarView setFrame:CGRectMake(cell.buttonBarView.frame.origin.x,
                                                    383,
                                                    cell.buttonBarView.frame.size.width,
                                                    cell.buttonBarView.frame.size.height)];
        }
        else
        {
            [cell.captionView setHidden:NO];
            
            [cell.captionView setFrame:CGRectMake(cell.captionView.frame.origin.x,
                                                  380,
                                                  cell.captionView.frame.size.width,
                                                  cell.captionView.frame.size.height)];
            
            [cell.buttonBarView setFrame:CGRectMake(cell.buttonBarView.frame.origin.x,
                                                    420,
                                                    cell.buttonBarView.frame.size.width,
                                                    cell.buttonBarView.frame.size.height)];
        }
        
        [cell.brandView setHidden:YES];
        [cell.tagButton setSelected:NO];
        
    }
    else
    {
        
        [cell.likeView setFrame:CGRectMake(cell.likeView.frame.origin.x,
                                           376,
                                           cell.likeView.frame.size.width,
                                           cell.likeView.frame.size.height)];
        
        
        if ([[item valueForKey:@"caption"] isEqualToString:@""]) {
            [cell.captionView setHidden:YES];
            
            [cell.buttonBarView setFrame:CGRectMake(cell.buttonBarView.frame.origin.x,
                                                    423,
                                                    cell.buttonBarView.frame.size.width,
                                                    cell.buttonBarView.frame.size.height)];
        }
        else
        {
            [cell.captionView setHidden:NO];
            
            [cell.captionView setFrame:CGRectMake(cell.captionView.frame.origin.x,
                                                  423,
                                                  cell.captionView.frame.size.width,
                                                  cell.captionView.frame.size.height)];
            
            [cell.buttonBarView setFrame:CGRectMake(cell.buttonBarView.frame.origin.x,
                                                    462,
                                                    cell.buttonBarView.frame.size.width,
                                                    cell.buttonBarView.frame.size.height)];
        }
        
        [cell.brandView setHidden:NO];
        [cell.tagButton setSelected:YES];
        
    }
    
    return cell;
    
    
}

- (UITableViewCell *)loadingCell {
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] ;
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = cell.center;
    [cell addSubview:activityIndicator];
   
    [activityIndicator startAnimating];
    
    cell.backgroundColor = [UIColor clearColor];
 
    
    cell.tag = kTimelineRow;
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int total = allTableData.count;
    
    if (currentPage<totalPage) {
        total = allTableData.count-1;
    }
    
    if (indexPath.section < total) {
        return [self timelineCellForIndexPath:indexPath];
    } else {
        return [self loadingCell];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (cell.tag == kTimelineRow) {
        currentPage++;
        
        myTableView.sectionHeaderHeight = 1.0f;
        
        double delayInSeconds = 2;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self refresh:NULL];
        });
        

    }
}



#pragma mark - NOTIFICATION HANDLER

-(void)handleUpdatedData:(NSNotification *)notification {
    NSLog(@"recieved");
    
    NSMutableArray * tempData = [[NSMutableArray alloc]init];
    
    [[NSUserDefaults standardUserDefaults] setObject:tempData forKey:@"currentID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"profiletimelineID"];
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RHZProfileViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"RHZProfileViewController"];
    
    [self.navigationController pushViewController:controller animated:YES];
    
    //  self.viewDeckController.centerController = SharedAppDelegate.profileController;
}

-(void)handleUpdatedDataItem:(NSNotification *)notification {
    NSLog(@"recieved");
    
    ItemDetailsViewController *controller =  [self.storyboard instantiateViewControllerWithIdentifier:@"itemdetails"];
    controller.itemId = [[NSUserDefaults standardUserDefaults] valueForKey:@"itemdetailsID"];
    controller.istimeline = true;
    
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)handleSearch:(NSNotification *)notification {
    
    bool isSearching = [[NSUserDefaults standardUserDefaults] boolForKey:@"isSearching"];
    
    double delayInSeconds = 0.8;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        if (isSearching) {
            [draggableView setHidden:YES];
        }
        else
        {
            [draggableView setHidden:NO];
        }
    });
}


-(void)handleCamera:(NSNotification *)notification {
    NSLog(@"btnclicked");
    
    [self btnclicked];
}


-(void)hashtagRefresh:(NSNotification *)notification {
    
    [self refresh:NULL];
    
}

#pragma mark - BUTTON CELL HANDLER

- (void)profileButton:(id)sender event:(id)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:touchPoint];
    
    NSArray  * items =[self.allTableData objectAtIndex:indexPath.section];
    NSDictionary * item = [items valueForKey:@"post"];
    
    
    NSString * temp = [item valueForKey:@"user_id"];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"profiletimelineID"];
    [[NSUserDefaults standardUserDefaults] setValue:temp forKey:@"profileID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //    self.viewDeckController.centerController = SharedAppDelegate.profileController;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RHZProfileViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"RHZProfileViewController"];
    
    [self.navigationController pushViewController:controller animated:YES];
    
}


- (void)showBrandView:(id)sender event:(id)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:touchPoint];
    
    
    if (!self.selectedIndexPaths) {
        self.selectedIndexPaths = [NSMutableArray new];
    }
    
    BOOL containsIndexPath = [self.selectedIndexPaths containsObject:indexPath];
    
    if (containsIndexPath) {
        [self.selectedIndexPaths removeObject:indexPath];
    }else{
        [self.selectedIndexPaths addObject:indexPath];
    }
    
    [self.tableView  beginUpdates];
    
    //[self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    
    [self.tableView reloadData];
    
    [self.tableView  endUpdates];
    
}

- (void)showTagView:(id)sender event:(id)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:touchPoint];
    
    
    if (!self.selectedImagePaths) {
        self.selectedImagePaths = [NSMutableArray new];
    }
    
    BOOL containsIndexPath = [self.selectedImagePaths containsObject:indexPath];
    
    if (containsIndexPath) {
        [self.selectedImagePaths removeObject:indexPath];
    }else{
        [self.selectedImagePaths addObject:indexPath];
    }
    
    [self.tableView  beginUpdates];
    
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    
    //[self.tableView reloadData];
    
    [self.tableView  endUpdates];
    
}

- (void)showLikeView:(id)sender event:(id)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:touchPoint];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSArray* items =[self.allTableData objectAtIndex:indexPath.section];
    NSDictionary * item = [[items valueForKey:@"post"] copy];
    
    NSString *cellIdentifier = @"TimelineCell";
    TimelineCell *cell =  (TimelineCell *)[myTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    int likesCount = [[item valueForKey:@"likes_count"]intValue];
    
    NSMutableDictionary* newItem = [NSMutableDictionary dictionaryWithDictionary:item];
    
    if ([[item valueForKey:@"liked_by_me"]integerValue] > 0)
    {
        
        [cell.likeButton setImage:[UIImage imageNamed:@"nglike"] forState:UIControlStateNormal];
        likesCount--;
        
        [newItem setValue:@"0" forKey:@"liked_by_me"];
        
    }
    else
    {
        [cell.likeButton setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
        likesCount++;
        
        [newItem setValue:@"1" forKey:@"liked_by_me"];
    }
    
    [newItem setValue:[NSString stringWithFormat:@"%d",likesCount] forKey:@"likes_count"];
    
    [[allTableData objectAtIndex:indexPath.section] setValue:newItem forKeyPath:@"post"];
    
    [self.myTableView  beginUpdates];
    
    [self.myTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    
    [self.myTableView  endUpdates];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser};
    
    [manager GET:[NSString stringWithFormat:@"%@posts/%@/%@",SERVICE_URL,[item valueForKey:@"id"],SERVICE_LIKE]  parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        [self.tableView  beginUpdates];
        
        [self refresh:NULL];
        
        [self.tableView  endUpdates];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self.tableView  beginUpdates];
        
        [self refresh:NULL];
        
        [self.tableView  endUpdates];
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
    
}


- (void)showCommentView:(id)sender event:(id)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:touchPoint];
    
    
    NSArray* items =[self.allTableData objectAtIndex:indexPath.section];
    NSDictionary * item = [items valueForKey:@"post"];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RHZCommentViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"RHZCommentViewController"];
    
    controller.currId = [[item valueForKey:@"id"]intValue];
    
    controller.imageURL = [item valueForKey:@"image"];
    
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)showAllLovers:(id)sender event:(id)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:touchPoint];
    
    
    NSArray* items =[self.allTableData objectAtIndex:indexPath.section];
    NSDictionary * item = [items valueForKey:@"post"];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LoverListViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"loverlist"];
    
    controller.currId = [[item valueForKey:@"id"]intValue];
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)showMoreButtons:(id)sender event:(id)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:touchPoint];
    
    NSArray* items =[self.allTableData objectAtIndex:indexPath.section];
    NSDictionary * item = [items valueForKey:@"post"];
    
    
    moreID = [[item valueForKey:@"id"]intValue];
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] init];
    actionSheet.delegate = self;
    
    if ([[item valueForKey:@"favorite"]intValue] == 1 )
        [actionSheet  addButtonWithTitle:@"Unfavorite"];
    else
        [actionSheet addButtonWithTitle:@"Favorite"];
    
    
    if ([[item valueForKey:@"enable_delete"]intValue] == 1 )
        [actionSheet addButtonWithTitle:@"Delete"];
    else
        [actionSheet addButtonWithTitle:@"Report"];
    
    
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];
    
    
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Favorite"]) {
        [self setFavorite:0];
    }
    else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Unfavorite"])
    {
        [self setFavorite:1];
    }
    else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Report"])
    {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Report Timeline"
                                                          message:@"Why are you reporting this post?"
                                                         delegate:self
                                                cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:@"I don't like this timeline",
                                @"This timeline is spam or scam",
                                @"This timeline puts people/brand at risk",
                                @"This timeline shouldn't be on STYYLI",
                                Nil];
        [message show];
        
        
    }
    else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Delete"])
    {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Information"
                                                          message:@"Are you sure want to delete this post?"
                                                         delegate:self
                                                cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:@"Yes",Nil];
        [message show];
    }
    
    
    
    
    
}


- (void)setFavorite:(int)favoriteType
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    NSDictionary *params = @{@"api_key":API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser
                             };
    
    [manager POST:[NSString stringWithFormat:@"%@posts/%d/%@",SERVICE_URL,moreID,SERVICE_PROFILE_FAVORITE] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (favoriteType == 0)
            [ALToastView toastInView:self.view withText:@"Post has been added to favorites"];
        else
            [ALToastView toastInView:self.view withText:@"Post has been removed from favorites"];
        
        [self refresh:NULL];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}

- (void)setDelete
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key":API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser
                             };
    
    [manager DELETE:[NSString stringWithFormat:@"%@posts/%d",SERVICE_URL,moreID] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [ALToastView toastInView:self.view withText:@"Post has been deleted"];
        
        NSLog(@"%@",responseObject);
        [self refresh:NULL];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}

#pragma mark -
#pragma mark report abuse

- (void)reportAbuse:(NSString*)text
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key"                 : API_KEY,
                             @"auth_token"              : [UserInfo sharedObject].tokenUser,
                             @"abuse_report[post_id]"   : [NSString stringWithFormat:@"%d",moreID],
                             @"abuse_report[content]"   : text
                             };
    
    [manager POST:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_PROFILE_REPORT] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        [ALToastView toastInView:self.view withText:@"Post has been reported"];
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}

@end
