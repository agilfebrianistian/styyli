//
//  BrandTableViewController.m
//  STYYLI
//
//  Created by Agil Febrianistian on 4/19/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import "BrandTableViewController.h"
#import "BrandTableCell.h"
#import "postData.h"

@interface BrandTableViewController ()

@end

@implementation BrandTableViewController
@synthesize topCategory;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isexpanded = false;
    
    [self.navigationController setNavigationBarHidden:NO animated:TRUE];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[CSActivityViewController sharedObject] showActivityView];
    
    topCategory = [[NSMutableArray alloc]init];
    
    [self getHeader];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getHeader
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": @"kuyainside"};
    
    [manager GET:@"http://styyli.kuyainside.com/top_category.json?api_key=kuyainside" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"%@",responseObject);
        
        NSArray *results = [(NSDictionary *)responseObject objectForKey:@"top_categories"];
        
        for (NSDictionary * items in results) {
            NSDictionary * item = [items valueForKey:@"top_category"];
            
            [topCategory addObject:item];
        }
        
        [self.menuTableView reloadData];
        
        [[CSActivityViewController sharedObject] hideActivityView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.topCategory count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray  * items = [topCategory objectAtIndex:indexPath.row];
    
    static NSString *CellIdentifier = @"CategoryCell";
    BrandTableCell *cell =  (BrandTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[BrandTableCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
        
    }
    
    
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = [UIColor grayColor];
    cell.selectedBackgroundView = bgView;
    cell.lblTitle.text = [items valueForKey:@"name"];
    cell.lblTitle.textColor = [UIColor blackColor];
    
    [cell setIndentationLevel:[[[self.topCategory objectAtIndex:indexPath.row] valueForKey:@"level"] intValue]];
    cell.indentationWidth = 25;
    
    float indentPoints = cell.indentationLevel * cell.indentationWidth;
    
    cell.contentView.frame = CGRectMake(indentPoints,cell.contentView.frame.origin.y,cell.contentView.frame.size.width - indentPoints,cell.contentView.frame.size.height);
    
    
    NSLog(@"%@",items);
    
    
    if([items valueForKey:@"categories"])
    {
        cell.btnExpand.alpha = 1.0;
        [cell.btnExpand addTarget:self action:@selector(showSubItems:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        cell.btnExpand.alpha = 0.0;
    }
    return cell;
    
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray  * items = [topCategory objectAtIndex:indexPath.row];
    
    if([items valueForKey:@"categories"])
    {
        
        NSArray *arr=[[items valueForKey:@"categories"] valueForKey:@"category"];
        
        BOOL isTableExpanded=NO;
        
        for(NSDictionary *subitems in arr )
        {
            NSInteger index=[self.topCategory indexOfObjectIdenticalTo:subitems];
            isTableExpanded=(index>0 && index!=NSIntegerMax);
            if(isTableExpanded) break;
        }
        
        if(isTableExpanded)
        {
            [self CollapseRows:arr];
        }
        else
        {
            NSUInteger count=indexPath.row+1;
            NSMutableArray *arrCells=[NSMutableArray array];
            for(NSDictionary *dInner in arr )
            {
                [arrCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
                [self.topCategory insertObject:dInner atIndex:count++];
            }
            [self.menuTableView insertRowsAtIndexPaths:arrCells withRowAnimation:UITableViewRowAnimationLeft];
        }
    }
    else
    {
        
        [postData sharedObject].categoryId = [items valueForKey:@"id"];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ImagePostViewController *filtersViewController = [storyboard instantiateViewControllerWithIdentifier:@"imagepost"];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            filtersViewController.modalPresentationStyle = UIModalPresentationFormSheet;
        }
        
        [self.navigationController pushViewController:filtersViewController animated:YES];
        
        
    }
    
    
}

-(void)CollapseRows:(NSArray*)ar
{
    
    for(NSDictionary *dInner in ar )
    {
		NSUInteger indexToRemove=[self.topCategory indexOfObjectIdenticalTo:dInner];
		NSArray *arInner=[dInner valueForKey:@"categories"];
		if(arInner && [arInner count]>0)
        {
			[self CollapseRows:arInner];
		}
		
		if([self.topCategory indexOfObjectIdenticalTo:dInner]!=NSNotFound)
        {
			[self.topCategory removeObjectIdenticalTo:dInner];
			[self.menuTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:
                                                        [NSIndexPath indexPathForRow:indexToRemove inSection:0]
                                                        ]
                                      withRowAnimation:UITableViewRowAnimationLeft];
        }
	}
    
}



-(void)showSubItems :(id) sender
{
    UIButton *btn = (UIButton*)sender;
    CGRect buttonFrameInTableView = [btn convertRect:btn.bounds toView:self.menuTableView];
    NSIndexPath *indexPath = [self.menuTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    if(btn.alpha==1.0)
    {
        if ([[btn imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"down-arrow.png"]])
        {
            [btn setImage:[UIImage imageNamed:@"up-arrow.png"] forState:UIControlStateNormal];
        }
        else
        {
            [btn setImage:[UIImage imageNamed:@"down-arrow.png"] forState:UIControlStateNormal];
        }
        
    }
    
    NSDictionary *d=[self.topCategory objectAtIndex:indexPath.row] ;
    
    NSArray *arr=[[d valueForKey:@"categories"] valueForKey:@"category"];
    
    
    if([d valueForKey:@"categories"])
    {
        BOOL isTableExpanded=NO;
        for(NSDictionary *subitems in arr )
        {
            NSInteger index=[self.topCategory indexOfObjectIdenticalTo:subitems];
            isTableExpanded=(index>0 && index!=NSIntegerMax);
            if(isTableExpanded) break;
        }
        
        if(isTableExpanded)
        {
            [self CollapseRows:arr];
        }
        else
        {
            NSUInteger count=indexPath.row+1;
            NSMutableArray *arrCells=[NSMutableArray array];
            for(NSDictionary *dInner in arr )
            {
                [arrCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
                [self.topCategory insertObject:dInner atIndex:count++];
            }
            [self.menuTableView insertRowsAtIndexPaths:arrCells withRowAnimation:UITableViewRowAnimationLeft];
        }
    }
    
}


@end
