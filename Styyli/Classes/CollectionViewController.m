//
//  CollectionViewController.m
//  STYYLI
//
//  Created by Agil Febrianistian on 7/17/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import "CollectionViewController.h"
#import "IIViewDeckController.h"

#import "UIImageView+WebCache.h"

@interface CollectionViewController ()

@end

@implementation CollectionViewController

@synthesize allTableData,headerView;

@synthesize profilePicture,profileName,profileStatus,profieCity,profileSite;
@synthesize profileButton;
@synthesize profileFollowing,profileFollower;
@synthesize profileStylesCount,profileWardrobeCount,profileFavoriteCount;
@synthesize handlerText,segmentedCategory;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    
    [self addNavigationItem];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Add Navigation Item

- (void)addNavigationItem
{
    UIImage *leftImage = [UIImage imageNamed:@"icon_menu"];
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton addTarget:self action:@selector(showLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    leftButton.bounds = CGRectMake( 0, 0, leftImage.size.width, leftImage.size.height );
    [leftButton setImage:leftImage forState:UIControlStateNormal];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    UIImage *rightImage1 = [UIImage imageNamed:@"IconItemRightNotification"];
    UIButton *rightButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton1 addTarget:self action:@selector(showNotificationPage) forControlEvents:UIControlEventTouchUpInside];
    rightButton1.bounds = CGRectMake( 0, 0, rightImage1.size.width, rightImage1.size.height );
    [rightButton1 setImage:rightImage1 forState:UIControlStateNormal];
    UIBarButtonItem *rightNotificationItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton1];
    
    self.navigationItem.leftBarButtonItem = leftItem;
    self.navigationItem.rightBarButtonItem = rightNotificationItem;
}

#pragma mark -
#pragma mark Button Function

- (void)showLeftMenu
{
    [self.viewDeckController performSelectorOnMainThread:@selector(toggleLeftView) withObject:nil waitUntilDone:YES];
}

- (void)showNotificationPage
{
    UIViewController *filtersViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"notificationlist"];
    [self.navigationController pushViewController:filtersViewController animated:YES];
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self getData];
    [self getNotification];
    
    [self refreshWithCategory:(int)[segmentedCategory selectedSegmentIndex]];
}

#pragma mark -
#pragma mark get notification

- (void)getNotification
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser,
                             @"id":[UserInfo sharedObject].idUser
                             };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_NOTIFICATIONS_UNREAD] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"%@",responseObject);
        
        if ([[responseObject valueForKey:@"counter"]intValue] > 0) {
            UIImage *rightImage1 = [UIImage imageNamed:@"IconItemRightNotification"];
            UIButton *rightButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
            [rightButton1 addTarget:self action:@selector(showNotificationPage) forControlEvents:UIControlEventTouchUpInside];
            rightButton1.bounds = CGRectMake( 0, 0, rightImage1.size.width + 20, rightImage1.size.height );
            [rightButton1 setTitle:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"counter"]] forState:UIControlStateNormal];
            rightButton1.titleLabel.font = [UIFont systemFontOfSize:11.f];
            [rightButton1 setImage:rightImage1 forState:UIControlStateNormal];
            UIBarButtonItem *rightNotificationItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton1];
            self.navigationItem.rightBarButtonItem = rightNotificationItem;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [[CSActivityViewController sharedObject]hideActivityView];
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}

#pragma mark -
#pragma mark get data from server

- (void)getData
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser,
                             };
    
    [manager GET:[NSString stringWithFormat:@"%@users/%@/%@",SERVICE_URL,[UserInfo sharedObject].idUser,SERVICE_PROFILE] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"%@",responseObject);
        
        
        [[CSActivityViewController sharedObject]hideActivityView];
        
        profileFollower.text = [NSString stringWithFormat:@"%@ followers",[responseObject valueForKey:@"followers"]];
        profileFollowing.text = [NSString stringWithFormat:@"%@ following",[responseObject valueForKey:@"followings"]];
        
        profileFavoriteCount.text = [NSString stringWithFormat:@"%@",[responseObject valueForKey:@"favorites"]];
        profileStylesCount.text = [NSString stringWithFormat:@"%@",[responseObject valueForKey:@"posts_count"]];
        profileWardrobeCount.text = [NSString stringWithFormat:@"%@",[responseObject valueForKey:@"items"]];
        
        NSDictionary * userItem =[responseObject valueForKey:@"user"];
        
        if ([userItem valueForKey:@"city"] == (id)[NSNull null])
            profieCity.text = @"";
        else
            profieCity.text = [NSString stringWithFormat:@"%@",[userItem valueForKey:@"city"]];
        
        if ([userItem valueForKey:@"website"]  == (id)[NSNull null])
            profileSite.text = @"";
        else
            profileSite.text = [NSString stringWithFormat:@"%@",[userItem valueForKey:@"website"]];
        
        if ([userItem valueForKey:@"description"]  == (id)[NSNull null])
            profileStatus.text = @"";
        else
            profileStatus.text = [NSString stringWithFormat:@"%@",[userItem valueForKey:@"description"]];
        
        
        CGRect frame = CGRectMake(0, 0, 0, 44);
        UILabel *label = [[UILabel alloc] initWithFrame:frame] ;
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont boldSystemFontOfSize:16.0];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor whiteColor];
        label.text = [userItem valueForKey:@"username"];
        self.navigationItem.titleView = label;
        
        profileName.text = [userItem valueForKey:@"fullname"];
        
        if([userItem valueForKey:@"image_small"]==(id) [NSNull null])
            self.profilePicture.image  = PLACEHOLDER_AVATAR;
        else
            [self.profilePicture sd_setImageWithURL:[NSURL URLWithString:[userItem valueForKey:@"image_small"]] placeholderImage:PLACEHOLDER_AVATAR options:SDWebImageRefreshCached];

        
        
        
        self.profilePicture.layer.cornerRadius = 50.0f;
        self.profilePicture.layer.masksToBounds=YES;
        self.profilePicture.layer.borderColor=[[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f] CGColor];
        self.profilePicture.layer.borderWidth= 2.5f;
        
        
        switch ([[responseObject valueForKey:@"followed_by_me"] integerValue]) {
            case 0:
            {
                [profileButton setImage:[UIImage imageNamed:@"icon_edit"] forState:UIControlStateNormal];
            }
                break;
            case 1:
            {
                [profileButton setImage:[UIImage imageNamed:@"icon_sudah_difollow"] forState:UIControlStateNormal];
            }
                break;
            case 2:
            {
                [profileButton setImage:[UIImage imageNamed:@"icon_belum_difollow"] forState:UIControlStateNormal];
            }
                break;
                
            default:
                break;
        }
        
        profileButton.tag = [[responseObject valueForKey:@"followed_by_me"] integerValue];
        [profileButton addTarget:self action:@selector(editButton) forControlEvents:UIControlEventTouchUpInside];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [[CSActivityViewController sharedObject]hideActivityView];
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}


-(void)editButton
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProfileSettingViewController *filtersViewController = [storyboard instantiateViewControllerWithIdentifier:@"profilesetting"];
    
    filtersViewController.idProfile     = [[NSUserDefaults standardUserDefaults] stringForKey:@"profileID"];
    filtersViewController.bioText       = profileStatus.text;
    filtersViewController.locationText  = profieCity.text;
    filtersViewController.websiteText   = profileSite.text;
    
    
    [self.navigationController pushViewController:filtersViewController animated:YES];
    
}

- (void)refreshWithCategory:(int)category
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token": [UserInfo sharedObject].tokenUser
                             };
    NSString *url;
    if (category == 0)
        url = [NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_PROFILE_COLLECTION_LOVE];
    else
        url = [NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_PROFILE_COLLECTION_TAG];
    
        [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
        [[CSActivityViewController sharedObject]hideActivityView];
            
         allTableData = [[NSMutableArray alloc]init];
           
            
         NSLog(@"%@",responseObject);
           
         NSArray *results = [(NSDictionary *)responseObject objectForKey:@"items"];
           
            if (results.count == 0) {
                
                handlerText.hidden = false;
            }
            else
            {
                
                handlerText.hidden = true;
                
                for (NSDictionary * items in results) {
                    NSDictionary * item = [items valueForKey:@"item"];
                    
                    [allTableData addObject:item];
                }
            }
            
            
            // Reload Table View
            [self.myCollection reloadData];

        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            NSLog(@"%@",error);
            
            handlerText.hidden = false;
            
            [[CSActivityViewController sharedObject]hideActivityView];
            
            UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [message show];
            
        }];
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return allTableData.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray  * items =[allTableData objectAtIndex:indexPath.row];
    
    CollectionCell *cell = (CollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionCell" forIndexPath:indexPath];
    
    cell.brandName.text = [items valueForKey:@"brand_name"];
    cell.itemName.text = [items valueForKey:@"name"];
    
    __block UIActivityIndicatorView *activityIndicator;
    __weak UIImageView *weakImageView = cell.itemImage;
    
    [cell.itemImage sd_setImageWithURL:[items valueForKey:@"image_url"]
                        placeholderImage:PLACEHOLDER_IMAGE
                                 options:SDWebImageProgressiveDownload
                                progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                    if (!activityIndicator) {
                                        [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite]];
                                        activityIndicator.center = weakImageView.center;
                                        [activityIndicator startAnimating];
                                    }
                                }
                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                   [activityIndicator removeFromSuperview];
                                   activityIndicator = nil;
                               }];
    
    
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray  * items =[allTableData objectAtIndex:indexPath.row];
    
    
    NSLog(@"%@",[items valueForKey:@"id"]);
    
    ItemDetailsViewController *controller =  [self.storyboard instantiateViewControllerWithIdentifier:@"itemdetails"];
    controller.itemId = [items valueForKey:@"id"];
    
    
    
    [self.navigationController pushViewController:controller animated:YES];
    
}

- (IBAction)segmentedChange:(id)sender {
    
    [[CSActivityViewController sharedObject]showActivityView];
    
    UISegmentedControl *segment = (UISegmentedControl*)sender;
    
    [self refreshWithCategory:(int)segment.selectedSegmentIndex];
    
}


@end
