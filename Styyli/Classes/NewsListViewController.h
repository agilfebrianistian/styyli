//
//  NewsListViewController.h
//  STYYLI
//
//  Created by Agil Febrianistian on 7/6/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "CSActivityViewController.h"
#import "UIImageView+AFNetworking.h"
#import "Constant.h"
#import "UserInfo.h"
#import "HomeCell.h"
#import "NewsDetailsViewController.h"

@interface NewsListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>


@property (weak, nonatomic) IBOutlet UITableView *newsListTable;

@property (nonatomic) int currId;

@property (strong, nonatomic) NSArray* allTableData;

@end
