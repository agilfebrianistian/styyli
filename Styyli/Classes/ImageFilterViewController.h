//
//  ImageFilterViewController.h
//  STYYLI
//
//  Created by Agil Febrianistian on 4/8/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#define kFilterImageViewTag 9999
#define kFilterImageViewContainerViewTag 9998
#define kBlueDotImageViewOffset 25.0f
#define kFilterCellHeight 72.0f
#define kBlueDotAnimationTime 0.2f
#define kFilterTableViewAnimationTime 0.2f
#define kGPUImageViewAnimationOffset 27.0f
#import "InstaFilters.h"
#import "UIImage+IF.h"
#import "CategoryTableViewController.h"

@interface ImageFilterViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, IFVideoCameraDelegate>

@property int curFlashMode;

@property (nonatomic, unsafe_unretained) BOOL shouldLaunchAsAVideoRecorder;
@property (nonatomic, unsafe_unretained) BOOL shouldLaunchAshighQualityVideo;

@property (nonatomic, strong) UIButton *transparentBackButton;

@property (nonatomic, weak) IBOutlet UIButton *photoAlbumButton;
@property (nonatomic, weak) IBOutlet UIButton *shootButton;

@property (nonatomic, weak) IBOutlet UIButton *toggleFiltersButton;

@property (nonatomic, weak) IBOutlet UIButton *cancelAlbumPhotoButton;
@property (nonatomic, weak) IBOutlet UIButton *confirmAlbumPhotoButton;

@property (nonatomic, weak) IBOutlet UIView *filterTableViewContainerView;
@property (nonatomic, weak) IBOutlet UITableView *filtersTableView;


@property (nonatomic, strong) UIImageView *backgroundImageView;
@property (nonatomic, strong) UIImageView *cameraToolBarImageView;
@property (nonatomic, strong) UIImageView *cameraCaptureBarImageView;


@property (nonatomic, strong) UIImageView *blueDotImageView;
@property (nonatomic, strong) UIImageView *cameraTrayImageView;

@property (nonatomic, strong) IFVideoCamera *videoCamera;

@property (nonatomic, unsafe_unretained) IFFilterType currentType;


- (IBAction)changeCameraBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *changeCameraBtn;
- (IBAction)changeFlashBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *changeFlashBtn;

@property (nonatomic, unsafe_unretained) BOOL isInVideoRecorderMode;
@property (nonatomic, unsafe_unretained) BOOL isHighQualityVideo;
@property (nonatomic, unsafe_unretained) BOOL isFiltersTableViewVisible;
@property (weak, nonatomic) IBOutlet UIView *bottombarView;


- (void)toggleFiltersButtonPressed:(id)sender;
- (void)photoAlbumButtonPressed:(id)sender;
- (void)shootButtonPressed:(id)sender;
- (void)shootButtonTouched:(id)sender;
- (void)shootButtonCancelled:(id)sender;
- (void)cancelAlbumPhotoButtonPressed:(id)sender;
- (void)confirmAlbumPhotoButtonPressed:(id)sender;


@property (nonatomic) bool isGallery;
@property (nonatomic) BOOL istimeline;


@end
