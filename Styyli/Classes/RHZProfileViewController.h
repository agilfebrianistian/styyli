//
//  RHZProfileViewController.h
//  Styyli
//
//  Created by R. Hafidhullah Zakariyya on 12/23/13.
//  Copyright (c) 2013 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "UserInfo.h"
#import "TimelineCell.h"
#import "TimelineHeaderCell.h"
#import "RHZCommentViewController.h"
#import "CSActivityViewController.h"
#import "PAImageView.h"
#import "LoverListViewController.h"
#import "Constant.h"
#import "ProfileSettingViewController.h"
#import "ALToastView.h"
#import "ItemDetailsViewController.h"

@interface RHZProfileViewController : UIViewController<UIActionSheetDelegate>
{
    int moreID;
    bool isTimeline;
    int currentPage;
    int totalPage;
}

@property (weak, nonatomic) IBOutlet UIImageView *profilePicture;
@property (weak, nonatomic) IBOutlet UILabel *profileName;
@property (weak, nonatomic) IBOutlet UILabel *profileStatus;
@property (weak, nonatomic) IBOutlet UILabel *profieCity;
@property (weak, nonatomic) IBOutlet UILabel *profileSite;

@property (weak, nonatomic) IBOutlet UIButton *profileButton;


@property (weak, nonatomic) IBOutlet UILabel *profileFollowing;
@property (weak, nonatomic) IBOutlet UILabel *profileFollower;

@property (weak, nonatomic) IBOutlet UILabel *profileStylesCount;
@property (weak, nonatomic) IBOutlet UILabel *profileWardrobeCount;
@property (weak, nonatomic) IBOutlet UILabel *profileFavoriteCount;

@property (strong, nonatomic) NSMutableDictionary *userData;

@property (strong, nonatomic) NSMutableArray* allTableData;

@property (weak, nonatomic) IBOutlet UITableView *myTableView;

@property (nonatomic, strong) NSMutableArray *selectedIndexPaths;

@property (nonatomic, strong) NSMutableArray *selectedImagePaths;


@property (strong, nonatomic) NSString* idProfile;


@property (weak, nonatomic) IBOutlet UIView *headerView;

@property int profileType;


@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;


@end
