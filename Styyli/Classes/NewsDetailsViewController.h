//
//  NewsDetailsViewController.h
//  STYYLI
//
//  Created by Agil Febrianistian on 7/25/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "UserInfo.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "CSActivityViewController.h"

@interface NewsDetailsViewController : UIViewController

@property (strong, nonatomic) NSString* itemId;

@property (weak, nonatomic) IBOutlet UIImageView *newsPict;
@property (weak, nonatomic) IBOutlet UITextView *newsText;

@property (weak, nonatomic) IBOutlet UILabel *newsTitle;

@property (strong, nonatomic) IBOutlet UIScrollView *scroller;

@end
