//
//  HomeViewController.h
//  STYYLI
//
//  Created by Agil Febrianistian on 6/14/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeCell.h"
#import "Constant.h"
#import "UserInfo.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "CSActivityViewController.h"
#import "iCarousel.h"
#import "ItemsViewController.h"
#import "POTDViewController.h"
#import "NewsDetailsViewController.h"

@interface HomeViewController : UIViewController<iCarouselDataSource,iCarouselDelegate>
{

    int sliderCounter;

}

@property (weak, nonatomic) IBOutlet UITableView *homeTable;

@property (nonatomic) int currId;
@property (weak, nonatomic) IBOutlet iCarousel *carouselView;

@property (strong, nonatomic) NSArray* allTableData;

@property (strong, nonatomic) NSArray* tableArray;

@property (strong, nonatomic) NSArray* pageArray;

@property (strong, nonatomic) NSArray* itemsArray;

@property (weak, nonatomic) IBOutlet UIButton *itemsButton;

@property (strong, nonatomic) IBOutlet UIPageControl *pageControlMenu;


@property (strong, nonatomic) IBOutlet UIScrollView *scroller;


- (IBAction)moreNewsButton:(id)sender;

- (IBAction)feedButton:(id)sender;

- (IBAction)POTDButton:(id)sender;

- (IBAction)newItemsButton:(id)sender;




@end
