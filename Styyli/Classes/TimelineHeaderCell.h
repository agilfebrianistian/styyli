//
//  TimelineHeaderCell.h
//  STYYLI
//
//  Created by Agil Febrianistian on 3/6/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimelineHeaderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *timelineAvatar;
@property (weak, nonatomic) IBOutlet UILabel *timelineUsername;
@property (weak, nonatomic) IBOutlet UILabel *timelineFullName;
@property (weak, nonatomic) IBOutlet UILabel *timelinePostdate;
@property (weak, nonatomic) IBOutlet UILabel *timelineTime;
@property (weak, nonatomic) IBOutlet UIView *dateView;




@end
