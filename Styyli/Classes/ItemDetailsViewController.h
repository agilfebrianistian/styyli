//
//  ItemDetailsViewController.h
//  STYYLI
//
//  Created by Agil Febrianistian on 7/21/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "UserInfo.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "CSActivityViewController.h"
#import "RHZCommentViewController.h"

@interface ItemDetailsViewController : UIViewController<UIActionSheetDelegate>

{
    NSString * link;
}

@property (strong, nonatomic) IBOutlet UIScrollView *scroller;
@property (strong, nonatomic) NSString* itemId;

@property (nonatomic) BOOL istimeline;

@property (strong, nonatomic) NSString* itemImage;

@property (weak, nonatomic) IBOutlet UIImageView *itemPicture;
@property (weak, nonatomic) IBOutlet UILabel *itemName;
@property (weak, nonatomic) IBOutlet UILabel *itemPrice;
@property (weak, nonatomic) IBOutlet UILabel *itemSize;

@property (weak, nonatomic) IBOutlet UITextView *itemDescription;
@property (weak, nonatomic) IBOutlet UIButton *loveButton;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;

- (IBAction)loveButton:(id)sender;
- (IBAction)commentButton:(id)sender;
- (IBAction)shareButton:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *buyButton;
- (IBAction)butButtonPressed:(id)sender;

@property (strong, nonatomic) NSMutableArray* allTableData;


@end
