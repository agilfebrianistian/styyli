//
//  BrandContainer.h
//  STYYLI
//
//  Created by Agil Febrianistian on 1/6/15.
//  Copyright (c) 2015 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrandContainerCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *itemImage;

@property (weak, nonatomic) IBOutlet UILabel *itemtitle;


@end
