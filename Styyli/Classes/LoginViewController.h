//
//  ViewController.h
//  Applist
//
//  Created by Agil Febrianistian on 2/4/14.
//  Copyright (c) 2014 Agil Febrianistian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "RHZAppDelegate.h"
#import "UserInfo.h"
#import "ForgotPassViewController.h"
#import "Constant.h"

@interface LoginViewController : UIViewController<UITextFieldDelegate,UIAlertViewDelegate>
{
    bool isUp;
}

@property (weak, nonatomic) IBOutlet UITextField *usernameText;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;
- (IBAction)submitButton:(id)sender;
- (IBAction)bgTapped:(id)sender;

- (IBAction)forgotPasswordBU:(id)sender;

@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
