//
//  NewsDetailsViewController.m
//  STYYLI
//
//  Created by Agil Febrianistian on 7/25/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import "NewsDetailsViewController.h"
#import "UIImageView+WebCache.h"

@interface NewsDetailsViewController ()

@end

@implementation NewsDetailsViewController
@synthesize scroller;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    
    [scroller setScrollEnabled:true];
    
    [scroller setContentSize:CGSizeMake(320,568)];
    
    
    [self addNavigationItem];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Add Navigation Item

- (void)addNavigationItem
{
    UIImage *leftImage = [UIImage imageNamed:@"IconBackToTimeline"];
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton addTarget:self action:@selector(showLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    leftButton.bounds = CGRectMake( 0, 0, leftImage.size.width, leftImage.size.height );
    [leftButton setImage:leftImage forState:UIControlStateNormal];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    

    
    self.navigationItem.leftBarButtonItem = leftItem;
    
    CGRect frame = CGRectMake(0, 0, 0, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame] ;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:16.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"News";
    self.navigationItem.titleView = label;
    
}

- (void)showLeftMenu
{
    NSArray *tempViewControllers = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    [self.navigationController popToViewController:[tempViewControllers objectAtIndex:[tempViewControllers count]-2] animated:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[CSActivityViewController sharedObject]showActivityView];
    
    [self getData];
    
}

#pragma mark -
#pragma mark get data from server

- (void)getData
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser
                             };
    
    [manager GET:[NSString stringWithFormat:@"%@news/%@/%@",SERVICE_URL,self.itemId,SERVICE_ITEM_DETAILS]   parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        [[CSActivityViewController sharedObject]hideActivityView];
        
        NSLog(@"%@",responseObject);
        
        
        NSMutableArray *item = [responseObject valueForKey:@"news"];
        
        if([[[item valueForKey:@"image"] valueForKey:@"image"]valueForKey:@"url"]==(id) [NSNull null])
            [self.newsPict setImage:PLACEHOLDER_IMAGE];
        else
        {
        
        
        NSURL *imageUrl = [[[item valueForKey:@"image"] valueForKey:@"image"]valueForKey:@"url"];
        
        __block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = self.newsPict;
        
        [self.newsPict sd_setImageWithURL:imageUrl
                            placeholderImage:PLACEHOLDER_IMAGE
                                     options:SDWebImageProgressiveDownload
                                    progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                        if (!activityIndicator) {
                                            [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge]];
                                            activityIndicator.center = weakImageView.center;
                                            [activityIndicator startAnimating];
                                        }
                                    }
                                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                       [activityIndicator removeFromSuperview];
                                       activityIndicator = nil;
                                   }];
        }
        
        
        self.newsText.text = [item valueForKey:@"description"];
        self.newsTitle.text = [item valueForKey:@"title"];

        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [[CSActivityViewController sharedObject]hideActivityView];
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}


@end
