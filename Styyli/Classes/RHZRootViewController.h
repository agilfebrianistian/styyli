//
//  RHZRootViewController.h
//  Styyli
//
//  Created by R. Hafidhullah Zakariyya on 12/23/13.
//  Copyright (c) 2013 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"

@interface RHZRootViewController : IIViewDeckController

@end
