//
//  ImagePostViewController.m
//  Styyli
//
//  Created by Agil Febrianistian on 2/14/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import "ImagePostViewController.h"
#import "DEMOComment.h"
#import "DEMOPhoto.h"
#import "DEMOTag.h"
#import <QuartzCore/QuartzCore.h>
#import "EBPhotoPagesController.h"
#import "EBPhotoPagesFactory.h"
#import "EBTagPopover.h"
#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"


@interface ImagePostViewController ()

@end

@implementation ImagePostViewController
@synthesize imageScreen,captiontext,scroller,locationText,tagsData;
@synthesize facebookButton;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.autoCompleteMgr = [[MJAutoCompleteManager alloc] init];
        self.autoCompleteMgr.dataSource = self;
        self.autoCompleteMgr.delegate = self;
        
        
        MJAutoCompleteTrigger *atTrigger = [[MJAutoCompleteTrigger alloc] initWithDelimiter:@"@"];
        atTrigger.cell = @"MDCustomAutoCompleteCell";
        
        [self.autoCompleteMgr addAutoCompleteTrigger:atTrigger];
        
        
        [self getHashtag:NULL];
        
        
        
        
    }
    return self;
}

- (void)getHashtag:(id)sender
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser
                             };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_HASHTAGS]   parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSLog(@"%@",responseObject);
        
        NSArray *results = [(NSDictionary *)responseObject valueForKey:@"hashtag"] ;
        
        if (results == (id)[NSNull null]) {
            
            
        }
        else
        {
            //            NSString *path = [[NSBundle mainBundle] pathForResource:@"Countries" ofType:@"plist"];
            //            NSArray *names = [[NSArray arrayWithContentsOfFile:path] valueForKey:@"name"];
            
            NSArray *names = [[results valueForKey:@"hashtag"] valueForKey:@"name"];
            
            
            NSArray *items = [MJAutoCompleteItem autoCompleteCellModelFromObjects:names];
            
            MJAutoCompleteTrigger *hashTrigger = [[MJAutoCompleteTrigger alloc] initWithDelimiter:@"#"
                                                                                autoCompleteItems:items];
            
            [self.autoCompleteMgr addAutoCompleteTrigger:hashTrigger];
            
            
            
        }
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [(UIRefreshControl *)sender endRefreshing];
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
    }];
    
}

- (void)getMention:(id)sender
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser
                             };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_SEARCH_PEOPLE]   parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSLog(@"%@",responseObject);
        
        NSArray *results = [(NSDictionary *)responseObject valueForKey:@"users"] ;
        
        if (results == (id)[NSNull null]) {
            
            
        }
        else
        {
            
            
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [(UIRefreshControl *)sender endRefreshing];
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
    }];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.autoCompleteMgr.container = self.autoCompleteContainer;
    
    tagsData = [[NSMutableArray alloc]init];
    
    [postData sharedObject].step = @"0";
    
    [scroller setScrollEnabled:true];
    
    [scroller setContentSize:CGSizeMake(320,568)];
    
    imageData = [postData sharedObject].pictureData;
    
    UIImage* image = [UIImage imageWithData:imageData];
    
    imageScreen.image = image;
    
    [self setSimulateLatency:NO];
    
    [self setPhotos:@[
                      [DEMOPhoto photoWithProperties:
                       @{@"imageFile":imageData,
                         }],
                      
                      ]];
    
    DEMOPhoto *photo;
    photo = self.photos[0];
    photo.disabledCommenting =YES;
    
    
    self.locationText.text = @"";
    
    [self addNavigationItem];
    [self locationCheck];
    
    photoPagesController = [[EBPhotoPagesController alloc] initWithDataSource:self delegate:self];
}


- (void)addNavigationItem
{
    UIImage *leftImage = [UIImage imageNamed:@"IconBackToTimeline"];
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton addTarget:self action:@selector(showLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    leftButton.bounds = CGRectMake( 0, 0, leftImage.size.width, leftImage.size.height );
    [leftButton setImage:leftImage forState:UIControlStateNormal];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    UIButton *rightButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton1 addTarget:self action:@selector(postMenu) forControlEvents:UIControlEventTouchUpInside];
    rightButton1.bounds = CGRectMake( 0, 0, 50, 40 );
    [rightButton1 setTitle:@"Done" forState:UIControlStateNormal];
    UIBarButtonItem *rightNotificationItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton1];
    
    UIEdgeInsets buttonEdges = UIEdgeInsetsMake(0, LOGO_INSET_ARROW, 0, -LOGO_INSET_ARROW);
    UIImage *leftImagea = [UIImage imageNamed:@"signin-logo.png"];
    UIButton *leftButtona = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButtona.bounds = CGRectMake(0, 0, leftImagea.size.width/2, leftImagea.size.height/2);
    [leftButtona setImage:leftImagea forState:UIControlStateNormal];
    [leftButtona setImageEdgeInsets:buttonEdges];
    [leftButtona setUserInteractionEnabled:NO];
    UIBarButtonItem *leftItema = [[UIBarButtonItem alloc] initWithCustomView:leftButtona];
    
    self.navigationItem.leftBarButtonItems = @[leftItem,leftItema];
    self.navigationItem.rightBarButtonItem = rightNotificationItem;
    
}


- (void)showLeftMenu
{
    NSArray *tempViewControllers = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    [self.navigationController popToViewController:[tempViewControllers objectAtIndex:[tempViewControllers count]-2] animated:YES];
}



- (void)postMenu
{
    
    
    [self dismissKeyboard];
    
    [[CSActivityViewController sharedObject] showActivityView];
    [self doPost];
    
    //    [self postFacebookwithurlimage:nil];
    
    //    FBSession *session = [[FBSession alloc] init];
    //    [FBSession setActiveSession:session];
    //
    //    if ([[FBSession activeSession] isOpen]) {
    //        if ([[[FBSession activeSession] permissions]indexOfObject:@"publish_actions"] == NSNotFound) {
    //
    //            [[FBSession activeSession] requestNewPublishPermissions:[NSArray arrayWithObject:@"publish_actions"] defaultAudience:FBSessionDefaultAudienceFriends
    //                                                  completionHandler:^(FBSession *session,NSError *error){
    //                                                 //error
    //                                                  }];
    //
    //        }else{
    //                 [self postFacebook];
    //        }
    //    }else{
    //
    //        [FBSession openActiveSessionWithPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
    //                                           defaultAudience:FBSessionDefaultAudienceOnlyMe
    //                                              allowLoginUI:YES
    //                                         completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
    //                                             if (!error && status == FBSessionStateOpen) {
    //                                                 [self postFacebook];
    //                                             }else{
    //                                                 NSLog(@"error");
    //                                             }
    //                                         }];
    //    }
    
    
}


- (IBAction)addBrand:(id)sender {
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleCancelData:)
                                                 name:@"cancelData"
                                               object:nil];
    
    
    [self presentViewController:photoPagesController animated:YES completion:nil];
    
    [photoPagesController enterTaggingMode];
    
}

-(void)handleCancelData:(NSNotification *)notification {
    NSLog(@"recieved");
    
    //    [photoPagesController cancelCurrentTagging];
    //    [photoPagesController enterTaggingMode];
    
}

- (IBAction)openEditor:(id)sender
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    __block ImageFilterViewController *filtersViewController = [storyboard instantiateViewControllerWithIdentifier:@"imagefilter"];
    
    
    filtersViewController.shouldLaunchAsAVideoRecorder = NO;
    filtersViewController.shouldLaunchAshighQualityVideo = NO;
    
    
    [self presentViewController:filtersViewController animated:YES completion:^(){
        filtersViewController = nil;
        
    }];
    
}

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    self.imageScreen.image = croppedImage;
    
    imageData = UIImageJPEGRepresentation(imageScreen.image, 0.5);
    
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - LOCATION

-(void)locationCheck
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    [locationManager stopUpdatingLocation];
    CLLocation *location = [locations lastObject];
    
    CLGeocoder * geocoder = [[CLGeocoder alloc] init];
    
    [geocoder reverseGeocodeLocation: location completionHandler:
     ^(NSArray *placemarks, NSError *error) {
         
         //Get nearby address
         CLPlacemark *placemark = [placemarks objectAtIndex:0];
         
         //String to hold address
         NSString* locatedAtcountry = placemark.country;
         NSString* locatedAtcity = placemark.locality;
         
         [locationText setText:[NSString stringWithFormat:@"%@, %@",locatedAtcity,locatedAtcountry]];
         
     }];
}

#pragma mark -
#pragma mark Attributed String

-(NSMutableAttributedString*)decorateMentions:(NSString *)stringWithTags{
    
    NSError *error = nil;
    NSMutableAttributedString *attString=[[NSMutableAttributedString alloc] initWithString:stringWithTags];
    
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"@(\\w+)" options:0 error:&error];
    NSArray *matches = [regex matchesInString:stringWithTags options:0 range:NSMakeRange(0, stringWithTags.length)];
    
    
    for (NSTextCheckingResult *match in matches) {
        NSRange wordRange = [match rangeAtIndex:0];
        
        //Set Foreground Color
        UIColor *foregroundColor=[UIColor colorWithRed:129.0/255.0 green:171.0/255.0 blue:193.0/255.0 alpha:1.0];
        [attString addAttribute:NSForegroundColorAttributeName value:foregroundColor range:wordRange];
        
    }
    
    NSRegularExpression *regex2 = [NSRegularExpression regularExpressionWithPattern:@"#(\\w+)" options:0 error:&error];
    NSArray *matches2 = [regex2 matchesInString:stringWithTags options:0 range:NSMakeRange(0, stringWithTags.length)];
    
    for (NSTextCheckingResult *match in matches2) {
        NSRange wordRange = [match rangeAtIndex:0];
        
        //Set Foreground Color
        UIColor *foregroundColor=[UIColor colorWithRed:129.0/255.0 green:171.0/255.0 blue:193.0/255.0 alpha:1.0];
        [attString addAttribute:NSForegroundColorAttributeName value:foregroundColor range:wordRange];
        
    }
    
    
    return attString;
}

#pragma mark - Textfield Animation


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (!isUp) {
        [self animateTextField: textField up: YES];
        isUp = YES;
    }
    
    [self.imageButton setEnabled:false];
    [scroller setScrollEnabled:false];
    
}

- (IBAction)textfieldChange:(id)sender {
    
    
    captiontext.attributedText = [self decorateMentions:captiontext.text];
    
    [self.autoCompleteMgr processString:captiontext.text];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (isUp == YES) {
        [self animateTextField: textField up: NO];
        isUp = NO;
    }
    
    [self.imageButton setEnabled:TRUE];
    [scroller setScrollEnabled:true];
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    //110
    
    const int movementDistance = 210; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    
    [self.autoCompleteMgr processString:@" "];
    
    
    return NO;
}

#pragma mark - MJAutoCompleteMgr DataSource Methods

- (void)autoCompleteManager:(MJAutoCompleteManager *)acManager
         itemListForTrigger:(MJAutoCompleteTrigger *)trigger
                 withString:(NSString *)string
                   callback:(MJAutoCompleteListCallback)callback
{
    
    if ([trigger.delimiter isEqual:@"#"])
    {
        callback(trigger.autoCompleteItemList);
    }
    else if ([trigger.delimiter isEqual:@"@"])
    {
        
        if (!self.didStartDownload)
        {
            self.didStartDownload = YES;
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            
            
            NSDictionary *params = @{@"api_key": API_KEY,
                                     @"auth_token":[UserInfo sharedObject].tokenUser
                                     };
            
            [manager GET:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_SEARCH_PEOPLE]   parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                
                NSLog(@"%@",responseObject);
                
                NSArray *results = [(NSDictionary *)responseObject valueForKey:@"users"] ;
                
                if (results == (id)[NSNull null]) {
                    
                    
                }
                else
                {
                    NSMutableArray *itemList = [NSMutableArray array];
                    
                    for (NSDictionary *dict in results)
                    {
                        
                        MJAutoCompleteItem *item = [[MJAutoCompleteItem alloc] init];
                        item.autoCompleteString = [[dict valueForKey:@"user"] valueForKey:@"username"];
                        item.context = dict;
                        
                        [itemList addObject:item];
                    }
                    
                    trigger.autoCompleteItemList = itemList;
                    callback(itemList);
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                NSLog(@"Error: %@", error);
                
                UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [message show];
            }];
            
        }
        else
        {
            callback(trigger.autoCompleteItemList);
        }
    }
}

#pragma mark - MJAutoCompleteMgr Delegate methods

- (void)autoCompleteManager:(MJAutoCompleteManager *)acManager
            willPresentCell:(id)autoCompleteCell
                 forTrigger:(MJAutoCompleteTrigger *)trigger
{
    if ([trigger.delimiter isEqual:@"@"])
    {
        MDCustomAutoCompleteCell *cell = autoCompleteCell;
        NSDictionary *context = cell.autoCompleteItem.context;
        
        //        [cell.avatarImageView setImageWithURL:[NSURL URLWithString:DICT_GET([context valueForKey:@"user"], @"image_small")] placeholderImage:PLACEHOLDER_AVATAR];
        
        [cell.avatarImageView sd_setImageWithURL:[NSURL URLWithString:DICT_GET([context valueForKey:@"user"], @"image_small")] placeholderImage:PLACEHOLDER_AVATAR options:SDWebImageRefreshCached];
        
        
        
        cell.avatarImageView.layer.cornerRadius = 20.0f;
        cell.avatarImageView.clipsToBounds = YES;
        cell.avatarImageView.layer.borderColor=[[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f] CGColor];
        cell.avatarImageView.layer.borderWidth= 1.2f;
        
    }
}

- (void)autoCompleteManager:(MJAutoCompleteManager *)acManager shouldUpdateToText:(NSString *)newText
{
    self.captiontext.text = newText;
}

#pragma mark - Keyboard controller

- (IBAction)bgTapped:(id)sender {
    [self dismissKeyboard];
}

-(void)dismissKeyboard {
    if ([captiontext isFirstResponder])
    {
        [captiontext resignFirstResponder];
    }
}


-(void)doPost{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    NSString * checkedString = @"";
    
    for (NSString* tag in tagsData) {
        
        if (checkedString.length == 0)
            checkedString = [NSString stringWithFormat:@"%@",tag];
        else
            checkedString = [NSString stringWithFormat:@"%@,%@",checkedString,tag];
    }
    
    
    
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token": [UserInfo sharedObject].tokenUser,
                             @"post[caption]": captiontext.text,
                             @"items": checkedString,
                             @"post[location]" : locationText.text};
    
    NSLog(@"%@",params);
    
    
    [manager POST:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_POSTS] parameters:params
     
constructingBodyWithBlock:^(id<AFMultipartFormData> formData){
    
    if (imageData != NULL) {
        [formData appendPartWithFileData:imageData name:@"post[image]" fileName:@"photoname.jpg" mimeType:@"image/jpeg"];
        
    }
}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              [[CSActivityViewController sharedObject] hideActivityView];
              
              
              NSLog(@"%@",responseObject);
              
              
              NSArray *tempViewControllers = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
              [self.navigationController popToViewController:[tempViewControllers objectAtIndex:[tempViewControllers count]-3] animated:YES];
              
              [self dismissViewControllerAnimated:YES completion:^() {
                  // do nothing
              }];
              
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              [[CSActivityViewController sharedObject] hideActivityView];
              
              NSLog(@"Error: %@", error);
              
              
              UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
              [message show];
          }];
}


-(void)uploadPhoto{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:SERVICE_URL]];
    NSData *imageDatas = UIImageJPEGRepresentation(self.imageScreen.image, 0.5);
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token": [UserInfo sharedObject].tokenUser,
                             @"post[caption]": captiontext.text,
                             @"items": tagsData,
                             @"post[location]" : locationText.text};
    AFHTTPRequestOperation *op = [manager POST:SERVICE_POSTS parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //do not put image inside parameters dictionary as I did, but append it!
        [formData appendPartWithFileData:imageDatas name:@"image" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
    }];
    [op start];
}


- (void)postFacebookwithurlimage : (NSString*) imageurl {
    
    //    UIImage *img = imageScreen.image;
    //
    //    FBPhotoParams *params = [[FBPhotoParams alloc] init];
    //    params.photos = @[img];
    
    BOOL isSuccessful = NO;
    BOOL canPresent = [FBDialogs canPresentMessageDialog];
    
    if (canPresent) {
        //        FBAppCall *appCall = [FBDialogs presentShareDialogWithPhotoParams:params
        //                                                              clientState:nil
        //                                                                  handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
        //                                                                      if (error) {
        //                                                                          NSLog(@"Error: %@", error.description);
        //                                                                      } else {
        //                                                                          NSLog(@"Success!");
        //                                                                      }
        //                                                                  }];
        
        
        FBAppCall *appCall = [FBDialogs presentMessageDialogWithLink:[NSURL URLWithString:@"http://www.styyli.com"]
                                                                name:@"styyli post"
                                                             caption:@"ngetest kegantengan"
                                                         description:@"test test test"
                                                             picture:[NSURL URLWithString:@"http://s3.amazonaws.com/styylistaging2/uploads/news/news/image/40/thumb_dummy2.jpg"]
                                                         clientState:nil
                                                             handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                                                 if (error) {
                                                                     NSLog(@"Error: %@", error.description);
                                                                 } else {
                                                                     NSLog(@"Success!");
                                                                 }
                                                             }];
        
        isSuccessful = (appCall  != nil);
        
    }
    
    if (!isSuccessful) {
        [self performPublishAction:^{
            FBRequestConnection *connection = [[FBRequestConnection alloc] init];
            connection.errorBehavior = FBRequestConnectionErrorBehaviorReconnectSession
            | FBRequestConnectionErrorBehaviorAlertUser
            | FBRequestConnectionErrorBehaviorRetry;
            
            [connection addRequest:[FBRequest requestForPostStatusUpdate:@"test"]
                 completionHandler:^(FBRequestConnection *innerConnection, id result, NSError *error) {
                     
                     [self showAlert:@"Photo Post" result:result error:error];
                     
                 }];
            [connection start];
        }];
    }
}


// Convenience method to perform some action that requires the "publish_actions" permissions.
- (void)performPublishAction:(void(^)(void))action {
    if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound) {
        [FBSession.activeSession requestNewPublishPermissions:@[@"publish_actions"]
                                              defaultAudience:FBSessionDefaultAudienceFriends
                                            completionHandler:^(FBSession *session, NSError *error) {
                                                if (!error) {
                                                    action();
                                                } else if (error.fberrorCategory != FBErrorCategoryUserCancelled) {
                                                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Permission denied"
                                                                                                        message:@"Unable to get permission to post"
                                                                                                       delegate:nil
                                                                                              cancelButtonTitle:@"OK"
                                                                                              otherButtonTitles:nil];
                                                    [alertView show];
                                                }
                                            }];
    } else {
        action();
    }
    
}

- (void)showAlert:(NSString *)message
           result:(id)result
            error:(NSError *)error {
    
    NSString *alertMsg;
    NSString *alertTitle;
    if (error) {
        alertTitle = @"Error";
        if (error.fberrorUserMessage && FBSession.activeSession.isOpen) {
            alertTitle = nil;
            
        } else {
            // Otherwise, use a general "connection problem" message.
            alertMsg = @"Operation failed due to a connection problem, retry later.";
        }
    } else {
        NSDictionary *resultDict = (NSDictionary *)result;
        alertMsg = [NSString stringWithFormat:@"Successfully posted '%@'.", message];
        NSString *postId = [resultDict valueForKey:@"id"];
        if (!postId) {
            postId = [resultDict valueForKey:@"postId"];
        }
        if (postId) {
            alertMsg = [NSString stringWithFormat:@"%@\nPost ID: %@", alertMsg, postId];
        }
        alertTitle = @"Success";
    }
    
    if (alertTitle) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alertTitle
                                                            message:alertMsg
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}

#pragma mark - EBPhotoPagesDataSource

- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController
    shouldExpectPhotoAtIndex:(NSInteger)index
{
    if(index < self.photos.count){
        return YES;
    }
    
    return NO;
}

- (void)photoPagesController:(EBPhotoPagesController *)controller
                imageAtIndex:(NSInteger)index
           completionHandler:(void (^)(UIImage *))handler
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        DEMOPhoto *photo = self.photos[index];
        if(self.simulateLatency){
            sleep(arc4random_uniform(2)+arc4random_uniform(2));
        }
        
        handler(photo.image);
    });
}


- (void)photoPagesController:(EBPhotoPagesController *)controller
attributedCaptionForPhotoAtIndex:(NSInteger)index
           completionHandler:(void (^)(NSAttributedString *))handler
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        DEMOPhoto *photo = self.photos[index];
        if(self.simulateLatency){
            sleep(arc4random_uniform(2)+arc4random_uniform(2));
        }
        
        handler(photo.attributedCaption);
    });
}

- (void)photoPagesController:(EBPhotoPagesController *)controller
      captionForPhotoAtIndex:(NSInteger)index
           completionHandler:(void (^)(NSString *))handler
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        DEMOPhoto *photo = self.photos[index];
        if(self.simulateLatency){
            sleep(arc4random_uniform(2)+arc4random_uniform(2));
        }
        
        handler(photo.caption);
    });
}


- (void)photoPagesController:(EBPhotoPagesController *)controller
     metaDataForPhotoAtIndex:(NSInteger)index
           completionHandler:(void (^)(NSDictionary *))handler
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        DEMOPhoto *photo = self.photos[index];
        if(self.simulateLatency){
            sleep(arc4random_uniform(2)+arc4random_uniform(2));
        }
        
        handler(photo.metaData);
    });
}

- (void)photoPagesController:(EBPhotoPagesController *)controller
         tagsForPhotoAtIndex:(NSInteger)index
           completionHandler:(void (^)(NSArray *))handler
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        DEMOPhoto *photo = self.photos[index];
        if(self.simulateLatency){
            sleep(arc4random_uniform(2)+arc4random_uniform(2));
        }
        
        handler(photo.tags);
    });
}


- (void)photoPagesController:(EBPhotoPagesController *)controller
     commentsForPhotoAtIndex:(NSInteger)index
           completionHandler:(void (^)(NSArray *))handler
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        DEMOPhoto *photo = self.photos[index];
        if(self.simulateLatency){
            sleep(arc4random_uniform(2)+arc4random_uniform(2));
        }
        
        handler(photo.comments);
    });
}


- (void)photoPagesController:(EBPhotoPagesController *)controller
numberOfcommentsForPhotoAtIndex:(NSInteger)index
           completionHandler:(void (^)(NSInteger))handler
{
    DEMOPhoto *photo = self.photos[index];
    if(self.simulateLatency){
        sleep(arc4random_uniform(2)+arc4random_uniform(2));
    }
    
    handler(photo.comments.count);
}


- (void)photoPagesController:(EBPhotoPagesController *)photoPagesController
       didReportPhotoAtIndex:(NSInteger)index
{
    NSLog(@"Reported photo at index %li", (long)index);
    //Do something about this image someone reported.
}



- (void)photoPagesController:(EBPhotoPagesController *)controller
            didDeleteComment:(id<EBPhotoCommentProtocol>)deletedComment
             forPhotoAtIndex:(NSInteger)index
{
    DEMOPhoto *photo = self.photos[index];
    NSMutableArray *remainingComments = [NSMutableArray arrayWithArray:photo.comments];
    [remainingComments removeObject:deletedComment];
    [photo setComments:[NSArray arrayWithArray:remainingComments]];
}


- (void)photoPagesController:(EBPhotoPagesController *)controller
         didDeleteTagPopover:(EBTagPopover *)tagPopover
              inPhotoAtIndex:(NSInteger)index
{
    DEMOPhoto *photo = self.photos[index];
    NSMutableArray *remainingTags = [NSMutableArray arrayWithArray:photo.tags];
    id<EBPhotoTagProtocol> tagData = [tagPopover dataSource];
    [remainingTags removeObject:tagData];
    [photo setTags:[NSArray arrayWithArray:remainingTags]];
}

- (void)photoPagesController:(EBPhotoPagesController *)photoPagesController
       didDeletePhotoAtIndex:(NSInteger)index
{
    NSLog(@"Delete photo at index %li", (long)index);
    DEMOPhoto *deletedPhoto = self.photos[index];
    NSMutableArray *remainingPhotos = [NSMutableArray arrayWithArray:self.photos];
    [remainingPhotos removeObject:deletedPhoto];
    [self setPhotos:remainingPhotos];
}

- (void)photoPagesController:(EBPhotoPagesController *)photoPagesController
         didAddNewTagAtPoint:(CGPoint)tagLocation
                    withText:(NSString *)tagText
             forPhotoAtIndex:(NSInteger)index
                     tagInfo:(NSDictionary *)tagInfo
                   withTagId:(NSString *)tagId
{
    NSLog(@"add new tag %@", tagText);
    
    DEMOPhoto *photo = self.photos[index];
    
    DEMOTag *newTag = [DEMOTag tagWithProperties:@{
                                                   @"tagPosition" : [NSValue valueWithCGPoint:tagLocation],
                                                   @"tagText"   : tagText,
                                                   @"tagId"     : tagId}];
    
    
    NSMutableArray *mutableTags = [NSMutableArray arrayWithArray:photo.tags];
    [mutableTags addObject:newTag];
    
    [photo setTags:[NSArray arrayWithArray:mutableTags]];
    
}


- (void)photoPagesController:(EBPhotoPagesController *)controller
              didPostComment:(NSString *)comment
             forPhotoAtIndex:(NSInteger)index
{
    DEMOComment *newComment = [DEMOComment
                               commentWithProperties:@{@"commentText": comment,
                                                       @"commentDate": [NSDate date],
                                                       @"authorImage": [UIImage imageNamed:@"guestAv.png"],
                                                       @"authorName" : @"Guest User"}];
    [newComment setUserCreated:YES];
    
    DEMOPhoto *photo = self.photos[index];
    [photo addComment:newComment];
    
    [controller setComments:photo.comments forPhotoAtIndex:index];
}



#pragma mark - User Permissions

- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController shouldAllowTaggingForPhotoAtIndex:(NSInteger)index
{
    if(!self.photos.count){
        return NO;
    }
    
    DEMOPhoto *photo = (DEMOPhoto *)self.photos[index];
    if(photo.disabledTagging){
        return NO;
    }
    
    return YES;
}

- (BOOL)photoPagesController:(EBPhotoPagesController *)controller
 shouldAllowDeleteForComment:(id<EBPhotoCommentProtocol>)comment
             forPhotoAtIndex:(NSInteger)index
{
    //We assume all comment objects used in the demo are of type DEMOComment
    DEMOComment *demoComment = (DEMOComment *)comment;
    
    if(demoComment.isUserCreated){
        //Demo user can only delete his or her own comments.
        return YES;
    }
    
    return NO;
}


- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController shouldAllowCommentingForPhotoAtIndex:(NSInteger)index
{
    if(!self.photos.count){
        return NO;
    }
    
    DEMOPhoto *photo = (DEMOPhoto *)self.photos[index];
    if(photo.disabledCommenting){
        return NO;
    } else {
        return YES;
    }
}


- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController shouldAllowActivitiesForPhotoAtIndex:(NSInteger)index
{
    if(!self.photos.count){
        return NO;
    }
    
    DEMOPhoto *photo = (DEMOPhoto *)self.photos[index];
    if(photo.disabledActivities){
        return NO;
    } else {
        return YES;
    }
}

- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController shouldAllowDeleteForPhotoAtIndex:(NSInteger)index
{
    if(!self.photos.count){
        return NO;
    }
    
    DEMOPhoto *photo = (DEMOPhoto *)self.photos[index];
    if(photo.disabledDelete){
        return NO;
    } else {
        return YES;
    }
}


- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController
     shouldAllowDeleteForTag:(EBTagPopover *)tagPopover
              inPhotoAtIndex:(NSInteger)index
{
    if(!self.photos.count){
        return NO;
    }
    
    DEMOPhoto *photo = (DEMOPhoto *)self.photos[index];
    if(photo.disabledDeleteForTags){
        return NO;
    }
    
    return YES;
}


- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController
    shouldAllowEditingForTag:(EBTagPopover *)tagPopover
              inPhotoAtIndex:(NSInteger)index
{
    if(!self.photos.count){
        return NO;
    }
    
    if(index > 0){
        return YES;
    }
    
    return NO;
}

- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController shouldAllowReportForPhotoAtIndex:(NSInteger)index
{
    return YES;
}

#pragma mark - EBPPhotoPagesDelegate


- (void)photoPagesControllerDidDismiss:(EBPhotoPagesController *)photoPagesController
{
    
    DEMOPhoto *tagPhoto = self.photos[0];
    
    tagsData = [[NSMutableArray alloc]init];
    
    EBTagPopover * tagPopover = [[EBTagPopover alloc]init];
    
    for (DEMOTag * a in tagPhoto.tags) {
        //NSMutableArray * temp = [[NSMutableArray alloc]init];
        
        [tagsData addObject:a.tagId];
        [tagsData addObject:a.tagText];
        [tagsData addObject:[NSString stringWithFormat:@"%f",a.tagPosition.x]];
        [tagsData addObject:[NSString stringWithFormat:@"%f",a.tagPosition.y]];
        
        tagPopover  = [[EBTagPopover alloc]initWithTag:a];
        
        [tagPopover setNormalizedArrowPoint:a.normalizedPosition];
        
        [tagPopover repositionInRect:self.imageScreen.frame];
        
        tagPopover.tag = 99;
        
        [self.imageScreen addSubview:tagPopover];
        
    }
}

-(BOOL)checkFacebookSession
{
    if([FBSession activeSession].state == FBSessionStateCreatedTokenLoaded)
    {
        return YES;
    }
    else{
        return NO;
    }
}


- (IBAction)facebookButtonPressed:(id)sender {
    
    if ([self checkFacebookSession]) {
        if (facebookButton.backgroundColor == [UIColor clearColor]) {
            [facebookButton setBackgroundColor:[UIColor greenColor]];
            facebookpost = true;
        }
        else
        {
            [facebookButton setBackgroundColor:[UIColor clearColor]];
            facebookpost = false;
        }
    }
    else
    {
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"please connect facebook in settings page" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
    }
    
    
    
    
}
@end
