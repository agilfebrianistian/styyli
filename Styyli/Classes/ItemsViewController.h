//
//  ItemsViewController.h
//  STYYLI
//
//  Created by Agil Febrianistian on 7/13/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemDetailsCell.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "Constant.h"
#import "UserInfo.h"
#import "KxMenu.h"
#import "ItemDetailsViewController.h"

@interface ItemsViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    int itemCategory;

}

@property (weak, nonatomic) IBOutlet UICollectionView *myCollection;


@property (strong, nonatomic) NSMutableArray* allTableData;
@property (strong, nonatomic) NSMutableArray* filteredTableData;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, assign) bool isFiltered;

- (IBAction)categoryButton:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *categoryButton;
@property (weak, nonatomic) IBOutlet UILabel *handlerText;

@end
