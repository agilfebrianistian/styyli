//
//  BrandTableCell.m
//  STYYLI
//
//  Created by Agil Febrianistian on 4/19/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import "BrandTableCell.h"
#import "BrandTableViewController.h"

@implementation BrandTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(15, 10, 320, 20)];
        self.lblTitle.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.lblTitle];
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


- (void)layoutSubviews{
    [super layoutSubviews];
    float indentPoints = self.indentationLevel * self.indentationWidth;
    
    self.contentView.frame = CGRectMake(
                                        indentPoints,
                                        self.contentView.frame.origin.y,
                                        self.contentView.frame.size.width - indentPoints,
                                        self.contentView.frame.size.height
                                        );
}

@end
