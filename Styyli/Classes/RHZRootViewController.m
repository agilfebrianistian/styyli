//
//  RHZRootViewController.m
//  Styyli
//
//  Created by R. Hafidhullah Zakariyya on 12/23/13.
//  Copyright (c) 2013 Mirosea. All rights reserved.
//

#import "RHZRootViewController.h"
#import "RHZAppDelegate.h"

@interface RHZRootViewController ()

@end

@implementation RHZRootViewController

- (id)initWithCoder:(NSCoder *)aDecoder{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    self = [super initWithCenterViewController:SharedAppDelegate.timelineController
                            leftViewController:[storyboard instantiateViewControllerWithIdentifier:@"RHZLeftViewController"]];
    
    self.leftSize = 80.0;
    self.panningMode = IIViewDeckDelegatePanning;
    //self.centerhiddenInteractivity = IIViewDeckCenterHiddenNotUserInteractiveWithTapToClose;
    self.sizeMode = IIViewDeckViewSizeMode;
    
    return self;
}

@end
