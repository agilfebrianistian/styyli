//
//  ItemDetailsViewController.m
//  STYYLI
//
//  Created by Agil Febrianistian on 7/21/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import "ItemDetailsViewController.h"
#import "UIImageView+WebCache.h"

@interface ItemDetailsViewController ()

@end

@implementation ItemDetailsViewController
@synthesize scroller,loveButton,commentButton;
@synthesize itemPicture,itemName,itemPrice,itemSize,itemDescription,allTableData;
@synthesize istimeline;
@synthesize buyButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [scroller setScrollEnabled:true];
    [scroller setContentSize:CGSizeMake(320,750)];
    
    
    [self addNavigationItem];
}


#pragma mark -
#pragma mark Add Navigation Item

- (void)addNavigationItem
{
    UIImage *leftImage = [UIImage imageNamed:@"IconBackToTimeline"];
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton addTarget:self action:@selector(showLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    leftButton.bounds = CGRectMake( 0, 0, leftImage.size.width, leftImage.size.height );
    [leftButton setImage:leftImage forState:UIControlStateNormal];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    self.navigationItem.leftBarButtonItem = leftItem;
    
}

- (void)showLeftMenu
{
    
    NSArray *tempViewControllers = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    [self.navigationController popToViewController:[tempViewControllers objectAtIndex:[tempViewControllers count]-2] animated:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (istimeline) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleUpdatedData:)
                                                     name:@"timelineItem"
                                                   object:nil];
    }
    
    
    [[CSActivityViewController sharedObject]showActivityView];
    
    [self getData];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"timelineItem"
                                                  object:nil];
}

-(void)handleUpdatedData:(NSNotification *)notification {
    NSLog(@"recieved");
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

#pragma mark -
#pragma mark get data from server

- (void)getData
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser
                             };
    
    [manager GET:[NSString stringWithFormat:@"%@items/%@/%@",SERVICE_URL,self.itemId,SERVICE_ITEM_DETAILS]   parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        [[CSActivityViewController sharedObject]hideActivityView];
        
        NSLog(@"%@",responseObject);
        
        allTableData = [[(NSMutableArray*)responseObject valueForKey:@"item"] mutableCopy];
        
        if([[[allTableData valueForKey:@"image"] valueForKey:@"image"] valueForKey:@"url"]==(id) [NSNull null])
            [itemPicture setImage:PLACEHOLDER_IMAGE];
        else
            [itemPicture sd_setImageWithURL:[NSURL URLWithString:[[[allTableData valueForKey:@"image"] valueForKey:@"image"] valueForKey:@"url"]] placeholderImage:PLACEHOLDER_IMAGE options:SDWebImageRefreshCached];
        
        itemName.text = [allTableData valueForKey:@"brand_name"];
        itemPrice.text = [NSString stringWithFormat:@"%@",[allTableData valueForKey:@"price"]];
        itemSize.text = @"XL,L,M,S";
        
        itemDescription.text = [allTableData valueForKey:@"description"];
        
        self.itemImage = [[[allTableData valueForKey:@"image"] valueForKey:@"image"]valueForKey:@"url"];
        
        [loveButton setTitle:[NSString stringWithFormat:@" %@",[allTableData valueForKey:@"likes_count"] ] forState:UIControlStateNormal];
        [commentButton setTitle:[NSString stringWithFormat:@" %@",[allTableData valueForKey:@"comment_count"]] forState:UIControlStateNormal];
        
        if ([[allTableData valueForKey:@"shop"]integerValue] == 1 && !([allTableData valueForKey:@"link"] == (id)[NSNull null]))
        {
            [buyButton setBackgroundColor:[UIColor yellowColor]];
            [buyButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            [buyButton setEnabled:true];
            
            
            link = [allTableData valueForKey:@"link"];
            
            if (![[allTableData valueForKey:@"deal_price"] isEqualToString:@""]) {
                itemPrice.text = [NSString stringWithFormat:@"%@",[allTableData valueForKey:@"deal_price"]];
            }
            
            if (![[allTableData valueForKey:@"deal_link"] isEqualToString:@""]) {
                link = [allTableData valueForKey:@"deal_link"];
            }
            
            
        }
        else
        {
            [buyButton setBackgroundColor:[UIColor blackColor]];
            [buyButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
            [buyButton setEnabled:false];
        }
        
        if ([[allTableData valueForKey:@"liked_by_me"]integerValue] > 0)
            [loveButton setImage:[UIImage imageNamed:@"nglike"] forState:UIControlStateNormal];
        else
            [loveButton setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
        
        
        
        CGRect frame = CGRectMake(0, 0, 0, 44);
        UILabel *label = [[UILabel alloc] initWithFrame:frame] ;
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont boldSystemFontOfSize:16.0];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor whiteColor];
        label.text = [allTableData valueForKey:@"name"];
        self.navigationItem.titleView = label;
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [[CSActivityViewController sharedObject]hideActivityView];
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}


- (IBAction)loveButton:(id)sender {
    
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    
    if ([[allTableData valueForKey:@"liked_by_me"]integerValue] > 0)
    {
        
        [loveButton setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
        [loveButton setTitle:[NSString stringWithFormat:@" %d",[[allTableData valueForKey:@"likes_count"]intValue]-1] forState:UIControlStateNormal];
    }
    else
    {
        [loveButton setImage:[UIImage imageNamed:@"nglike"] forState:UIControlStateNormal];
        [loveButton setTitle:[NSString stringWithFormat:@" %d",[[allTableData valueForKey:@"likes_count"]intValue]+1] forState:UIControlStateNormal];
        
    }
    
    NSLog(@"%@",[allTableData valueForKey:@"id"]);
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"api_key": API_KEY,
                             @"auth_token":[UserInfo sharedObject].tokenUser};
    
    [manager GET:[NSString stringWithFormat:@"%@items/%@/%@",SERVICE_URL,[allTableData valueForKey:@"id"],SERVICE_LIKE]  parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        
        [self getData];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
    
    
}

- (IBAction)commentButton:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RHZCommentViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"RHZCommentViewController"];
    
    controller.currId = [self.itemId intValue];
    controller.commentType = 1;
    controller.imageURL =  self.itemImage;
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)shareButton:(id)sender {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] init];
    actionSheet.delegate = self;
    
    [actionSheet  addButtonWithTitle:@"Share to Facebook"];

    [actionSheet addButtonWithTitle:@"Share to Twitter"];

    
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];
    
    
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Share to Facebook"]) {
       
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [controller setInitialText:@"get this item on STYYLI"];
        [controller addImage:itemPicture.image];
        [self presentViewController:controller animated:YES completion:Nil];
        
    }
    else
    {
       
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [controller setInitialText:@"get this item on STYYLI"];
        [controller addImage:itemPicture.image];
        [self presentViewController:controller animated:YES completion:Nil];
        
    }    
}

- (IBAction)butButtonPressed:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link]];

}
@end
