//
//  Constant.h
//  STYYLI
//
//  Created by Agil Febrianistian on 4/1/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#ifndef STYYLI_Constant_h
#define STYYLI_Constant_h


#pragma mark GLOBAL
//#define SERVICE_URL                   @"http://test.styyli.com/"
#define SERVICE_URL                     @"http://staging.styyli.com/api/v1/"
#define API_KEY                         @"kuyainside123xystjflwjfgwksuhfstyyliD2076da8"
#define SERVICE_REGISTER_DEVICE         @"device_tokens.json"


#pragma mark REGISTER
#define SERVICE_REGISTER                @"register.json"
#define SERVICE_CHECK_USERNAME          @"check_username.json"
#define SERVICE_CHECK_EMAIL             @"check_email.json"

#pragma mark LOGIN
#define SERVICE_LOGIN                   @"login.json"
#define SERVICE_AUTO_LOGIN              @"auto_login.json"
#define SERVICE_FORGOT_PASSWORD         @"password.json"
#define SERVICE_LOGOUT                  @"logout.json"


#pragma mark TIMELINE
#define SERVICE_TIMELINE                @"posts.json"
#define SERVICE_LIKE                    @"like.json"
#define SERVICE_TRENDSETTER             @"trendsetter.json"

#pragma mark LIKERS
#define SERVICE_LIKERS                  @"likers.json"

#pragma mark COMMENTS
#define SERVICE_COMMENTS                @"comments.json"

#pragma mark PROFILE
#define SERVICE_PROFILE                 @"show.json"
#define SERVICE_PROFILE_FAVORITE        @"favorites.json"
#define SERVICE_PROFILE_STYLEPIN        @"style_pins.json"
#define SERVICE_PROFILE_COLLECTION_TAG  @"collection_items.json"
#define SERVICE_PROFILE_COLLECTION_LOVE @"collection_items_loved.json"
#define SERVICE_PROFILE_REPORT          @"abuse_reports.json"

#pragma mark PROFILE_SETTING
#define SERVICE_PROFILE_SETTING         @"update_profile.json"

#pragma mark FOLLOW
#define SERVICE_FOLLOW                  @"follow.json"

#pragma mark POST
#define SERVICE_CATEGORIES              @"categories.json"
#define SERVICE_BRANDS                  @"brands.json"
#define SERVICE_ITEMS                   @"items.json"
#define SERVICE_POSTS                   @"posts.json"

#pragma mark HOME
#define SERVICE_NEWS                    @"news.json"

#pragma mark SEARCH
#define SERVICE_SEARCH_PEOPLE           @"users.json"
#define SERVICE_SEARCH_BRANDS           @"brands.json"
#define SERVICE_SEARCH_ITEM             @"items.json"
#define SERVICE_SEARCH_NEWS             @"news.json"
#define SERVICE_SEARCH_HISTORY          @"search.json"

#pragma mark SETTINGS

#pragma mark USER
#define SERVICE_ITEM_DETAILS            @"show.json"

#pragma mark NOTIFICATIONS
#define SERVICE_NOTIFICATIONS           @"notifications.json"
#define SERVICE_NOTIFICATIONS_READ      @"read.json"
#define SERVICE_NOTIFICATIONS_UNREAD    @"notification_unread.json"

#pragma mark FEEDBACK
#define SERVICE_FEEDBACK                @"feedbacks.json"

#pragma mark CHANGE PASSWORD
#define SERVICE_CHANGE_PASSWORD         @"change_password.json"

#pragma mark HASHTAG & NOTIFICATION
#define SERVICE_HASHTAGS                @"hashtags.json"

#pragma mark EDGE INSET
#define LOGO_INSET_STRIP                 79
#define LOGO_INSET_ARROW                 85


#pragma mark IMAGE PLACEHOLDER
#define PLACEHOLDER_AVATAR               [UIImage imageNamed:@"placeholder_avatar"]
#define PLACEHOLDER_IMAGE                [UIImage imageNamed:@"placeholder_image"]


#define DICT_GET(_dict_, _key_) _dict_[_key_] == [NSNull null] ? @"" : _dict_[_key_]

#endif