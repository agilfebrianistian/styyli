//
//  RHZAppDelegate.m
//  Styyli
//
//  Created by Reebonz on 12/23/13.
//  Copyright (c) 2013 Mirosea. All rights reserved.
//

#import "RHZAppDelegate.h"
#import "ICETutorialController.h"
#import "CSActivityViewController.h"
#import <HockeySDK/HockeySDK.h>
#import <Parse/Parse.h>

@implementation RHZAppDelegate

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication
                    fallbackHandler:^(FBAppCall *call) {
                        NSLog(@"In fallback handler");
                    }];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // FBSample logic
    // if the app is going away, we close the session object
    [FBSession.activeSession close];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // FBSample logic
    // Call the 'activateApp' method to log an app event for use in analytics and advertising reporting.
    [FBAppEvents activateApp];
    
    // FBSample logic
    // We need to properly handle activation of the application with regards to SSO
    //  (e.g., returning from iOS 6.0 authorization dialog or from fast app switching).
    [FBAppCall handleDidBecomeActive];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//akun agil
    [Parse setApplicationId:@"0igCFq0BaobL23cvFuYX4P0iPIrT41Zm9Nfsxgut"
                  clientKey:@"fb922cKonFf6DmKzP0k1xkWpzHejr2sbRxCsLD3e"];
    
//akun irzan
//    [Parse setApplicationId:@"4gAiH4t6NnIJeofjygZrcAT44NjGqXxE51K5Azp5"
//                  clientKey:@"hafhHBHT6XqyUvJvxBVKNimP1drfJLzYYyVLWSQl"];
    
    [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:@"4ddc4db74b89a039859f05782afe243b"];
    [[BITHockeyManager sharedHockeyManager] startManager];
    [[BITHockeyManager sharedHockeyManager].authenticator
     authenticateInstallation];

    
    // Override point for customization after application launch.
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"NavBar6"] forBarMetrics:UIBarMetricsDefault];
   
    [Fabric with:@[TwitterKit]];
//    ./Fabric.framework/run 990b0d5733e1fec36a7eb1641f475ca0f316d02c 6d8192bf724d2024386d8b3e26041aa5f1ca10d5cd63a660b7154d027d613d93
    
    [[CSActivityViewController sharedObject]showActivityView];
    
    
    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];

    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"timelineID"];
    
    if ([userdefault valueForKey:@"loginAuth"]) {
     
        NSLog(@"ada");
        
        [self doLogin];
    }
    else
    {
        
        NSLog(@"tidak ada");
        
        [self transitToTutorialScreen];
    }
    
    
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [application registerForRemoteNotifications];
    }
    else
    {
        // iOS < 8 Notifications
        [application registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
    


    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    return YES;
}


-(void)doLogin{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
        NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
    
        NSLog(@"%@", [userdefault valueForKey:@"loginAuth"]);
    
    
    NSDictionary *params = @{@"api_key":API_KEY,
                             @"auth_token": [userdefault valueForKey:@"loginAuth"]};
    
    [manager POST:[NSString stringWithFormat:@"%@%@",SERVICE_URL,SERVICE_AUTO_LOGIN] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

        [[CSActivityViewController sharedObject]hideActivityView];
  
        if ([[responseObject valueForKey:@"success"]integerValue] == 1) {
            
            [UserInfo sharedObject].tokenUser = [responseObject valueForKey:@"auth_token"];
            
            NSDictionary * userItem =[responseObject valueForKey:@"user"];
            
            [UserInfo sharedObject].firstNameUser   = [userItem valueForKey:@"first_name"];
            [UserInfo sharedObject].lastNameUser    = [userItem valueForKey:@"last_name"];
            [UserInfo sharedObject].birthdayUser    = [userItem valueForKey:@"birthday"];
            [UserInfo sharedObject].cityUser        = [userItem valueForKey:@"city"];
            [UserInfo sharedObject].countryUser     = [userItem valueForKey:@"country"];
            [UserInfo sharedObject].genderUser      = [userItem valueForKey:@"gender"];
            [UserInfo sharedObject].usernameUser    = [userItem valueForKey:@"username"];
            [UserInfo sharedObject].emailUser       = [userItem valueForKey:@"email"];
            [UserInfo sharedObject].idUser          = [userItem valueForKey:@"id"];
            [UserInfo sharedObject].phoneUser       = [userItem valueForKey:@"phone"];
            [UserInfo sharedObject].zipcodeUser     = [userItem valueForKey:@"zipcode"];
            
            
            [UserInfo sharedObject].avatarUser      = [userItem valueForKey:@"image_small"];
            
            NSUserDefaults *login = [NSUserDefaults standardUserDefaults];
            [login setValue:[UserInfo sharedObject].tokenUser forKey:@"loginAuth"];
            [login synchronize];
            
            [self transitToHomeScreen];
            
        }
        else
        {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            [self transitToTutorialScreen];
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSLog(@"Error: %@", error);

        [[CSActivityViewController sharedObject]hideActivityView];
        
        [self transitToTutorialScreen];
        
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
    }];
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}



#pragma mark -
#pragma mark Global Variables

- (UINavigationController *)profileController
{
    if(_profileController == nil){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _profileController = [storyboard instantiateViewControllerWithIdentifier:@"RHZProfileViewController"];
    }
    
    return [[UINavigationController alloc] initWithRootViewController:_profileController];

//    if(_profileController == nil){
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        _profileController = [storyboard instantiateViewControllerWithIdentifier:@"RHZProfileViewController"];
//        
//        self.navController = [[UINavigationController alloc] init];
//        
//        [self.navController setViewControllers:@[_profileController] animated:NO];
//        
//    }
//    
//    return self.navController;



}

- (UINavigationController *)timelineController
{
    if(_timelineController == nil){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _timelineController = [storyboard instantiateViewControllerWithIdentifier:@"timeline"];
        
        self.navController = [[UINavigationController alloc] initWithNavigationBarClass:[GTScrollNavigationBar class] toolbarClass:nil];
        
        [self.navController setViewControllers:@[_timelineController] animated:NO];
        
    }
    return self.navController;
}

- (UINavigationController *)homeController
{
    if(_homeController == nil){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _homeController = [storyboard instantiateViewControllerWithIdentifier:@"home"];
        
    }
    return [[UINavigationController alloc] initWithRootViewController:_homeController];
}

- (UINavigationController *)newsController
{
    if(_newsController == nil){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _newsController = [storyboard instantiateViewControllerWithIdentifier:@"RHZNewsViewController"];
    }
    return [[UINavigationController alloc] initWithRootViewController:_newsController];
}

- (UINavigationController *)shopController
{
    if(_shopController == nil){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _shopController = [storyboard instantiateViewControllerWithIdentifier:@"RHZShopViewController"];
        
    }
    return [[UINavigationController alloc] initWithRootViewController:_shopController];
}


- (UINavigationController *)settingsController
{
    if(_settingsController == nil){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _settingsController = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    }
    return [[UINavigationController alloc] initWithRootViewController:_settingsController];
}


- (UINavigationController *)stylePinController
{
    if(_stylePinController == nil){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _stylePinController = [storyboard instantiateViewControllerWithIdentifier:@"stylepin"];
    }
    return [[UINavigationController alloc] initWithRootViewController:_stylePinController];
}

- (UINavigationController *)collectionController
{
    if(_collectionController == nil){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _collectionController = [storyboard instantiateViewControllerWithIdentifier:@"collection"];
    }
    return [[UINavigationController alloc] initWithRootViewController:_collectionController];
}


- (void)transitToTutorialScreen
{
    
    
    NSUserDefaults *login = [NSUserDefaults standardUserDefaults];
    [login setValue:NULL forKey:@"loginAuth"];
    [login synchronize];
    
    
    // Init the pages texts, and pictures.
    ICETutorialPage *layer1 = [[ICETutorialPage alloc] initWithSubTitle:@""
                                                            description:@"Snap what you wear,\r\n capture your best look of the day"
                                                            pictureName:@"TutorialScreen011"];
    ICETutorialPage *layer2 = [[ICETutorialPage alloc] initWithSubTitle:@""
                                                            description:@"Share it to your friends and followers,\r\n always dress to impress"
                                                            pictureName:@"TutorialScreen012"];
    ICETutorialPage *layer3 = [[ICETutorialPage alloc] initWithSubTitle:@""
                                                            description:@"Follow your favourite trendsetters,\r\n be inspired and in the now"
                                                            pictureName:@"TutorialScreen013"];
    ICETutorialPage *layer4 = [[ICETutorialPage alloc] initWithSubTitle:@""
                                                            description:@"Take your wardrobe everywhere,\r\n curate your selected collections"
                                                            pictureName:@"TutorialScreen014"];
    

    // Set the common style for SubTitles and Description (can be overrided on each page).
    ICETutorialLabelStyle *subStyle = [[ICETutorialLabelStyle alloc] init];
    [subStyle setFont:TUTORIAL_SUB_TITLE_FONT];
    [subStyle setTextColor:TUTORIAL_LABEL_TEXT_COLOR];
    [subStyle setLinesNumber:TUTORIAL_SUB_TITLE_LINES_NUMBER];
    [subStyle setOffset:TUTORIAL_SUB_TITLE_OFFSET];
    
    ICETutorialLabelStyle *descStyle = [[ICETutorialLabelStyle alloc] init];
    [descStyle setFont:TUTORIAL_DESC_FONT];
    [descStyle setTextColor:TUTORIAL_LABEL_TEXT_COLOR];
    [descStyle setLinesNumber:TUTORIAL_DESC_LINES_NUMBER];
    [descStyle setOffset:TUTORIAL_DESC_OFFSET];
    
    // Load into an array.
    NSArray *tutorialLayers = @[layer1,layer2,layer3,layer4];
    
    // Override point for customization after application launch.

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    self.viewController = [storyboard instantiateViewControllerWithIdentifier:@"ICETutorialController"];
    [self.viewController doInitialize];
    [self.viewController setPages:tutorialLayers];
    [self.viewController stopScrolling];
    
    // Set the common styles, and start scrolling (auto scroll, and looping enabled by default)
    [self.viewController setCommonPageSubTitleStyle:subStyle];
    [self.viewController setCommonPageDescriptionStyle:descStyle];
    
     __unsafe_unretained typeof(self) weakSelf = self;
    
    // Set button 1 action.
    [self.viewController setButton1Block:^(UIButton *button){
        
        double delayInSeconds = 0.2;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    [weakSelf transitToRegister];
        });

      
    
    }];

    // Set button 2 action, stop the scrolling.
    [self.viewController setButton2Block:^(UIButton *button){
        
        double delayInSeconds = 0.2;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            [weakSelf transitToLogin];

        });

        
    }];
    
    // Run it.
    [self.viewController startScrolling];
    
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
}

- (void)transitToHomeScreen
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    self.rootviewController = [storyboard instantiateViewControllerWithIdentifier:@"RHZRootViewController"];
    
    self.window.rootViewController = self.rootviewController;
    
    [self.window makeKeyAndVisible];
    
}

- (void)transitToLogin
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    self.loginviewController = [storyboard instantiateViewControllerWithIdentifier:@"login"];
    
    //self.window.rootViewController = self.loginviewController;
    
//    [UIView transitionFromView:self.window.rootViewController.view
//                        toView:self.loginviewController.view
//                      duration:0.8
//                       options:UIViewAnimationOptionTransitionFlipFromRight
//                    completion:^(BOOL finished)
//     {
//         self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:self.loginviewController];
//     }];
    
    
    self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:self.loginviewController];
    [self.window makeKeyAndVisible];
    
}


- (void)transitToRegister
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    self.registerviewController = [storyboard instantiateViewControllerWithIdentifier:@"register"];
    
    //self.window.rootViewController = self.registerviewController;
    
    
//    [UIView transitionFromView:self.window.rootViewController.view
//                        toView:self.registerviewController.view
//                      duration:0.8
//                       options:UIViewAnimationOptionTransitionFlipFromRight
//                    completion:^(BOOL finished)
//     {
//         self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:self.registerviewController];
//     }];
    
    self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:self.registerviewController];
    
    [self.window makeKeyAndVisible];
    
}


//- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
//{
//	NSLog(@"My token is: %@", deviceToken);
//    
//    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
//    [userdefault setValue:deviceToken forKey:@"deviceToken"];
//    [userdefault synchronize];
//    
//    
//}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
    NSUserDefaults *token = [NSUserDefaults standardUserDefaults];
    [token setValue:NULL forKey:@"deviceToken"];
    [token synchronize];
    
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
    [userdefault setValue:deviceToken forKey:@"deviceToken"];
    [userdefault synchronize];
    
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [PFPush handlePush:userInfo];
}

@end
