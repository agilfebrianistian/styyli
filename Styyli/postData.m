//
//  postData.m
//  STYYLI
//
//  Created by Agil Febrianistian on 4/12/14.
//  Copyright (c) 2014 Mirosea. All rights reserved.
//

#import "postData.h"

@implementation postData
@synthesize pictureData,categoryId,itemName,brandId,itemId,step;

static postData* sharedObject = nil;

+(postData*)sharedObject{
    @synchronized(self){
        if (sharedObject ==nil) {
            sharedObject = [[self alloc]init];
        }
    }
    return sharedObject;
}

@end
